# Shopfront Service

This is a facade to SFCC in the form of a REST API. It 
expects SLT canonical JSON. In most cases the services 
attempts to convert to SFCC XML and save somewhere: an 
SFTP or the local file system.

This is an alternative to storefront-service. It uses the FastAPI 
framework, which offers async behavior and strongly-typed adta models 
and validation, where storefront-service is 
built on Falcon. If possible use this over storefront-service. The main client 
and consumer of this service will be Lambdas in the shopfront-loaders project.

## Install
Install with Git and Pipenv
```bash
git clone git@bitbucket.org:slt_development/shopfront-service.git
cd shopfront-service
pipenv install --dev
```

Tell Python where it's at. Developers can use this .env file 
to store sensitive properties ordinary stored in settings.ini
or parameter store so they are not inadvertently checked 
into source control.
```bash
pipenv run echo "PYTHONPATH=${PWD}" >> .env  
source .env 
```

# Configuration
See settings.init and configuration.py. slt-utils is used in 
concert with settings.init; it  checks AWS 
Parameter Store and environment values. The Parameter Store 
prefix for this application should be _shopfront-service_

The examples below won't work unless you've configured the 
application via settings.init or by creating and 
sourcing a .env file.

# Running it

Enter the virtual environment
```bash
pipenv shell
```

Run via WSGI
```bash
uvicorn --port 8008 app.main:app 
```
Run it with some environment variables and the reload option
```bash
APP_ENV=dev APP_NAME=shopfront_service uvicorn --port 8008 app.main:app --reload
```
Adding these variables will allow the program to check the AWS Parameter 
Store before properties. The values in Parameter 
Store will override environment variables and the 
settings.ini file. With the above settings the program would search 
for the SFCC_SFTP_HOST property in /tf-dev/shared/SFCC_SFTP_HOST and 
/tf-dev/shopfront_service/SFCC_SFTP_HOST. The slt-utils library is 
used for this.  

View the API specification

```bash
http://127.0.0.1:8008/shopfront-service/docs
http://127.0.0.1:8008/shopfront-service/redoc
http://127.0.0.1:8008/shopfront-service/openapi.json
```

Exercise the service with the great httpie utility
```bash
http -v POST localhost:8008/shopfront-service/stores < app/tests/stores/stores_one.json
http -v POST localhost:8008/shopfront-service/hardgoods/bulk < app/tests/hardgoods/hardgoods_many.json
http -v POST localhost:8008/shopfront-service/assignments/bulk < app/tests/assignments/assignments_many.json
http -v POST localhost:8008/shopfront-service/inventory/bulk < app/tests/inventory/inventory_many.json
http -v POST localhost:8008/shopfront-service/pricebooks/bulk < app/tests/pricebooks/pricebooks_many.json 
http -v POST localhost:8008/shopfront-service/taxonomy/bulk  < app/tests/taxonomy/taxonomy_many.json
```

Get OpenAPI Spec
```bash
http://localhost:8008/docs
```
## Asynchronous operations
Most of the operations use HTTP POST and the work is performed synchronously: 
 callers wait until the data is written to disk or SFTP'd somewhere. 
 However an alternative approach is provided for the Price Books resource. 
 The create_pricebooks_async operation returns an HTTP 200 and a status 
 message immediately after converting to XML, but the SFTP operations are run within 
 a separate background task and only  after the response is returned.


## Run tests

All verbose and w/o warnings
```bash
pytest -s -vv --disable-pytest-warnings
```

# Also See
- The functions in storefront-loaders call this service. See https://bitbucket.org/slt_development/storefront-loaders/src/master/
- SFCC Development: https://www.sltnation.com/confluence/display/SLTP/SFCC