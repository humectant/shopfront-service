import pyxb

from statsd import StatsClient
from typing import List
from pydantic import BaseModel
from app.util import shopfront
from app.schema import catalog
from app.configuration import CONFIG

import logging

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-assignments')

logger = logging.getLogger('shopfront.assignments')
logger.setLevel(CONFIG.LOG_LEVEL)


class Assignment(BaseModel):

    productId: str
    productName: str = None
    classificationReferences: List[str]


class Assignments(BaseModel):

    categoryAssignments: List[Assignment]

    def to_pyxb(self):

        pyxb_catalog = catalog.catalog()
        pyxb_catalog.catalog_id = "shop-slt-storefront-catalog"

        categories = []
        for product in self.categoryAssignments:
            product_id = product.productId
            category_ids = product.classificationReferences
            for category_id in category_ids:
                logging.info(f"Product {product_id} assigned to category {category_id}")
                category = catalog.category_assignment()
                category.category_id = category_id
                category.product_id = product_id
                categories.append(category)

        pyxb_catalog.extend(categories)

        return pyxb_catalog


@statsd.timer('create_assignments_bulk')
def create_assignments_bulk(assignments_object: Assignments, save=True, tracking_id=None):

    logger.info(f'create_assignments_bulk', extra={"tracking_id": tracking_id})
    doc = assignments_object.to_pyxb()
    xml = shopfront.to_xml(doc, CONFIG.assignments_namespace)
    path = shopfront.url_join(
        CONFIG.output_directory,
        CONFIG.assignments_directory)
    if save:
        shopfront.save(CONFIG.output_destination,
                       path,
                       shopfront.file_name(CONFIG.assignments_filename),
                       xml)
    output = xml.decode("utf-8")
    return output


