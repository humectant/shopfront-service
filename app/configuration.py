from decouple import config
import logging.config


class InjectingFilter(logging.Filter):

    def filter(self, record):

        if not hasattr(record, 'tracking_id'):
            record.tracking_id = ''

        return True


DEFAULT_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'correlation_filter': {
            '()': InjectingFilter
        }
    },
    'formatters': {
        'default': {
            'format': '[%(asctime)s] [%(levelname)s] [%(name)s] [%(tracking_id)s] %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'WARNING',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'formatter': 'default',
            'filters': ['correlation_filter']
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'propagate': True
        }
    }
}

logging.config.dictConfig(DEFAULT_LOGGING)


class Config(object):

    # Properties that can be externally managed
    LOG_LEVEL = config('LOG_LEVEL')
    KEY_BUCKET = config('KEY_BUCKET')
    SFCC_SFTP_HOST = config('SFCC_SFTP_HOST')
    SFCC_SFTP_USERNAME = config('SFCC_SFTP_USERNAME')
    SFCC_SFTP_PASSWORD = config('SFCC_SFTP_PASSWORD')
    SFCC_SFTP_KEY_PATH = config('SFCC_SFTP_KEY_PATH')

    if LOG_LEVEL:
        for handler in logging.getLogger().handlers:
            if handler.get_name() == 'console':
                handler.setLevel(LOG_LEVEL)

    # Properties that rarely change
    service_root = 'shopfront-service'
    output_destination = 'sftp'  # local or sftp
    output_directory = 'stage/to_sfcc/pim'

    stores_filename = 'shop-slt-stores'
    stores_directory = 'stores'
    stores_namespace = 'http://www.demandware.com/xml/impex/store/2007-04-30'

    assignments_filename = 'shop-slt-storefront-catalog-cat-assignments'
    assignments_directory = 'categoryassignment'
    assignments_namespace = 'http://www.demandware.com/xml/impex/catalog/2006-10-31'

    culinary_filename = 'shop-slt-master-catalog-culinary'
    culinary_directory = 'product_catalogs/culinary_classes'
    culinary_namespace = 'http://www.demandware.com/xml/impex/catalog/2006-10-31'

    hardgoods_filename = 'shop-slt-master-catalog'
    hardgoods_directory = 'product_catalogs/hard_goods'
    hardgoods_namespace = 'http://www.demandware.com/xml/impex/catalog/2006-10-31'

    taxonomy_filename = 'shop-slt-storefront-catalog'
    taxonomy_directory = 'storefront-catalog'
    taxonomy_namespace = 'http://www.demandware.com/xml/impex/catalog/2006-10-31'

    pricebook_list_filename = 'shop-slt-list-price'
    pricebook_regular_filename = 'shop-slt-regular-price'
    pricebook_sale_filename = 'shop-slt-sale-price'
    pricebooks_directory = 'price_books/hard_goods'
    pricebooks_namespace = 'http://www.demandware.com/xml/impex/pricebook/2006-10-31'

    inventory_webstore_id = 2
    inventory_filename = 'inventory'
    inventory_webstore_full = 'slt-inventory-webstore-FULLLOAD'
    inventory_physcial_full = 'slt-inventory-store-FULLLOAD'
    inventory_webstore_delta = 'slt-inventory-webstore-DELTALOAD'
    inventory_physcial_delta = 'slt-inventory-store-DELTALOAD'
    inventory_web_directory = 'inventory/web_store'
    inventory_bopis_directory = 'inventory/bopis_stores'
    inventory_namespace = 'http://www.demandware.com/xml/impex/inventory/2007-05-31'


CONFIG = Config()


