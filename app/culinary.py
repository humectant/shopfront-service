import pyxb
import threading

from statsd import StatsClient
from typing import List
from pydantic import BaseModel
from app.util import shopfront
from app.schema import catalog
from app.configuration import CONFIG
from app.util import sfcc

import logging

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-culinary')

logger = logging.getLogger('shopfront.culinary')
logger.setLevel(CONFIG.LOG_LEVEL)


class CulinaryEvent(BaseModel):

    storeId: str
    bbEventChainId: str = None
    month: str = None
    year: str = None
    date: str = None
    dateFilter: str = None
    dateAndTime: str = None
    isCourse: str = None
    taxClassID: str = None
    onlineFlag: str = None
    onlineFrom: str = None
    onlineTo: str = None
    searchableFlag: str = None


class CulinaryProduct(BaseModel):

    productId: str
    productName: str = None
    menu: str = None
    shortMenu: str = None
    notes: str = None
    vendorStyleNumber: str = None
    departmentId: str = None
    departmentName: str = None
    classId: str = None
    className: str = None
    divisionId: str = None
    divisionName: str = None
    classDescription: str = None
    classRestrictionMessage: str = None
    classImageUrl: str = None
    productUrl: str = None
    type: str = None
    classType: str = None
    cuisineFilter: str = None
    classTags: str = None
    reportingClassType: str = None
    reportingClassPgm: str = None
    isClassCancelled: str = None
    webShortDescription: str = None
    classLength: str = None
    classLevel: str = None
    classSubType: str = None
    bbCompanyId: str = None
    bbEventGroupId: str = None
    culinaryEvents: List[CulinaryEvent] = None


class CulinaryProducts(BaseModel):

    culinary: List[CulinaryProduct] = None

    def to_pyxb(self):

        pyxb_catalog = catalog.catalog()
        pyxb_catalog.catalog_id = 'shop-slt-master-catalog-culinary'

        for culinary_product in self.culinary:

            # Fetch culinary events
            culinary_events = []
            # culinary_events = [store_event
            #                    for store_event in slt_cooking_class.get("storeEvents", [])]
            # for slt_store_event in slt_store_events:
            #    store_event = Culinary.build_store_event(slt_store_event)
            #    cat.product.append(store_event)

            cooking_class = self.build_cooking_class(culinary_product, culinary_events)
            pyxb_catalog.product.append(cooking_class)

            return pyxb_catalog

    def build_cooking_class(self, culinary_product: CulinaryProduct, culinary_events: List[CulinaryEvent]):

        pyxb_catalog = catalog.product()
        pyxb_catalog.product_id = culinary_product.productId

        # Page attributes
        page_attributes = catalog.complexType_PageAttributes()
        sfcc.append_attributes([
            (page_attributes.page_description, culinary_product.webShortDescription),
            (page_attributes.page_title, culinary_product.webShortDescription)]
        )
        pyxb_catalog.append(page_attributes)

        # Custom attributes
        custom_attributes = catalog.sharedType_SiteSpecificCustomAttributes()
        pyxb_catalog.append(custom_attributes)
        sfcc.map_custom_attributes(custom_attributes, [
            ('classDescription', culinary_product.classDescription),
            ('productType', 'culinary'),
            ('storeId', culinary_product.storeId),
            ('bbEventGroupId', culinary_product.bbEventGroupId),
            ('bbCompanyId', culinary_product.bbCompanyId),
            ('cuisine', culinary_product.cuisine),
            ('classType', culinary_product.classType),
            ('whatYouWillLearn', culinary_product.whatYouWillLearn),
            ('whatToExpect', culinary_product.whatToExpect),
            ('shortMenu', culinary_product.shortMenu),
            ('longMenu', culinary_product.longMenu),
            ('age', culinary_product.age),
            ('instructor', culinary_product.instructor),
            ('cookbookCopyrightText', culinary_product.cookbookCopyrightText),
            ('cookbookCopyrightImage', culinary_product.cookbookCopyrightImage),
            ('classFormat', culinary_product.classFormat),
            ('isCourse', culinary_product.isCourse),
        ])

        # Variants
        variations = catalog.complexType_Product_Variations()
        variations_attributes = catalog.complexType_Product_Variations_Attributes()
        variants = catalog.complexType_Product_Variations_Variants()
        sfcc.add_variants(variants, culinary_events)

        # TODO
        # product.variations.append(build_variants(event_list))
        # product.variations.attributes.extend(build_date_time_variation_attribute(event_list))

        # Session Variations (Dates and Times)
        # TODO
        self.add_culinary_variations(variations_attributes, culinary_events)
        variations.append(variations_attributes)
        variations.append(variants)
        pyxb_catalog.append(variations)

        # Images / Variations
        # TODO
        # pyxb_catalog.images = catalog.complexType_Product_Images()
        # image_groups = sfcc.image_groups(culinary_product, slt_product_skus)
        # for image_group in image_groups:
        #    pyxb_catalog.images.append(image_group)

        # Product Attributes
        sfcc.append_attributes([
            (pyxb_catalog.long_description, culinary_product.get('classDescription'))
        ])

        pyxb_catalog.min_order_quantity = 1
        pyxb_catalog.step_quantity = 1

        return pyxb_catalog

    def build_culinary_event(self, culinary_event: CulinaryEvent):

        pyxb_catalog = catalog.product()
        custom_attributes = catalog.sharedType_SiteSpecificCustomAttributes()
        pyxb_catalog.product_id = culinary_event.bbEventChainId
        online_flag = bool(getattr(culinary_event, 'isWebSkuActive', False))
        sfcc.append_attrib(pyxb_catalog.online_flag, sfcc.sfcc_boolean(online_flag))
        if online_flag:
            sfcc.append_date_attrib(pyxb_catalog.online_from, culinary_event.onlineFrom)
            sfcc.append_date_attrib(pyxb_catalog.online_to, culinary_event.onlineTo)
        sfcc.append_attrib(pyxb_catalog.searchable_flag, getattr(culinary_event, 'searchableFlag', 'true'))
        sfcc.map_attrib(pyxb_catalog, 'available_flag', getattr(culinary_event, 'availableFlag', 'true'))

        pyxb_catalog.append(custom_attributes)
        sfcc.map_custom_attributes(custom_attributes, [
            ('material', culinary_event.material),
            ('capacity', culinary_event.capacity),
            ('color', culinary_event.itemColor),
            ('size', culinary_event.itemSize),
            ('giftPack', culinary_event.giftWrap)
        ])

        return pyxb_catalog

    def add_culinary_variations(self, variations_attributes, culinary_events: List[CulinaryEvent]):
        """
        Generate variations XML
        sessionDate sessionTime
        sort by date
        """

        """
        date_values = catalog.complexType_VariationAttribute_Values()
        date_attrib = build_attribute('date', 'Date')
        date_attrib.append(date_values)
        variations_attributes.append(date_attrib)
        time_values = catalog.complexType_VariationAttribute_Values()
        time_attrib = build_attribute('time', 'Time')
        time_attrib.append(time_values)
        variations_attributes.append(time_attrib)
        for culinary_event in culinary_events:
            date = culinary_event.xxx
            time = culinary_event.xxx
            variation_attribute_value = \
                build_variation_attribute_value(id_from_date_value(date), date)
            date_values.append(variation_attribute_value)
            variation_attribute_value = \
                build_variation_attribute_value(id_from_date_value(time), time)
            time_values.append(variation_attribute_value)
        """


@statsd.timer('create_culinary')
def create_culinary(culinary_product: CulinaryProduct, save=True, tracking_id=None):

    logger.info(f'create_culinary', extra={"tracking_id": tracking_id})
    culinary = CulinaryProducts(culinary=[culinary_product])
    return create_culinary_bulk(culinary, save)


@statsd.timer('create_culinary_bulk')
def create_culinary_bulk(culinary_products: CulinaryProducts, save=True, tracking_id=None):

    logger.info(f'create_culinary_bulk', extra={"tracking_id": tracking_id})
    doc = culinary_products.to_pyxb()
    xml = shopfront.to_xml(doc, CONFIG.culinary_namespace)
    path = shopfront.url_join(CONFIG.output_directory, CONFIG.culinary_directory)
    if save:
        shopfront.save(CONFIG.output_destination,
                       path,
                       shopfront.file_name(CONFIG.culinary_filename),
                       xml)
    return xml.decode("utf-8")




