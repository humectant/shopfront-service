import pyxb

from statsd import StatsClient
from typing import List
from pydantic import BaseModel
from app.util import shopfront
from app.schema import catalog
from app.configuration import CONFIG
from app.util import sfcc

import logging

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-hardgoods')

logger = logging.getLogger('shopfront.hardgoods')
logger.setLevel(CONFIG.LOG_LEVEL)


class HardGoodSKU(BaseModel):

    skuId: str
    productId: str
    skuName: str = None
    capacity: str = None
    countryOfManufacture: str = None
    countryOfOrigin: str = None
    giftRegistry: str = None
    giftWrapPrice: str = None
    imageUrl: str = None
    isAssigned: str = None
    isDropShip: str = None
    isGiftWrap: str = None
    isHazMat: str = None
    isItemSet: str = None
    isProp65Mat: str = None
    isWebOnly: str = None
    isWebSkuActive: str = None
    itemColor: str = None
    itemSize: str = None
    itemDescription: str = None
    onlineFrom: str = None
    onlineTo: str = None
    productHeadline: str = None
    productHeight: str = None
    productLength: str = None
    productWeight: str = None
    productWidth: str = None
    searchableFlag: str = None
    skuType: str = None
    isSoldOut: str = None
    status: str = None
    style: str = None
    vendorId: str = None
    vendorName: str = None
    vendorStyle: str = None
    webBrand: str = None
    webMaterial: str = None
    webPrice: str = None
    webShortDescription: str = None


class HardGoodProduct(BaseModel):

    productId: str
    productName: str = None
    careAndUsage: str = None
    classId: str = None
    className: str = None
    colorGroup: str = None
    dropShipMessage: str = None
    featuresText: str = None
    isDropShip: str = None
    isHazMat: str = None
    isNew: str = None
    isProp65Mat: str = None
    isWebOnly: str = None
    isWebSkuActive: str = None
    ipDescription: str = None
    keywords: str = None
    onlineFrom: str = None
    onlineTo: str = None
    onSale: str = None
    pageTitle: str = None
    productDescription: str = None
    productHeadline: str = None
    productHeight: str = None
    productLength: str = None
    productWeight: str = None
    productWidth: str = None
    searchableFlag: str = None
    specifications: str = None
    vendorName: str = None
    vendorNumber: str = None
    vendorStyleNumber: str = None
    warranty: str = None
    whatsInBox: str = None
    webBrand: str = None
    webMaterial: str = None
    webShortDescription: str = None
    youtubeVideoIds: str = None
    classificationReferences: List[str] = None
    childSkus: List[str] = None
    childSkuDetails: List[HardGoodSKU] = None


class HardGoodProducts(BaseModel):

    products: List[HardGoodProduct]

    def to_pyxb(self):

        pyxb_catalog = catalog.catalog()
        pyxb_catalog.catalog_id = 'shop-slt-master-catalog'

        for product in self.products:
            pyxb_product = self.build_product(product)
            pyxb_catalog.product.append(pyxb_product)
            for sku in product.childSkuDetails:
                pyxb_sku = self.build_product_sku(sku)
                pyxb_catalog.product.append(pyxb_sku)

        return pyxb_catalog

    def build_product(self, product: HardGoodProduct):

        sfcc_record = catalog.product()
        sfcc_record.product_id = product.productId

        # Page attributes
        page_attributes = catalog.complexType_PageAttributes()
        sfcc.append_attributes([
            (page_attributes.page_description, product.webShortDescription),
            (page_attributes.page_title, product.webShortDescription),
            (page_attributes.page_keywords, product.keywords)]
        )
        sfcc_record.append(page_attributes)

        # Custom attributes
        custom_attributes = catalog.sharedType_SiteSpecificCustomAttributes()
        sfcc_record.append(custom_attributes)
        sfcc.map_custom_attributes(custom_attributes, [
            ('productHeadLine', product.productHeadline),
            ('youtubeVideoIds', product.youtubeVideoIds),
            ('vendorStyleNumber', 'Hardgood'),
            ('material', product.webMaterial),
            ('productType', 'Hardgood'),
            ('warranty', product.warranty),
            ('featuresText', product.featuresText),
            ('careAndUsage', product.careAndUsage),
            ('whatsInBox', product.whatsInBox),
            ('colorGroup', product.colorGroup),
            ('dropShipMessage', product.dropShipMessage),
            ('dropShipEligibility', product.isDropShip),
            ('isNew', product.isNew),
            ('isSale', product.onSale)
        ])

        # Variants
        variations = catalog.complexType_Product_Variations()
        variations_attributes = catalog.complexType_Product_Variations_Attributes()
        variants = catalog.complexType_Product_Variations_Variants()
        sfcc.add_variants(variants, product.childSkuDetails)

        # Color & Size Variations
        self.add_hardgood_variations(variations_attributes, product)
        variations.append(variations_attributes)
        variations.append(variants)
        sfcc_record.append(variations)

        # Images / Variations
        sfcc_record.images = catalog.complexType_Product_Images()
        image_groups = sfcc.image_groups(product, product.childSkuDetails)
        for image_group in image_groups:
            sfcc_record.images.append(image_group)

        # Product Attributes
        sfcc.append_attributes([
            (sfcc_record.long_description, product.productDescription),
            (sfcc_record.short_description, product.productName),
            (sfcc_record.display_name, product.webShortDescription)
        ])

        sfcc.map_attrib(sfcc_record, 'upc', product.vendorNumber)
        sfcc_record.min_order_quantity = 1
        sfcc_record.step_quantity = 1
        sfcc.append_attrib(sfcc_record.searchable_flag,
                           sfcc.sfcc_boolean(product.searchableFlag))

        online_flag = bool(getattr(product, 'isWebSkuActive', False))
        sfcc.append_attrib(sfcc_record.online_flag, sfcc.sfcc_boolean(online_flag))
        if online_flag:
            sfcc.append_date_attrib(sfcc_record.online_from, product.onlineFrom)
            sfcc.append_date_attrib(sfcc_record.online_to, product.onlineTo)

        sfcc_record.tax_class_id = 'standard'
        sfcc.map_attrib(sfcc_record, 'brand', product.webBrand)
        sfcc.map_attrib(sfcc_record, 'manufacturer_name', product.webBrand)
        sfcc_record.classification_category = 'products'

        return sfcc_record

    def build_product_sku(self, sku: HardGoodSKU):

        sfcc_sku_record = catalog.product()
        custom_attributes = catalog.sharedType_SiteSpecificCustomAttributes()
        sfcc_sku_record.product_id = sku.skuId
        online_flag = bool(getattr(sku, 'isWebSkuActive', False))
        sfcc.append_attrib(sfcc_sku_record.online_flag, sfcc.sfcc_boolean(online_flag))
        if online_flag:
            sfcc.append_date_attrib(sfcc_sku_record.online_from, sku.onlineFrom)
            sfcc.append_date_attrib(sfcc_sku_record.online_to, sku.onlineTo)
        sfcc.append_attrib(sfcc_sku_record.searchable_flag,
                           sfcc.sfcc_boolean(sku.searchableFlag))
        sfcc.map_attrib(sfcc_sku_record, 'available_flag', 'true')

        sfcc_sku_record.append(custom_attributes)
        sfcc.map_custom_attributes(custom_attributes, [
            ('material', sku.webMaterial),
            ('capacity', sku.capacity),
            ('color', sku.itemColor),
            ('size', sku.itemSize),
            ('giftPack', sku.isGiftWrap),
        ])

        return sfcc_sku_record

    def add_hardgood_variations(self, variations_attributes, product: HardGoodProduct):

        slt_product_skus = product.childSkuDetails
        color_ind = (sku.itemColor for sku in slt_product_skus)
        all_have_colors = all(color_ind)
        size_ind = (sku.itemSize for sku in slt_product_skus)
        all_have_sizes = all(size_ind)
        if any([all_have_colors, all_have_sizes]):
            size_values = catalog.complexType_VariationAttribute_Values()
            size_attrib = sfcc.build_attribute('size', 'Size')
            size_attrib.append(size_values)
            color_values = catalog.complexType_VariationAttribute_Values()
            color_attrib = sfcc.build_attribute('color', 'Color')
            color_attrib.append(color_values)
            if all_have_sizes:
                variations_attributes.append(size_attrib)
            if all_have_colors:
                variations_attributes.append(color_attrib)
            for sku in slt_product_skus:
                size = sku.itemSize
                color = sku.itemColor
                if all_have_sizes and size:
                    variation_attribute_value = \
                        sfcc.build_variation_attribute_value(sfcc.id_from_value(size), size)
                    size_values.append(variation_attribute_value)
                if all_have_colors and color:
                    variation_attribute_value = \
                        sfcc. build_variation_attribute_value(sfcc.id_from_value(color), color)
                    color_values.append(variation_attribute_value)


@statsd.timer('create_hardgoods')
def create_hardgoods(hardgoods_product: HardGoodProduct, save=True, tracking_id=None):

    logger.info(f'create_hardgoods', extra={"tracking_id": tracking_id})
    products = HardGoodProducts(products=[hardgoods_product])
    return create_hardgoods_bulk(products, save)


@statsd.timer('create_hardgoods_bulk')
def create_hardgoods_bulk(hardgoods_products: HardGoodProducts, save=True, tracking_id=None):

    logger.info(f'create_hardgoods_bulk', extra={"tracking_id": tracking_id})
    doc = hardgoods_products.to_pyxb()
    xml = shopfront.to_xml(doc, CONFIG.hardgoods_namespace)
    path = shopfront.url_join(CONFIG.output_directory, CONFIG.hardgoods_directory)
    if save:
        shopfront.save(CONFIG.output_destination,
                       path,
                       shopfront.file_name(CONFIG.hardgoods_filename),
                       xml)
    return xml.decode("utf-8")
