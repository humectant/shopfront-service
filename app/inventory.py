import pyxb

from statsd import StatsClient
from typing import List
from pydantic import BaseModel
from app.util import shopfront
from app.schema import inventory as pyxb_inventory
from app.configuration import CONFIG

import logging

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-inventory')

logger = logging.getLogger('shopfront.inventory')
logger.setLevel(CONFIG.LOG_LEVEL)


class Record(BaseModel):

    title: str = None
    skuId: str
    storeId: str
    inventory: int
    inTransit: str = None
    onHand: str = None
    availableQty: int = None
    averageWeeklySales: str = None
    maxDays: int = None
    minDays: int = None
    storeIdeal: int = None
    storeMax: int = None
    storeMin: int = None
    reservedQty: int = None


class Inventory(BaseModel):
    inventory: List[Record]

    def sort(self):
        self.inventory = sorted(self.inventory, key=lambda k: k.storeId)

    def to_pyxb(self):

        product_inventory = pyxb_inventory.inventory()
        self.sort()
        store_id = None
        for inv_item in self.inventory:
            inventory_list = pyxb_inventory.complexType_InventoryList()
            product_inventory.append(inventory_list)
            inventory_list.header = pyxb_inventory.complexType_Header()
            inventory_list.records = pyxb_inventory.complexType_InventoryRecords()
            if store_id != inv_item.storeId:
                if inv_item.storeId == CONFIG.inventory_webstore_id:
                    inventory_list.header.list_id = "inventory-slt-webstore"
                    inventory_list.header.description = "SLT Webstore Inventory"
                else:
                    inventory_list.header.list_id = f"inventory_store_{inv_item.storeId}"
                    inventory_list.header.description = f"store {inv_item.storeId}"
                inventory_list.header.default_instock = False
                # inventory_list.header.use_bundle_inventory_only = False
                # inventory_list.header.on_order = False

            inventory_record = pyxb_inventory.complexType_InventoryRecord()
            inventory_record.allocation = inv_item.inventory
            inventory_record.product_id = inv_item.skuId

            dropShipInd = True

            # Price perpetual logic
            if dropShipInd:
                inventory_record.perpetual = True
            else:
                inventory_record.allocation = 1000
                inventory_record.perpetual = False

            # inventory_record.preorder_backorder_allocation = inv_item.get('preorder-backorder-allocation', 0)
            # inventory_record.preorder_backorder_handling = inv_item.get('preorder-backorder-handling', 'none')
            # inventory_record.in_stock_date = inv_item.get('in-stock-date', NO_VALUE)
            # inventory_record.perpetual = inv_item.get('perpetual', 'false')
            inventory_record.preorder_backorder_allocation = 0
            inventory_record.preorder_backorder_handling = 'none'
            inventory_record.perpetual = 'false'

            inventory_list.records.append(inventory_record)
            store_id = inv_item.storeId

        return product_inventory


@statsd.timer('create_inventory_bulk')
def create_inventory_bulk(inv: Inventory, save=True, tracking_id=None):

    logger.info(f'create_stores_bulk', extra={"tracking_id": tracking_id})
    doc = inv.to_pyxb()
    xml = shopfront.to_xml(doc, CONFIG.inventory_namespace)
    path = shopfront.url_join(
        CONFIG.output_directory, CONFIG.inventory_web_directory)
    if save:
        shopfront.save(CONFIG.output_destination,
                       path,
                       shopfront.file_name(CONFIG.inventory_webstore_delta),
                       xml)
    return xml.decode("utf-8")






