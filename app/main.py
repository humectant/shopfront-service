from fastapi import FastAPI, Header, BackgroundTasks, HTTPException
from app import stores
from app import assignments
from app import culinary
from app import hardgoods
from app import pricebooks
from app import taxonomy
from app import inventory
from app.util import shopfront
from app.configuration import CONFIG
from starlette.responses import HTMLResponse
import logging
import uvicorn
import paramiko
import socket
import pyxb

logger = logging.getLogger(__name__)

app = FastAPI(
    title="Shopfront Service",
    description="Facade to Salesforce Commerce Cloud",
    version="1.0",
    docs_url=f"/{CONFIG.service_root}/docs",
    redoc_url=f"/{CONFIG.service_root}/redoc",
    openapi_url=f"/{CONFIG.service_root}/openapi.json"
)


@app.get(f"/{CONFIG.service_root}/health")
def health():
    return {"status": "up"}


@app.post(f"/{CONFIG.service_root}/stores", content_type=HTMLResponse)
def create_store(store: stores.Store, tracking_id: str = Header(None)):
    xml = stores.create_store(store, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/stores/bulk", content_type=HTMLResponse)
def create_stores_bulk(stores_bulk: stores.Stores,
                       tracking_id: str = Header(None)):
    xml = stores.create_stores_bulk(stores_bulk, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/assignments/bulk", content_type=HTMLResponse)
def create_assignments_bulk(assignments_bulk: assignments.Assignments,
                            tracking_id: str = Header(None)):
    xml = assignments.create_assignments_bulk(assignments_bulk, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/culinary", content_type=HTMLResponse)
def create_culinary(culinary_product: culinary.CulinaryProduct,
                    tracking_id: str = Header(None)):
    xml = culinary.create_culinary(culinary_product, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/culinary/bulk", content_type=HTMLResponse)
def create_culinary_bulk(culinary_bulk: culinary.CulinaryProducts,
                         tracking_id: str = Header(None)):
    xml = culinary.create_culinary_bulk(culinary_bulk, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/hardgoods", content_type=HTMLResponse)
def create_hardgoods(hardgood_product: hardgoods.HardGoodProduct,
                     tracking_id: str = Header(None)):
    xml = hardgoods.create_hardgoods(hardgood_product, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/hardgoods/bulk", content_type=HTMLResponse)
def create_hardgoods_bulk(hardgood_bulk: hardgoods.HardGoodProducts,
                          tracking_id: str = Header(None)):
    xml = hardgoods.create_hardgoods_bulk(hardgood_bulk, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/pricebooks/bulk", content_type=HTMLResponse)
def create_pricebooks_bulk(pricebooks_bulk: pricebooks.PriceBooks,
                           tracking_id: str = Header(None)):
    xml = pricebooks.create_pricebooks_bulk(pricebooks_bulk, tracking_id=tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/pricebooks/async")
def create_pricebooks_async(pricebooks_bulk: pricebooks.PriceBooks,
                            background_tasks: BackgroundTasks,
                            tracking_id: str = Header(None)):
    documents = pricebooks.create_pricebooks_async(pricebooks_bulk, tracking_id=tracking_id)
    background_tasks.add_task(shopfront.save_documents, documents)
    return {'status': 'success', 'message': f'Queued Price Books for processing'}


@app.post(f"/{CONFIG.service_root}/taxonomy/bulk", content_type=HTMLResponse)
def create_taxonomy_bulk(taxonomy_bulk: taxonomy.Taxonomy,
                         tracking_id: str = Header(None)):
    xml = taxonomy.create_taxonomy_bulk(taxonomy_bulk, tracking_id)
    return xml


@app.post(f"/{CONFIG.service_root}/inventory/bulk", content_type=HTMLResponse)
def create_taxonomy_bulk(inventory_bulk: inventory.Inventory,
                         tracking_id: str = Header(None)):
    xml = inventory.create_inventory_bulk(inventory_bulk, tracking_id)
    return xml


def pyxb_handler(request, exc):
    logger.error('PyXB parsing error', exc_info=str(exc.details()))
    raise HTTPException(status_code=500, detail=str(exc.details()))


def ssh_handler(request, exc):
    logger.error('SSH error', exc_info=str(exc.__dict__))
    raise HTTPException(status_code=503, detail=str(exc.__dict__))


app.add_exception_handler(pyxb.PyXBException, pyxb_handler)
app.add_exception_handler(paramiko.ssh_exception.SSHException, ssh_handler)
app.add_exception_handler(socket.error, ssh_handler)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8008)

