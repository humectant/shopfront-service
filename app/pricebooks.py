import logging
import pyxb

from typing import List
from pydantic import BaseModel
from statsd import StatsClient
from app.configuration import CONFIG
from app.schema import pricebook as pyxb_pricebook
from app.util import shopfront

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-pricebooks')

logger = logging.getLogger('shopfront.pricebooks')
logger.setLevel(CONFIG.LOG_LEVEL)


class PriceBook(BaseModel):

    price: str
    type: str
    sku: str


class PriceBooks(BaseModel):

    pricebooks: List[PriceBook] = None

    def of_type(self, price_type):
        return [pricebook for pricebook in self.pricebooks
                if pricebook.type == price_type]


@statsd.timer('create_pricebooks')
def create_pricebooks(pricebook: PriceBook, save=True, tracking_id=None):

    logger.debug(f'create_pricebooks', extra={"tracking_id": tracking_id})
    pricebooks = PriceBooks(pricebooks=[pricebook])
    return create_pricebooks_bulk(pricebooks, save)


@statsd.timer('create_pricebooks_bulk')
def create_pricebooks_bulk(pricebooks_object: PriceBooks, save=True, tracking_id=None):

    logger.info(f'create_pricebooks_bulk', extra={"tracking_id": tracking_id})
    price_count = 0
    price_types = [('sale', 'shop-slt-sale-price', 'shop-slt-sale-price'),
                   ('regular', 'shop-slt-regular-price', 'shop-slt-regular-price'),
                   ('list', 'shop-slt-list-price', 'shop-slt-list-price')]

    all_prices = pyxb_pricebook.pricebooks()

    for price_type, pricebook_id, filename in price_types:
        price_books = pricebooks_object.of_type(price_type)
        price_count += len(price_books)

        some_prices = pyxb_pricebook.pricebooks()
        pricebook_node = to_pricebook(pricebook_id, price_books)
        some_prices.append(pricebook_node)
        all_prices.append(pricebook_node)

        xml = shopfront.to_xml(some_prices, CONFIG.pricebooks_namespace)
        path = shopfront.url_join(
            CONFIG.output_directory, CONFIG.pricebooks_directory)
        if save:
            shopfront.save(CONFIG.output_destination,
                           path,
                           shopfront.file_name(filename),
                           xml)

    xml = shopfront.to_xml(all_prices, CONFIG.pricebooks_namespace)
    return xml.decode("utf-8")


@statsd.timer('create_pricebooks_async')
def create_pricebooks_async(pricebooks_object: PriceBooks, tracking_id=None):

    logger.info(f'create_pricebooks_bulk', extra={"tracking_id": tracking_id})
    price_count = 0
    price_types = [('sale', 'shop-slt-sale-price', 'shop-slt-sale-price'),
                   ('regular', 'shop-slt-regular-price', 'shop-slt-regular-price'),
                   ('list', 'shop-slt-list-price', 'shop-slt-list-price')]

    all_prices = pyxb_pricebook.pricebooks()

    for price_type, pricebook_id, filename in price_types:
        price_books = pricebooks_object.of_type(price_type)
        price_count += len(price_books)

        some_prices = pyxb_pricebook.pricebooks()
        pricebook_node = to_pricebook(pricebook_id, price_books)
        some_prices.append(pricebook_node)

        all_prices.append(pricebook_node)

        xml = shopfront.to_xml(some_prices, CONFIG.pricebooks_namespace)
        path = shopfront.url_join(
            CONFIG.output_directory, CONFIG.pricebooks_directory)

        yield shopfront.Document(
            CONFIG.output_destination,
            path,
            shopfront.file_name(filename),
            xml)


def to_pricebook(pricebook_id, price_books, delete=False):

    pricebook_node = pyxb_pricebook.complexType_PriceBook()
    header = pyxb_pricebook.complexType_Header()
    pricebook_node.header = header
    header.pricebook_id = pricebook_id
    header.currency = "USD"
    price_tables = pyxb_pricebook.complexType_PriceTables()
    pricebook_node.price_tables = price_tables

    if delete:
        skus = set([price.sku for price in price_books])
        for sku in skus:
            delete_price_table = pyxb_pricebook.complexType_PriceTable()
            price_tables.append(delete_price_table)
            delete_price_table.product_id = sku
            delete_price_table.mode = 'delete-all'

    for price in price_books:
        price_table = pyxb_pricebook.complexType_PriceTable()
        price_tables.append(price_table)
        price_table.product_id = price.sku
        price_table.amount.append(price.price)
        amount = price_table.amount[-1]
        amount.quantity = 1

    return pricebook_node
