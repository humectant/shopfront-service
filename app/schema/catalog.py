# ./catalog.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:4b90aa085ddb83001f94a6476c9fe51f3fc86b69
# Generated 2019-02-09 15:07:44.986844 by PyXB version 1.2.6 using Python 3.7.1.final.0
# Namespace http://www.demandware.com/xml/impex/catalog/2006-10-31

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:8237aa92-2cbf-11e9-9482-784f43a65fa5')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes
import pyxb.binding.xml_

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://www.demandware.com/xml/impex/catalog/2006-10-31', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.ProductIDComparator
class simpleType_ProductIDComparator (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.ProductIDComparator')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 967, 4)
    _Documentation = None
simpleType_ProductIDComparator._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_ProductIDComparator, enum_prefix=None)
simpleType_ProductIDComparator.is_equal = simpleType_ProductIDComparator._CF_enumeration.addEnumeration(unicode_value='is equal', tag='is_equal')
simpleType_ProductIDComparator.is_not_equal = simpleType_ProductIDComparator._CF_enumeration.addEnumeration(unicode_value='is not equal', tag='is_not_equal')
simpleType_ProductIDComparator._InitializeFacetMap(simpleType_ProductIDComparator._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.ProductIDComparator', simpleType_ProductIDComparator)
_module_typeBindings.simpleType_ProductIDComparator = simpleType_ProductIDComparator

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.CategoryIDComparator
class simpleType_CategoryIDComparator (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.CategoryIDComparator')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 974, 4)
    _Documentation = None
simpleType_CategoryIDComparator._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_CategoryIDComparator, enum_prefix=None)
simpleType_CategoryIDComparator.is_equal = simpleType_CategoryIDComparator._CF_enumeration.addEnumeration(unicode_value='is equal', tag='is_equal')
simpleType_CategoryIDComparator.is_direct_child_of = simpleType_CategoryIDComparator._CF_enumeration.addEnumeration(unicode_value='is direct child of', tag='is_direct_child_of')
simpleType_CategoryIDComparator.is_child_of = simpleType_CategoryIDComparator._CF_enumeration.addEnumeration(unicode_value='is child of', tag='is_child_of')
simpleType_CategoryIDComparator.is_direct_parent_of = simpleType_CategoryIDComparator._CF_enumeration.addEnumeration(unicode_value='is direct parent of', tag='is_direct_parent_of')
simpleType_CategoryIDComparator.is_parent_of = simpleType_CategoryIDComparator._CF_enumeration.addEnumeration(unicode_value='is parent of', tag='is_parent_of')
simpleType_CategoryIDComparator.is_sibling_of = simpleType_CategoryIDComparator._CF_enumeration.addEnumeration(unicode_value='is sibling of', tag='is_sibling_of')
simpleType_CategoryIDComparator._InitializeFacetMap(simpleType_CategoryIDComparator._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.CategoryIDComparator', simpleType_CategoryIDComparator)
_module_typeBindings.simpleType_CategoryIDComparator = simpleType_CategoryIDComparator

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.BrandComparator
class simpleType_BrandComparator (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.BrandComparator')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 985, 4)
    _Documentation = None
simpleType_BrandComparator._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_BrandComparator, enum_prefix=None)
simpleType_BrandComparator.exists = simpleType_BrandComparator._CF_enumeration.addEnumeration(unicode_value='exists', tag='exists')
simpleType_BrandComparator.does_not_exist = simpleType_BrandComparator._CF_enumeration.addEnumeration(unicode_value='does not exist', tag='does_not_exist')
simpleType_BrandComparator.is_equal = simpleType_BrandComparator._CF_enumeration.addEnumeration(unicode_value='is equal', tag='is_equal')
simpleType_BrandComparator.is_not_equal = simpleType_BrandComparator._CF_enumeration.addEnumeration(unicode_value='is not equal', tag='is_not_equal')
simpleType_BrandComparator._InitializeFacetMap(simpleType_BrandComparator._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.BrandComparator', simpleType_BrandComparator)
_module_typeBindings.simpleType_BrandComparator = simpleType_BrandComparator

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.AttributeComparator
class simpleType_AttributeComparator (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.AttributeComparator')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 994, 4)
    _Documentation = None
simpleType_AttributeComparator._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_AttributeComparator, enum_prefix=None)
simpleType_AttributeComparator.less_than = simpleType_AttributeComparator._CF_enumeration.addEnumeration(unicode_value='less than', tag='less_than')
simpleType_AttributeComparator.greater_than = simpleType_AttributeComparator._CF_enumeration.addEnumeration(unicode_value='greater than', tag='greater_than')
simpleType_AttributeComparator.equals = simpleType_AttributeComparator._CF_enumeration.addEnumeration(unicode_value='equals', tag='equals')
simpleType_AttributeComparator.is_not_equal = simpleType_AttributeComparator._CF_enumeration.addEnumeration(unicode_value='is not equal', tag='is_not_equal')
simpleType_AttributeComparator.exists = simpleType_AttributeComparator._CF_enumeration.addEnumeration(unicode_value='exists', tag='exists')
simpleType_AttributeComparator.does_not_exist = simpleType_AttributeComparator._CF_enumeration.addEnumeration(unicode_value='does not exist', tag='does_not_exist')
simpleType_AttributeComparator._InitializeFacetMap(simpleType_AttributeComparator._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.AttributeComparator', simpleType_AttributeComparator)
_module_typeBindings.simpleType_AttributeComparator = simpleType_AttributeComparator

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.String
class simpleType_Generic_String (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1006, 4)
    _Documentation = None
simpleType_Generic_String._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String', simpleType_Generic_String)
_module_typeBindings.simpleType_Generic_String = simpleType_Generic_String

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Currency
class simpleType_Currency (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Currency')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1096, 4)
    _Documentation = None
simpleType_Currency._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Currency._CF_pattern.addPattern(pattern='[A-Z]{3}')
simpleType_Currency._InitializeFacetMap(simpleType_Currency._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Currency', simpleType_Currency)
_module_typeBindings.simpleType_Currency = simpleType_Currency

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.CategoryAssignment.Position
class simpleType_CategoryAssignment_Position (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.CategoryAssignment.Position')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1102, 4)
    _Documentation = None
simpleType_CategoryAssignment_Position._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_CategoryAssignment_Position, enum_prefix=None)
simpleType_CategoryAssignment_Position.none = simpleType_CategoryAssignment_Position._CF_enumeration.addEnumeration(unicode_value='none', tag='none')
simpleType_CategoryAssignment_Position.top = simpleType_CategoryAssignment_Position._CF_enumeration.addEnumeration(unicode_value='top', tag='top')
simpleType_CategoryAssignment_Position.bottom = simpleType_CategoryAssignment_Position._CF_enumeration.addEnumeration(unicode_value='bottom', tag='bottom')
simpleType_CategoryAssignment_Position.auto = simpleType_CategoryAssignment_Position._CF_enumeration.addEnumeration(unicode_value='auto', tag='auto')
simpleType_CategoryAssignment_Position._InitializeFacetMap(simpleType_CategoryAssignment_Position._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.CategoryAssignment.Position', simpleType_CategoryAssignment_Position)
_module_typeBindings.simpleType_CategoryAssignment_Position = simpleType_CategoryAssignment_Position

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Refinement.Type
class simpleType_Refinement_Type (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Refinement.Type')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1111, 4)
    _Documentation = None
simpleType_Refinement_Type._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Refinement_Type, enum_prefix=None)
simpleType_Refinement_Type.attribute = simpleType_Refinement_Type._CF_enumeration.addEnumeration(unicode_value='attribute', tag='attribute')
simpleType_Refinement_Type.category = simpleType_Refinement_Type._CF_enumeration.addEnumeration(unicode_value='category', tag='category')
simpleType_Refinement_Type.price = simpleType_Refinement_Type._CF_enumeration.addEnumeration(unicode_value='price', tag='price')
simpleType_Refinement_Type.promotion = simpleType_Refinement_Type._CF_enumeration.addEnumeration(unicode_value='promotion', tag='promotion')
simpleType_Refinement_Type._InitializeFacetMap(simpleType_Refinement_Type._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Refinement.Type', simpleType_Refinement_Type)
_module_typeBindings.simpleType_Refinement_Type = simpleType_Refinement_Type

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.UnbucketedValues.Mode
class simpleType_UnbucketedValues_Mode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.UnbucketedValues.Mode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1120, 4)
    _Documentation = None
simpleType_UnbucketedValues_Mode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_UnbucketedValues_Mode, enum_prefix=None)
simpleType_UnbucketedValues_Mode.show_values = simpleType_UnbucketedValues_Mode._CF_enumeration.addEnumeration(unicode_value='show-values', tag='show_values')
simpleType_UnbucketedValues_Mode.show_as_bucket = simpleType_UnbucketedValues_Mode._CF_enumeration.addEnumeration(unicode_value='show-as-bucket', tag='show_as_bucket')
simpleType_UnbucketedValues_Mode.hide = simpleType_UnbucketedValues_Mode._CF_enumeration.addEnumeration(unicode_value='hide', tag='hide')
simpleType_UnbucketedValues_Mode._InitializeFacetMap(simpleType_UnbucketedValues_Mode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.UnbucketedValues.Mode', simpleType_UnbucketedValues_Mode)
_module_typeBindings.simpleType_UnbucketedValues_Mode = simpleType_UnbucketedValues_Mode

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Refinement.ValueSet
class simpleType_Refinement_ValueSet (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Refinement.ValueSet')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1128, 4)
    _Documentation = None
simpleType_Refinement_ValueSet._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Refinement_ValueSet, enum_prefix=None)
simpleType_Refinement_ValueSet.search_result = simpleType_Refinement_ValueSet._CF_enumeration.addEnumeration(unicode_value='search-result', tag='search_result')
simpleType_Refinement_ValueSet.refinement_category = simpleType_Refinement_ValueSet._CF_enumeration.addEnumeration(unicode_value='refinement-category', tag='refinement_category')
simpleType_Refinement_ValueSet._InitializeFacetMap(simpleType_Refinement_ValueSet._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Refinement.ValueSet', simpleType_Refinement_ValueSet)
_module_typeBindings.simpleType_Refinement_ValueSet = simpleType_Refinement_ValueSet

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Bucket.Type
class simpleType_Bucket_Type (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Bucket.Type')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1135, 4)
    _Documentation = None
simpleType_Bucket_Type._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Bucket_Type, enum_prefix=None)
simpleType_Bucket_Type.none = simpleType_Bucket_Type._CF_enumeration.addEnumeration(unicode_value='none', tag='none')
simpleType_Bucket_Type.values_ = simpleType_Bucket_Type._CF_enumeration.addEnumeration(unicode_value='values', tag='values_')
simpleType_Bucket_Type.thresholds = simpleType_Bucket_Type._CF_enumeration.addEnumeration(unicode_value='thresholds', tag='thresholds')
simpleType_Bucket_Type.periods = simpleType_Bucket_Type._CF_enumeration.addEnumeration(unicode_value='periods', tag='periods')
simpleType_Bucket_Type._InitializeFacetMap(simpleType_Bucket_Type._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Bucket.Type', simpleType_Bucket_Type)
_module_typeBindings.simpleType_Bucket_Type = simpleType_Bucket_Type

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Sorting.Mode
class simpleType_Sorting_Mode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Sorting.Mode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1144, 4)
    _Documentation = None
simpleType_Sorting_Mode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Sorting_Mode, enum_prefix=None)
simpleType_Sorting_Mode.value_name = simpleType_Sorting_Mode._CF_enumeration.addEnumeration(unicode_value='value-name', tag='value_name')
simpleType_Sorting_Mode.value_count = simpleType_Sorting_Mode._CF_enumeration.addEnumeration(unicode_value='value-count', tag='value_count')
simpleType_Sorting_Mode.category_position = simpleType_Sorting_Mode._CF_enumeration.addEnumeration(unicode_value='category-position', tag='category_position')
simpleType_Sorting_Mode._InitializeFacetMap(simpleType_Sorting_Mode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Sorting.Mode', simpleType_Sorting_Mode)
_module_typeBindings.simpleType_Sorting_Mode = simpleType_Sorting_Mode

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Sorting.Direction
class simpleType_Sorting_Direction (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Sorting.Direction')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1152, 4)
    _Documentation = None
simpleType_Sorting_Direction._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Sorting_Direction, enum_prefix=None)
simpleType_Sorting_Direction.ascending = simpleType_Sorting_Direction._CF_enumeration.addEnumeration(unicode_value='ascending', tag='ascending')
simpleType_Sorting_Direction.descending = simpleType_Sorting_Direction._CF_enumeration.addEnumeration(unicode_value='descending', tag='descending')
simpleType_Sorting_Direction._InitializeFacetMap(simpleType_Sorting_Direction._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Sorting.Direction', simpleType_Sorting_Direction)
_module_typeBindings.simpleType_Sorting_Direction = simpleType_Sorting_Direction

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Category.LinkType
class simpleType_Category_LinkType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Category.LinkType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1159, 4)
    _Documentation = None
simpleType_Category_LinkType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Category_LinkType, enum_prefix=None)
simpleType_Category_LinkType.cross_sell = simpleType_Category_LinkType._CF_enumeration.addEnumeration(unicode_value='cross-sell', tag='cross_sell')
simpleType_Category_LinkType.up_sell = simpleType_Category_LinkType._CF_enumeration.addEnumeration(unicode_value='up-sell', tag='up_sell')
simpleType_Category_LinkType.accessory = simpleType_Category_LinkType._CF_enumeration.addEnumeration(unicode_value='accessory', tag='accessory')
simpleType_Category_LinkType.spare_part = simpleType_Category_LinkType._CF_enumeration.addEnumeration(unicode_value='spare-part', tag='spare_part')
simpleType_Category_LinkType.other = simpleType_Category_LinkType._CF_enumeration.addEnumeration(unicode_value='other', tag='other')
simpleType_Category_LinkType._InitializeFacetMap(simpleType_Category_LinkType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Category.LinkType', simpleType_Category_LinkType)
_module_typeBindings.simpleType_Category_LinkType = simpleType_Category_LinkType

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Product.LinkType
class simpleType_Product_LinkType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Product.LinkType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1169, 4)
    _Documentation = None
simpleType_Product_LinkType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Product_LinkType, enum_prefix=None)
simpleType_Product_LinkType.cross_sell = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='cross-sell', tag='cross_sell')
simpleType_Product_LinkType.replacement = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='replacement', tag='replacement')
simpleType_Product_LinkType.up_sell = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='up-sell', tag='up_sell')
simpleType_Product_LinkType.accessory = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='accessory', tag='accessory')
simpleType_Product_LinkType.spare_part = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='spare-part', tag='spare_part')
simpleType_Product_LinkType.newer_version = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='newer-version', tag='newer_version')
simpleType_Product_LinkType.alt_orderunit = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='alt-orderunit', tag='alt_orderunit')
simpleType_Product_LinkType.other = simpleType_Product_LinkType._CF_enumeration.addEnumeration(unicode_value='other', tag='other')
simpleType_Product_LinkType._InitializeFacetMap(simpleType_Product_LinkType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Product.LinkType', simpleType_Product_LinkType)
_module_typeBindings.simpleType_Product_LinkType = simpleType_Product_LinkType

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Option.SortMode
class simpleType_Option_SortMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Option.SortMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1182, 4)
    _Documentation = None
simpleType_Option_SortMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_Option_SortMode, enum_prefix=None)
simpleType_Option_SortMode.position = simpleType_Option_SortMode._CF_enumeration.addEnumeration(unicode_value='position', tag='position')
simpleType_Option_SortMode.price = simpleType_Option_SortMode._CF_enumeration.addEnumeration(unicode_value='price', tag='price')
simpleType_Option_SortMode._InitializeFacetMap(simpleType_Option_SortMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.Option.SortMode', simpleType_Option_SortMode)
_module_typeBindings.simpleType_Option_SortMode = simpleType_Option_SortMode

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.ImportMode
class simpleType_ImportMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.ImportMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1196, 4)
    _Documentation = None
simpleType_ImportMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_ImportMode, enum_prefix=None)
simpleType_ImportMode.delete = simpleType_ImportMode._CF_enumeration.addEnumeration(unicode_value='delete', tag='delete')
simpleType_ImportMode._InitializeFacetMap(simpleType_ImportMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.ImportMode', simpleType_ImportMode)
_module_typeBindings.simpleType_ImportMode = simpleType_ImportMode

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.MergeMode
class simpleType_MergeMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """Used to control the behavior of list elements in import file."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.MergeMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1202, 4)
    _Documentation = 'Used to control the behavior of list elements in import file.'
simpleType_MergeMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_MergeMode, enum_prefix=None)
simpleType_MergeMode.add = simpleType_MergeMode._CF_enumeration.addEnumeration(unicode_value='add', tag='add')
simpleType_MergeMode.merge = simpleType_MergeMode._CF_enumeration.addEnumeration(unicode_value='merge', tag='merge')
simpleType_MergeMode.remove = simpleType_MergeMode._CF_enumeration.addEnumeration(unicode_value='remove', tag='remove')
simpleType_MergeMode.replace = simpleType_MergeMode._CF_enumeration.addEnumeration(unicode_value='replace', tag='replace')
simpleType_MergeMode._InitializeFacetMap(simpleType_MergeMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.MergeMode', simpleType_MergeMode)
_module_typeBindings.simpleType_MergeMode = simpleType_MergeMode

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.RecommendationSourceType
class simpleType_RecommendationSourceType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.RecommendationSourceType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1234, 4)
    _Documentation = None
simpleType_RecommendationSourceType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_RecommendationSourceType, enum_prefix=None)
simpleType_RecommendationSourceType.product = simpleType_RecommendationSourceType._CF_enumeration.addEnumeration(unicode_value='product', tag='product')
simpleType_RecommendationSourceType.category = simpleType_RecommendationSourceType._CF_enumeration.addEnumeration(unicode_value='category', tag='category')
simpleType_RecommendationSourceType._InitializeFacetMap(simpleType_RecommendationSourceType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.RecommendationSourceType', simpleType_RecommendationSourceType)
_module_typeBindings.simpleType_RecommendationSourceType = simpleType_RecommendationSourceType

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.RecommendationTargetType
class simpleType_RecommendationTargetType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.RecommendationTargetType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1241, 4)
    _Documentation = None
simpleType_RecommendationTargetType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_RecommendationTargetType, enum_prefix=None)
simpleType_RecommendationTargetType.product = simpleType_RecommendationTargetType._CF_enumeration.addEnumeration(unicode_value='product', tag='product')
simpleType_RecommendationTargetType._InitializeFacetMap(simpleType_RecommendationTargetType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.RecommendationTargetType', simpleType_RecommendationTargetType)
_module_typeBindings.simpleType_RecommendationTargetType = simpleType_RecommendationTargetType

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.SiteMapChangeFrequency
class simpleType_SiteMapChangeFrequency (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.SiteMapChangeFrequency')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1247, 4)
    _Documentation = None
simpleType_SiteMapChangeFrequency._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_SiteMapChangeFrequency, enum_prefix=None)
simpleType_SiteMapChangeFrequency.always = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='always', tag='always')
simpleType_SiteMapChangeFrequency.hourly = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='hourly', tag='hourly')
simpleType_SiteMapChangeFrequency.daily = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='daily', tag='daily')
simpleType_SiteMapChangeFrequency.weekly = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='weekly', tag='weekly')
simpleType_SiteMapChangeFrequency.monthly = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='monthly', tag='monthly')
simpleType_SiteMapChangeFrequency.yearly = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='yearly', tag='yearly')
simpleType_SiteMapChangeFrequency.never = simpleType_SiteMapChangeFrequency._CF_enumeration.addEnumeration(unicode_value='never', tag='never')
simpleType_SiteMapChangeFrequency._InitializeFacetMap(simpleType_SiteMapChangeFrequency._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.SiteMapChangeFrequency', simpleType_SiteMapChangeFrequency)
_module_typeBindings.simpleType_SiteMapChangeFrequency = simpleType_SiteMapChangeFrequency

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.SiteMapPriority
class simpleType_SiteMapPriority (pyxb.binding.datatypes.double):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.SiteMapPriority')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1259, 4)
    _Documentation = None
simpleType_SiteMapPriority._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=simpleType_SiteMapPriority, value=pyxb.binding.datatypes.double(0.0))
simpleType_SiteMapPriority._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=simpleType_SiteMapPriority, value=pyxb.binding.datatypes.double(1.0))
simpleType_SiteMapPriority._InitializeFacetMap(simpleType_SiteMapPriority._CF_minInclusive,
   simpleType_SiteMapPriority._CF_maxInclusive)
Namespace.addCategoryObject('typeBinding', 'simpleType.SiteMapPriority', simpleType_SiteMapPriority)
_module_typeBindings.simpleType_SiteMapPriority = simpleType_SiteMapPriority

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.VariationGroupsDisplayMode
class simpleType_VariationGroupsDisplayMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.VariationGroupsDisplayMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1266, 4)
    _Documentation = None
simpleType_VariationGroupsDisplayMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_VariationGroupsDisplayMode, enum_prefix=None)
simpleType_VariationGroupsDisplayMode.individual = simpleType_VariationGroupsDisplayMode._CF_enumeration.addEnumeration(unicode_value='individual', tag='individual')
simpleType_VariationGroupsDisplayMode.merged = simpleType_VariationGroupsDisplayMode._CF_enumeration.addEnumeration(unicode_value='merged', tag='merged')
simpleType_VariationGroupsDisplayMode._InitializeFacetMap(simpleType_VariationGroupsDisplayMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.VariationGroupsDisplayMode', simpleType_VariationGroupsDisplayMode)
_module_typeBindings.simpleType_VariationGroupsDisplayMode = simpleType_VariationGroupsDisplayMode

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.AttributeType
class simpleType_AttributeType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.AttributeType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1273, 4)
    _Documentation = None
simpleType_AttributeType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_AttributeType, enum_prefix=None)
simpleType_AttributeType.string = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='string', tag='string')
simpleType_AttributeType.int = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='int', tag='int')
simpleType_AttributeType.double = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='double', tag='double')
simpleType_AttributeType.text = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='text', tag='text')
simpleType_AttributeType.html = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='html', tag='html')
simpleType_AttributeType.image = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='image', tag='image')
simpleType_AttributeType.boolean = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='boolean', tag='boolean')
simpleType_AttributeType.date = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='date', tag='date')
simpleType_AttributeType.datetime = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='datetime', tag='datetime')
simpleType_AttributeType.email = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='email', tag='email')
simpleType_AttributeType.password = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='password', tag='password')
simpleType_AttributeType.set_of_string = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='set-of-string', tag='set_of_string')
simpleType_AttributeType.set_of_int = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='set-of-int', tag='set_of_int')
simpleType_AttributeType.set_of_double = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='set-of-double', tag='set_of_double')
simpleType_AttributeType.enum_of_string = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='enum-of-string', tag='enum_of_string')
simpleType_AttributeType.enum_of_int = simpleType_AttributeType._CF_enumeration.addEnumeration(unicode_value='enum-of-int', tag='enum_of_int')
simpleType_AttributeType._InitializeFacetMap(simpleType_AttributeType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.AttributeType', simpleType_AttributeType)
_module_typeBindings.simpleType_AttributeType = simpleType_AttributeType

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.String.10
class simpleType_Generic_String_10 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.10')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1010, 4)
    _Documentation = None
simpleType_Generic_String_10._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_10._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(10))
simpleType_Generic_String_10._InitializeFacetMap(simpleType_Generic_String_10._CF_minLength,
   simpleType_Generic_String_10._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.10', simpleType_Generic_String_10)
_module_typeBindings.simpleType_Generic_String_10 = simpleType_Generic_String_10

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.String.28
class simpleType_Generic_String_28 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.28')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1017, 4)
    _Documentation = None
simpleType_Generic_String_28._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_28._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(28))
simpleType_Generic_String_28._InitializeFacetMap(simpleType_Generic_String_28._CF_minLength,
   simpleType_Generic_String_28._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.28', simpleType_Generic_String_28)
_module_typeBindings.simpleType_Generic_String_28 = simpleType_Generic_String_28

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.String.60
class simpleType_Generic_String_60 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.60')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1024, 4)
    _Documentation = None
simpleType_Generic_String_60._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_60._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(60))
simpleType_Generic_String_60._InitializeFacetMap(simpleType_Generic_String_60._CF_minLength,
   simpleType_Generic_String_60._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.60', simpleType_Generic_String_60)
_module_typeBindings.simpleType_Generic_String_60 = simpleType_Generic_String_60

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.String.256
class simpleType_Generic_String_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1031, 4)
    _Documentation = None
simpleType_Generic_String_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_String_256._InitializeFacetMap(simpleType_Generic_String_256._CF_minLength,
   simpleType_Generic_String_256._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.256', simpleType_Generic_String_256)
_module_typeBindings.simpleType_Generic_String_256 = simpleType_Generic_String_256

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.String.4000
class simpleType_Generic_String_4000 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.4000')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1038, 4)
    _Documentation = None
simpleType_Generic_String_4000._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_4000._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(4000))
simpleType_Generic_String_4000._InitializeFacetMap(simpleType_Generic_String_4000._CF_minLength,
   simpleType_Generic_String_4000._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.4000', simpleType_Generic_String_4000)
_module_typeBindings.simpleType_Generic_String_4000 = simpleType_Generic_String_4000

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.NonEmptyString.32
class simpleType_Generic_NonEmptyString_32 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.32')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1046, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_32._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_32._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(32))
simpleType_Generic_NonEmptyString_32._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_32._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_32._InitializeFacetMap(simpleType_Generic_NonEmptyString_32._CF_minLength,
   simpleType_Generic_NonEmptyString_32._CF_maxLength,
   simpleType_Generic_NonEmptyString_32._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.32', simpleType_Generic_NonEmptyString_32)
_module_typeBindings.simpleType_Generic_NonEmptyString_32 = simpleType_Generic_NonEmptyString_32

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.NonEmptyString.100
class simpleType_Generic_NonEmptyString_100 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.100')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1054, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_100._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_100._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(100))
simpleType_Generic_NonEmptyString_100._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_100._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_100._InitializeFacetMap(simpleType_Generic_NonEmptyString_100._CF_minLength,
   simpleType_Generic_NonEmptyString_100._CF_maxLength,
   simpleType_Generic_NonEmptyString_100._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.100', simpleType_Generic_NonEmptyString_100)
_module_typeBindings.simpleType_Generic_NonEmptyString_100 = simpleType_Generic_NonEmptyString_100

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.NonEmptyString.256
class simpleType_Generic_NonEmptyString_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1062, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_NonEmptyString_256._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_256._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_256._InitializeFacetMap(simpleType_Generic_NonEmptyString_256._CF_minLength,
   simpleType_Generic_NonEmptyString_256._CF_maxLength,
   simpleType_Generic_NonEmptyString_256._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.256', simpleType_Generic_NonEmptyString_256)
_module_typeBindings.simpleType_Generic_NonEmptyString_256 = simpleType_Generic_NonEmptyString_256

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.NonEmptyString.1800
class simpleType_Generic_NonEmptyString_1800 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.1800')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1070, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_1800._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_1800._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(1800))
simpleType_Generic_NonEmptyString_1800._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_1800._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_1800._InitializeFacetMap(simpleType_Generic_NonEmptyString_1800._CF_minLength,
   simpleType_Generic_NonEmptyString_1800._CF_maxLength,
   simpleType_Generic_NonEmptyString_1800._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.1800', simpleType_Generic_NonEmptyString_1800)
_module_typeBindings.simpleType_Generic_NonEmptyString_1800 = simpleType_Generic_NonEmptyString_1800

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.Generic.NonEmptyString.4000
class simpleType_Generic_NonEmptyString_4000 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.4000')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1079, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_4000._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_4000._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(4000))
simpleType_Generic_NonEmptyString_4000._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_4000._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_4000._InitializeFacetMap(simpleType_Generic_NonEmptyString_4000._CF_minLength,
   simpleType_Generic_NonEmptyString_4000._CF_maxLength,
   simpleType_Generic_NonEmptyString_4000._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.4000', simpleType_Generic_NonEmptyString_4000)
_module_typeBindings.simpleType_Generic_NonEmptyString_4000 = simpleType_Generic_NonEmptyString_4000

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.AttributeDefinition.ID
class simpleType_AttributeDefinition_ID (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.AttributeDefinition.ID')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1088, 4)
    _Documentation = None
simpleType_AttributeDefinition_ID._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_AttributeDefinition_ID._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_AttributeDefinition_ID._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_AttributeDefinition_ID._CF_pattern.addPattern(pattern='[^\\s|]|([^\\s|][^|]*[^\\s|])')
simpleType_AttributeDefinition_ID._InitializeFacetMap(simpleType_AttributeDefinition_ID._CF_minLength,
   simpleType_AttributeDefinition_ID._CF_maxLength,
   simpleType_AttributeDefinition_ID._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.AttributeDefinition.ID', simpleType_AttributeDefinition_ID)
_module_typeBindings.simpleType_AttributeDefinition_ID = simpleType_AttributeDefinition_ID

# Atomic simple type: {http://www.demandware.com/xml/impex/catalog/2006-10-31}simpleType.RecommendationType
class simpleType_RecommendationType (simpleType_Generic_String):

    """Allowed string values are the 'int' values defined for the Recommendation.recommendationType attribute, and additionally the values 'cross-sell', 'up-sell', and 'other' for backward compatibility."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.RecommendationType')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 1189, 4)
    _Documentation = "Allowed string values are the 'int' values defined for the Recommendation.recommendationType attribute, and additionally the values 'cross-sell', 'up-sell', and 'other' for backward compatibility."
simpleType_RecommendationType._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'simpleType.RecommendationType', simpleType_RecommendationType)
_module_typeBindings.simpleType_RecommendationType = simpleType_RecommendationType

# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Header with content type ELEMENT_ONLY
class complexType_Header (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Header with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Header')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 40, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image-settings uses Python identifier image_settings
    __image_settings = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image-settings'), 'image_settings', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexcatalog2006_10_31image_settings', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 42, 12), )

    
    image_settings = property(__image_settings.value, __image_settings.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 43, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    _ElementMap.update({
        __image_settings.name() : __image_settings,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Header = complexType_Header
Namespace.addCategoryObject('typeBinding', 'complexType.Header', complexType_Header)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.Settings with content type ELEMENT_ONLY
class complexType_Image_Settings (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.Settings with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Image.Settings')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 47, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}internal-location uses Python identifier internal_location
    __internal_location = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'internal-location'), 'internal_location', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_Settings_httpwww_demandware_comxmlimpexcatalog2006_10_31internal_location', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 50, 16), )

    
    internal_location = property(__internal_location.value, __internal_location.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}external-location uses Python identifier external_location
    __external_location = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'external-location'), 'external_location', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_Settings_httpwww_demandware_comxmlimpexcatalog2006_10_31external_location', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 51, 16), )

    
    external_location = property(__external_location.value, __external_location.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}view-types uses Python identifier view_types
    __view_types = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'view-types'), 'view_types', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_Settings_httpwww_demandware_comxmlimpexcatalog2006_10_31view_types', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 53, 12), )

    
    view_types = property(__view_types.value, __view_types.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-attribute-id uses Python identifier variation_attribute_id
    __variation_attribute_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-id'), 'variation_attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_Settings_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_attribute_id', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 54, 12), )

    
    variation_attribute_id = property(__variation_attribute_id.value, __variation_attribute_id.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}alt-pattern uses Python identifier alt_pattern
    __alt_pattern = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'alt-pattern'), 'alt_pattern', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_Settings_httpwww_demandware_comxmlimpexcatalog2006_10_31alt_pattern', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 55, 12), )

    
    alt_pattern = property(__alt_pattern.value, __alt_pattern.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}title-pattern uses Python identifier title_pattern
    __title_pattern = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'title-pattern'), 'title_pattern', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_Settings_httpwww_demandware_comxmlimpexcatalog2006_10_31title_pattern', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 56, 12), )

    
    title_pattern = property(__title_pattern.value, __title_pattern.set, None, None)

    _ElementMap.update({
        __internal_location.name() : __internal_location,
        __external_location.name() : __external_location,
        __view_types.name() : __view_types,
        __variation_attribute_id.name() : __variation_attribute_id,
        __alt_pattern.name() : __alt_pattern,
        __title_pattern.name() : __title_pattern
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Image_Settings = complexType_Image_Settings
Namespace.addCategoryObject('typeBinding', 'complexType.Image.Settings', complexType_Image_Settings)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.ViewTypes with content type ELEMENT_ONLY
class complexType_Image_ViewTypes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.ViewTypes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Image.ViewTypes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 60, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}view-type uses Python identifier view_type
    __view_type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'view-type'), 'view_type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_ViewTypes_httpwww_demandware_comxmlimpexcatalog2006_10_31view_type', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 62, 12), )

    
    view_type = property(__view_type.value, __view_type.set, None, None)

    _ElementMap.update({
        __view_type.name() : __view_type
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Image_ViewTypes = complexType_Image_ViewTypes
Namespace.addCategoryObject('typeBinding', 'complexType.Image.ViewTypes', complexType_Image_ViewTypes)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.ExternalLocation with content type ELEMENT_ONLY
class complexType_Image_ExternalLocation (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.ExternalLocation with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Image.ExternalLocation')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 70, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}http-url uses Python identifier http_url
    __http_url = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'http-url'), 'http_url', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_ExternalLocation_httpwww_demandware_comxmlimpexcatalog2006_10_31http_url', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 72, 12), )

    
    http_url = property(__http_url.value, __http_url.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}https-url uses Python identifier https_url
    __https_url = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'https-url'), 'https_url', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_ExternalLocation_httpwww_demandware_comxmlimpexcatalog2006_10_31https_url', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 73, 12), )

    
    https_url = property(__https_url.value, __https_url.set, None, None)

    _ElementMap.update({
        __http_url.name() : __http_url,
        __https_url.name() : __https_url
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Image_ExternalLocation = complexType_Image_ExternalLocation
Namespace.addCategoryObject('typeBinding', 'complexType.Image.ExternalLocation', complexType_Image_ExternalLocation)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeDefinitions with content type ELEMENT_ONLY
class complexType_AttributeDefinitions (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeDefinitions with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AttributeDefinitions')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 77, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute-definition uses Python identifier attribute_definition
    __attribute_definition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute-definition'), 'attribute_definition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinitions_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute_definition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 79, 12), )

    
    attribute_definition = property(__attribute_definition.value, __attribute_definition.set, None, None)

    _ElementMap.update({
        __attribute_definition.name() : __attribute_definition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_AttributeDefinitions = complexType_AttributeDefinitions
Namespace.addCategoryObject('typeBinding', 'complexType.AttributeDefinitions', complexType_AttributeDefinitions)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.StoreAttributes with content type ELEMENT_ONLY
class complexType_Product_StoreAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.StoreAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.StoreAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 294, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}receipt-name uses Python identifier receipt_name
    __receipt_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'receipt-name'), 'receipt_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_StoreAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31receipt_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 296, 12), )

    
    receipt_name = property(__receipt_name.value, __receipt_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}tax-class uses Python identifier tax_class
    __tax_class = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'tax-class'), 'tax_class', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_StoreAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31tax_class', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 297, 12), )

    
    tax_class = property(__tax_class.value, __tax_class.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}force-price-flag uses Python identifier force_price_flag
    __force_price_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'force-price-flag'), 'force_price_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_StoreAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31force_price_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 298, 12), )

    
    force_price_flag = property(__force_price_flag.value, __force_price_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}non-inventory-flag uses Python identifier non_inventory_flag
    __non_inventory_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'non-inventory-flag'), 'non_inventory_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_StoreAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31non_inventory_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 299, 12), )

    
    non_inventory_flag = property(__non_inventory_flag.value, __non_inventory_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}non-revenue-flag uses Python identifier non_revenue_flag
    __non_revenue_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'non-revenue-flag'), 'non_revenue_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_StoreAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31non_revenue_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 300, 12), )

    
    non_revenue_flag = property(__non_revenue_flag.value, __non_revenue_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}non-discountable-flag uses Python identifier non_discountable_flag
    __non_discountable_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'non-discountable-flag'), 'non_discountable_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_StoreAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31non_discountable_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 301, 12), )

    
    non_discountable_flag = property(__non_discountable_flag.value, __non_discountable_flag.set, None, None)

    _ElementMap.update({
        __receipt_name.name() : __receipt_name,
        __tax_class.name() : __tax_class,
        __force_price_flag.name() : __force_price_flag,
        __non_inventory_flag.name() : __non_inventory_flag,
        __non_revenue_flag.name() : __non_revenue_flag,
        __non_discountable_flag.name() : __non_discountable_flag
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_StoreAttributes = complexType_Product_StoreAttributes
Namespace.addCategoryObject('typeBinding', 'complexType.Product.StoreAttributes', complexType_Product_StoreAttributes)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryLinks with content type ELEMENT_ONLY
class complexType_CategoryLinks (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryLinks with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.CategoryLinks')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 406, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-link uses Python identifier category_link
    __category_link = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-link'), 'category_link', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryLinks_httpwww_demandware_comxmlimpexcatalog2006_10_31category_link', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 408, 12), )

    
    category_link = property(__category_link.value, __category_link.set, None, None)

    _ElementMap.update({
        __category_link.name() : __category_link
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_CategoryLinks = complexType_CategoryLinks
Namespace.addCategoryObject('typeBinding', 'complexType.CategoryLinks', complexType_CategoryLinks)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.BundledProducts with content type ELEMENT_ONLY
class complexType_Product_BundledProducts (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.BundledProducts with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.BundledProducts')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 419, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}bundled-product uses Python identifier bundled_product
    __bundled_product = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'bundled-product'), 'bundled_product', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_BundledProducts_httpwww_demandware_comxmlimpexcatalog2006_10_31bundled_product', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 421, 12), )

    
    bundled_product = property(__bundled_product.value, __bundled_product.set, None, None)

    _ElementMap.update({
        __bundled_product.name() : __bundled_product
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_BundledProducts = complexType_Product_BundledProducts
Namespace.addCategoryObject('typeBinding', 'complexType.Product.BundledProducts', complexType_Product_BundledProducts)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.RetailSetProducts with content type ELEMENT_ONLY
class complexType_Product_RetailSetProducts (pyxb.binding.basis.complexTypeDefinition):
    """Deprecated please use the element product-set-products (complexType.Product.ProductSetProducts)."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.RetailSetProducts')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 433, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}retail-set-product uses Python identifier retail_set_product
    __retail_set_product = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'retail-set-product'), 'retail_set_product', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_RetailSetProducts_httpwww_demandware_comxmlimpexcatalog2006_10_31retail_set_product', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 438, 12), )

    
    retail_set_product = property(__retail_set_product.value, __retail_set_product.set, None, None)

    _ElementMap.update({
        __retail_set_product.name() : __retail_set_product
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_RetailSetProducts = complexType_Product_RetailSetProducts
Namespace.addCategoryObject('typeBinding', 'complexType.Product.RetailSetProducts', complexType_Product_RetailSetProducts)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductSetProducts with content type ELEMENT_ONLY
class complexType_Product_ProductSetProducts (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductSetProducts with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ProductSetProducts')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 453, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-set-product uses Python identifier product_set_product
    __product_set_product = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-set-product'), 'product_set_product', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ProductSetProducts_httpwww_demandware_comxmlimpexcatalog2006_10_31product_set_product', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 455, 12), )

    
    product_set_product = property(__product_set_product.value, __product_set_product.set, None, None)

    _ElementMap.update({
        __product_set_product.name() : __product_set_product
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_ProductSetProducts = complexType_Product_ProductSetProducts
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ProductSetProducts', complexType_Product_ProductSetProducts)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.CategoryLinks with content type ELEMENT_ONLY
class complexType_Product_CategoryLinks (pyxb.binding.basis.complexTypeDefinition):
    """Deprecated please use the elements category-assignment (complexType.CategoryAssignment) and classification-category (complexType.Product.ClassificationCategory)."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.CategoryLinks')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 464, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}classification-link uses Python identifier classification_link
    __classification_link = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'classification-link'), 'classification_link', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_CategoryLinks_httpwww_demandware_comxmlimpexcatalog2006_10_31classification_link', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 469, 12), )

    
    classification_link = property(__classification_link.value, __classification_link.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-link uses Python identifier category_link
    __category_link = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-link'), 'category_link', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_CategoryLinks_httpwww_demandware_comxmlimpexcatalog2006_10_31category_link', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 470, 12), )

    
    category_link = property(__category_link.value, __category_link.set, None, None)

    _ElementMap.update({
        __classification_link.name() : __classification_link,
        __category_link.name() : __category_link
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_CategoryLinks = complexType_Product_CategoryLinks
Namespace.addCategoryObject('typeBinding', 'complexType.Product.CategoryLinks', complexType_Product_CategoryLinks)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductLinks with content type ELEMENT_ONLY
class complexType_Product_ProductLinks (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductLinks with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ProductLinks')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 495, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-link uses Python identifier product_link
    __product_link = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-link'), 'product_link', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ProductLinks_httpwww_demandware_comxmlimpexcatalog2006_10_31product_link', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 497, 12), )

    
    product_link = property(__product_link.value, __product_link.set, None, None)

    _ElementMap.update({
        __product_link.name() : __product_link
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_ProductLinks = complexType_Product_ProductLinks
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ProductLinks', complexType_Product_ProductLinks)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PageAttributes with content type ELEMENT_ONLY
class complexType_PageAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PageAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PageAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 507, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-title uses Python identifier page_title
    __page_title = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-title'), 'page_title', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PageAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31page_title', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 509, 12), )

    
    page_title = property(__page_title.value, __page_title.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-description uses Python identifier page_description
    __page_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-description'), 'page_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PageAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31page_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 510, 12), )

    
    page_description = property(__page_description.value, __page_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-keywords uses Python identifier page_keywords
    __page_keywords = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-keywords'), 'page_keywords', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PageAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31page_keywords', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 511, 12), )

    
    page_keywords = property(__page_keywords.value, __page_keywords.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-url uses Python identifier page_url
    __page_url = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-url'), 'page_url', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PageAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31page_url', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 512, 12), )

    
    page_url = property(__page_url.value, __page_url.set, None, None)

    _ElementMap.update({
        __page_title.name() : __page_title,
        __page_description.name() : __page_description,
        __page_keywords.name() : __page_keywords,
        __page_url.name() : __page_url
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_PageAttributes = complexType_PageAttributes
Namespace.addCategoryObject('typeBinding', 'complexType.PageAttributes', complexType_PageAttributes)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations with content type ELEMENT_ONLY
class complexType_Product_Variations (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 517, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attributes uses Python identifier attributes
    __attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attributes'), 'attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_httpwww_demandware_comxmlimpexcatalog2006_10_31attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 519, 12), )

    
    attributes = property(__attributes.value, __attributes.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variants uses Python identifier variants
    __variants = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variants'), 'variants', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_httpwww_demandware_comxmlimpexcatalog2006_10_31variants', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 520, 12), )

    
    variants = property(__variants.value, __variants.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-groups uses Python identifier variation_groups
    __variation_groups = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-groups'), 'variation_groups', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_groups', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 521, 12), )

    
    variation_groups = property(__variation_groups.value, __variation_groups.set, None, None)

    _ElementMap.update({
        __attributes.name() : __attributes,
        __variants.name() : __variants,
        __variation_groups.name() : __variation_groups
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_Variations = complexType_Product_Variations
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations', complexType_Product_Variations)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Attributes with content type ELEMENT_ONLY
class complexType_Product_Variations_Attributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Attributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.Attributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 525, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute uses Python identifier attribute
    __attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute'), 'attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 527, 12), )

    
    attribute = property(__attribute.value, __attribute.set, None, 'Deprecated please use the elements variation-attribute (complexType.VariationAttribute) or shared-variation-attribute (complexType.Product.VariationAttribute.Reference).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-attribute uses Python identifier variation_attribute
    __variation_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute'), 'variation_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 532, 12), )

    
    variation_attribute = property(__variation_attribute.value, __variation_attribute.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}shared-variation-attribute uses Python identifier shared_variation_attribute
    __shared_variation_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'shared-variation-attribute'), 'shared_variation_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_httpwww_demandware_comxmlimpexcatalog2006_10_31shared_variation_attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 533, 12), )

    
    shared_variation_attribute = property(__shared_variation_attribute.value, __shared_variation_attribute.set, None, None)

    _ElementMap.update({
        __attribute.name() : __attribute,
        __variation_attribute.name() : __variation_attribute,
        __shared_variation_attribute.name() : __shared_variation_attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_Variations_Attributes = complexType_Product_Variations_Attributes
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.Attributes', complexType_Product_Variations_Attributes)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment.VariationAssignments with content type ELEMENT_ONLY
class complexType_CategoryAssignment_VariationAssignments (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment.VariationAssignments with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.CategoryAssignment.VariationAssignments')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 590, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-assignment uses Python identifier variation_assignment
    __variation_assignment = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment'), 'variation_assignment', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_VariationAssignments_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_assignment', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 593, 12), )

    
    variation_assignment = property(__variation_assignment.value, __variation_assignment.set, None, None)

    _ElementMap.update({
        __variation_assignment.name() : __variation_assignment
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_CategoryAssignment_VariationAssignments = complexType_CategoryAssignment_VariationAssignments
Namespace.addCategoryObject('typeBinding', 'complexType.CategoryAssignment.VariationAssignments', complexType_CategoryAssignment_VariationAssignments)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment.VariationAssignment.Values with content type ELEMENT_ONLY
class complexType_CategoryAssignment_VariationAssignment_Values (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment.VariationAssignment.Values with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.CategoryAssignment.VariationAssignment.Values')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 607, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-assignment-value uses Python identifier variation_assignment_value
    __variation_assignment_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment-value'), 'variation_assignment_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_VariationAssignment_Values_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_assignment_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 609, 12), )

    
    variation_assignment_value = property(__variation_assignment_value.value, __variation_assignment_value.set, None, None)

    _ElementMap.update({
        __variation_assignment_value.name() : __variation_assignment_value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_CategoryAssignment_VariationAssignment_Values = complexType_CategoryAssignment_VariationAssignment_Values
Namespace.addCategoryObject('typeBinding', 'complexType.CategoryAssignment.VariationAssignment.Values', complexType_CategoryAssignment_VariationAssignment_Values)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeGroups with content type ELEMENT_ONLY
class complexType_AttributeGroups (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeGroups with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AttributeGroups')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 614, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute-group uses Python identifier attribute_group
    __attribute_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute-group'), 'attribute_group', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeGroups_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute_group', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 616, 12), )

    
    attribute_group = property(__attribute_group.value, __attribute_group.set, None, None)

    _ElementMap.update({
        __attribute_group.name() : __attribute_group
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_AttributeGroups = complexType_AttributeGroups
Namespace.addCategoryObject('typeBinding', 'complexType.AttributeGroups', complexType_AttributeGroups)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Attributes.Attribute.Values with content type ELEMENT_ONLY
class complexType_Product_Variations_Attributes_Attribute_Values (pyxb.binding.basis.complexTypeDefinition):
    """Deprecated please use the element variation-attribute-values (complexType.VariationAttribute.Values) see complexType.VariationAttribute."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.Attributes.Attribute.Values')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 647, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_Attribute_Values_httpwww_demandware_comxmlimpexcatalog2006_10_31value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 652, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_Variations_Attributes_Attribute_Values = complexType_Product_Variations_Attributes_Attribute_Values
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.Attributes.Attribute.Values', complexType_Product_Variations_Attributes_Attribute_Values)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.RefinementDefinitions with content type ELEMENT_ONLY
class complexType_RefinementDefinitions (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.RefinementDefinitions with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.RefinementDefinitions')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 668, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}refinement-definition uses Python identifier refinement_definition
    __refinement_definition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'refinement-definition'), 'refinement_definition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinitions_httpwww_demandware_comxmlimpexcatalog2006_10_31refinement_definition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 670, 12), )

    
    refinement_definition = property(__refinement_definition.value, __refinement_definition.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}blocked-refinement-definition uses Python identifier blocked_refinement_definition
    __blocked_refinement_definition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'blocked-refinement-definition'), 'blocked_refinement_definition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinitions_httpwww_demandware_comxmlimpexcatalog2006_10_31blocked_refinement_definition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 671, 12), )

    
    blocked_refinement_definition = property(__blocked_refinement_definition.value, __blocked_refinement_definition.set, None, None)

    _ElementMap.update({
        __refinement_definition.name() : __refinement_definition,
        __blocked_refinement_definition.name() : __blocked_refinement_definition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_RefinementDefinitions = complexType_RefinementDefinitions
Namespace.addCategoryObject('typeBinding', 'complexType.RefinementDefinitions', complexType_RefinementDefinitions)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.RefinementBuckets with content type ELEMENT_ONLY
class complexType_RefinementBuckets (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.RefinementBuckets with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.RefinementBuckets')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 698, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute-bucket uses Python identifier attribute_bucket
    __attribute_bucket = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute-bucket'), 'attribute_bucket', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementBuckets_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute_bucket', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 700, 12), )

    
    attribute_bucket = property(__attribute_bucket.value, __attribute_bucket.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}price-bucket uses Python identifier price_bucket
    __price_bucket = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'price-bucket'), 'price_bucket', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementBuckets_httpwww_demandware_comxmlimpexcatalog2006_10_31price_bucket', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 701, 12), )

    
    price_bucket = property(__price_bucket.value, __price_bucket.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}threshold-bucket uses Python identifier threshold_bucket
    __threshold_bucket = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'threshold-bucket'), 'threshold_bucket', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementBuckets_httpwww_demandware_comxmlimpexcatalog2006_10_31threshold_bucket', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 702, 12), )

    
    threshold_bucket = property(__threshold_bucket.value, __threshold_bucket.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}period-bucket uses Python identifier period_bucket
    __period_bucket = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'period-bucket'), 'period_bucket', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementBuckets_httpwww_demandware_comxmlimpexcatalog2006_10_31period_bucket', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 703, 12), )

    
    period_bucket = property(__period_bucket.value, __period_bucket.set, None, None)

    _ElementMap.update({
        __attribute_bucket.name() : __attribute_bucket,
        __price_bucket.name() : __price_bucket,
        __threshold_bucket.name() : __threshold_bucket,
        __period_bucket.name() : __period_bucket
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_RefinementBuckets = complexType_RefinementBuckets
Namespace.addCategoryObject('typeBinding', 'complexType.RefinementBuckets', complexType_RefinementBuckets)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeRefinementBucket with content type ELEMENT_ONLY
class complexType_AttributeRefinementBucket (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeRefinementBucket with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AttributeRefinementBucket')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 707, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 709, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value-list uses Python identifier value_list
    __value_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value-list'), 'value_list', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31value_list', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 710, 12), )

    
    value_list = property(__value_list.value, __value_list.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}presentation-id uses Python identifier presentation_id
    __presentation_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'presentation-id'), 'presentation_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31presentation_id', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 711, 12), )

    
    presentation_id = property(__presentation_id.value, __presentation_id.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 712, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeRefinementBucket_default', pyxb.binding.datatypes.boolean)
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 714, 8)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 714, 8)
    
    default = property(__default.value, __default.set, None, "If TRUE, the attribute marks a default bucket. It is used to represent the unbucketed values, if 'unbucketed-values-mode' is 'show-as-bucket'. Note: The element 'value-list' is not considered, since the unbucketed values are determined dynamically.\n                    If the default bucket is not defined within import file the system creates a new one with an empty 'display-name'.")

    _ElementMap.update({
        __display_name.name() : __display_name,
        __value_list.name() : __value_list,
        __presentation_id.name() : __presentation_id,
        __description.name() : __description
    })
    _AttributeMap.update({
        __default.name() : __default
    })
_module_typeBindings.complexType_AttributeRefinementBucket = complexType_AttributeRefinementBucket
Namespace.addCategoryObject('typeBinding', 'complexType.AttributeRefinementBucket', complexType_AttributeRefinementBucket)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ThresholdRefinementBucket with content type ELEMENT_ONLY
class complexType_ThresholdRefinementBucket (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ThresholdRefinementBucket with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ThresholdRefinementBucket')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 730, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ThresholdRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 732, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}threshold uses Python identifier threshold
    __threshold = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'threshold'), 'threshold', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ThresholdRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31threshold', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 733, 12), )

    
    threshold = property(__threshold.value, __threshold.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}presentation-id uses Python identifier presentation_id
    __presentation_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'presentation-id'), 'presentation_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ThresholdRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31presentation_id', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 734, 12), )

    
    presentation_id = property(__presentation_id.value, __presentation_id.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ThresholdRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 735, 12), )

    
    description = property(__description.value, __description.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __threshold.name() : __threshold,
        __presentation_id.name() : __presentation_id,
        __description.name() : __description
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_ThresholdRefinementBucket = complexType_ThresholdRefinementBucket
Namespace.addCategoryObject('typeBinding', 'complexType.ThresholdRefinementBucket', complexType_ThresholdRefinementBucket)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PeriodRefinementBucket with content type ELEMENT_ONLY
class complexType_PeriodRefinementBucket (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PeriodRefinementBucket with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PeriodRefinementBucket')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 739, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PeriodRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 741, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}duration-in-days uses Python identifier duration_in_days
    __duration_in_days = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'duration-in-days'), 'duration_in_days', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PeriodRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31duration_in_days', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 742, 12), )

    
    duration_in_days = property(__duration_in_days.value, __duration_in_days.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}presentation-id uses Python identifier presentation_id
    __presentation_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'presentation-id'), 'presentation_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PeriodRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31presentation_id', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 743, 12), )

    
    presentation_id = property(__presentation_id.value, __presentation_id.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PeriodRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 744, 12), )

    
    description = property(__description.value, __description.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __duration_in_days.name() : __duration_in_days,
        __presentation_id.name() : __presentation_id,
        __description.name() : __description
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_PeriodRefinementBucket = complexType_PeriodRefinementBucket
Namespace.addCategoryObject('typeBinding', 'complexType.PeriodRefinementBucket', complexType_PeriodRefinementBucket)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Options with content type ELEMENT_ONLY
class complexType_Product_Options (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Options with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Options')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 749, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}option uses Python identifier option
    __option = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'option'), 'option', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Options_httpwww_demandware_comxmlimpexcatalog2006_10_31option', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 751, 12), )

    
    option = property(__option.value, __option.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}shared-option uses Python identifier shared_option
    __shared_option = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'shared-option'), 'shared_option', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Options_httpwww_demandware_comxmlimpexcatalog2006_10_31shared_option', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 752, 12), )

    
    shared_option = property(__shared_option.value, __shared_option.set, None, None)

    _ElementMap.update({
        __option.name() : __option,
        __shared_option.name() : __shared_option
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_Options = complexType_Product_Options
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Options', complexType_Product_Options)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Values with content type ELEMENT_ONLY
class complexType_Option_Values (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Values with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Option.Values')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 761, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}option-value uses Python identifier option_value
    __option_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'option-value'), 'option_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Values_httpwww_demandware_comxmlimpexcatalog2006_10_31option_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 763, 12), )

    
    option_value = property(__option_value.value, __option_value.set, None, None)

    _ElementMap.update({
        __option_value.name() : __option_value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Option_Values = complexType_Option_Values
Namespace.addCategoryObject('typeBinding', 'complexType.Option.Values', complexType_Option_Values)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Value.Prices with content type ELEMENT_ONLY
class complexType_Option_Value_Prices (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Value.Prices with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Option.Value.Prices')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 778, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}option-value-price uses Python identifier option_value_price
    __option_value_price = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'option-value-price'), 'option_value_price', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_Prices_httpwww_demandware_comxmlimpexcatalog2006_10_31option_value_price', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 780, 12), )

    
    option_value_price = property(__option_value_price.value, __option_value_price.set, None, None)

    _ElementMap.update({
        __option_value_price.name() : __option_value_price
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Option_Value_Prices = complexType_Option_Value_Prices
Namespace.addCategoryObject('typeBinding', 'complexType.Option.Value.Prices', complexType_Option_Value_Prices)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ValueDefinitions with content type ELEMENT_ONLY
class complexType_ValueDefinitions (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ValueDefinitions with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ValueDefinitions')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 793, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value-definition uses Python identifier value_definition
    __value_definition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value-definition'), 'value_definition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ValueDefinitions_httpwww_demandware_comxmlimpexcatalog2006_10_31value_definition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 795, 12), )

    
    value_definition = property(__value_definition.value, __value_definition.set, None, None)

    
    # Attribute immutable uses Python identifier immutable
    __immutable = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'immutable'), 'immutable', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ValueDefinitions_immutable', pyxb.binding.datatypes.boolean, unicode_default='false')
    __immutable._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 797, 8)
    __immutable._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 797, 8)
    
    immutable = property(__immutable.value, __immutable.set, None, None)

    _ElementMap.update({
        __value_definition.name() : __value_definition
    })
    _AttributeMap.update({
        __immutable.name() : __immutable
    })
_module_typeBindings.complexType_ValueDefinitions = complexType_ValueDefinitions
Namespace.addCategoryObject('typeBinding', 'complexType.ValueDefinitions', complexType_ValueDefinitions)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ValueDefinition with content type ELEMENT_ONLY
class complexType_ValueDefinition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ValueDefinition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ValueDefinition')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 800, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display uses Python identifier display
    __display = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display'), 'display', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ValueDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31display', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 802, 12), )

    
    display = property(__display.value, __display.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ValueDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31value', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 803, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ValueDefinition_default', pyxb.binding.datatypes.boolean)
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 805, 8)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 805, 8)
    
    default = property(__default.value, __default.set, None, None)

    _ElementMap.update({
        __display.name() : __display,
        __value.name() : __value
    })
    _AttributeMap.update({
        __default.name() : __default
    })
_module_typeBindings.complexType_ValueDefinition = complexType_ValueDefinition
Namespace.addCategoryObject('typeBinding', 'complexType.ValueDefinition', complexType_ValueDefinition)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttributes with content type ELEMENT_ONLY
class sharedType_CustomAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 825, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attribute uses Python identifier custom_attribute
    __custom_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), 'custom_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_CustomAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 827, 12), )

    
    custom_attribute = property(__custom_attribute.value, __custom_attribute.set, None, None)

    _ElementMap.update({
        __custom_attribute.name() : __custom_attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.sharedType_CustomAttributes = sharedType_CustomAttributes
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttributes', sharedType_CustomAttributes)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificCustomAttributes with content type ELEMENT_ONLY
class sharedType_SiteSpecificCustomAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificCustomAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificCustomAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 839, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attribute uses Python identifier custom_attribute
    __custom_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), 'custom_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificCustomAttributes_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 841, 12), )

    
    custom_attribute = property(__custom_attribute.value, __custom_attribute.set, None, None)

    _ElementMap.update({
        __custom_attribute.name() : __custom_attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.sharedType_SiteSpecificCustomAttributes = sharedType_SiteSpecificCustomAttributes
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificCustomAttributes', sharedType_SiteSpecificCustomAttributes)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PageMetaTagRules with content type ELEMENT_ONLY
class complexType_PageMetaTagRules (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PageMetaTagRules with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PageMetaTagRules')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 893, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-meta-tag-rule uses Python identifier page_meta_tag_rule
    __page_meta_tag_rule = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-meta-tag-rule'), 'page_meta_tag_rule', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PageMetaTagRules_httpwww_demandware_comxmlimpexcatalog2006_10_31page_meta_tag_rule', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 895, 12), )

    
    page_meta_tag_rule = property(__page_meta_tag_rule.value, __page_meta_tag_rule.set, None, None)

    _ElementMap.update({
        __page_meta_tag_rule.name() : __page_meta_tag_rule
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_PageMetaTagRules = complexType_PageMetaTagRules
Namespace.addCategoryObject('typeBinding', 'complexType.PageMetaTagRules', complexType_PageMetaTagRules)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecificationRule with content type ELEMENT_ONLY
class complexType_ProductSpecificationRule (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecificationRule with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecificationRule')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 904, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecificationRule_httpwww_demandware_comxmlimpexcatalog2006_10_31name', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 906, 12), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecificationRule_httpwww_demandware_comxmlimpexcatalog2006_10_31description', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 907, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}enabled-flag uses Python identifier enabled_flag
    __enabled_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'enabled-flag'), 'enabled_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecificationRule_httpwww_demandware_comxmlimpexcatalog2006_10_31enabled_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 908, 12), )

    
    enabled_flag = property(__enabled_flag.value, __enabled_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}last-calculated uses Python identifier last_calculated
    __last_calculated = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'last-calculated'), 'last_calculated', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecificationRule_httpwww_demandware_comxmlimpexcatalog2006_10_31last_calculated', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 909, 12), )

    
    last_calculated = property(__last_calculated.value, __last_calculated.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-specification uses Python identifier product_specification
    __product_specification = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-specification'), 'product_specification', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecificationRule_httpwww_demandware_comxmlimpexcatalog2006_10_31product_specification', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 910, 12), )

    
    product_specification = property(__product_specification.value, __product_specification.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __description.name() : __description,
        __enabled_flag.name() : __enabled_flag,
        __last_calculated.name() : __last_calculated,
        __product_specification.name() : __product_specification
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_ProductSpecificationRule = complexType_ProductSpecificationRule
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecificationRule', complexType_ProductSpecificationRule)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification with content type ELEMENT_ONLY
class complexType_ProductSpecification (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 914, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}included-products uses Python identifier included_products
    __included_products = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'included-products'), 'included_products', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_httpwww_demandware_comxmlimpexcatalog2006_10_31included_products', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 916, 12), )

    
    included_products = property(__included_products.value, __included_products.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}excluded-products uses Python identifier excluded_products
    __excluded_products = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'excluded-products'), 'excluded_products', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_httpwww_demandware_comxmlimpexcatalog2006_10_31excluded_products', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 917, 12), )

    
    excluded_products = property(__excluded_products.value, __excluded_products.set, None, None)

    _ElementMap.update({
        __included_products.name() : __included_products,
        __excluded_products.name() : __excluded_products
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_ProductSpecification = complexType_ProductSpecification
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification', complexType_ProductSpecification)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ConditionGroups with content type ELEMENT_ONLY
class complexType_ProductSpecification_ConditionGroups (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ConditionGroups with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification.ConditionGroups')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 921, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}condition-group uses Python identifier condition_group
    __condition_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'condition-group'), 'condition_group', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ConditionGroups_httpwww_demandware_comxmlimpexcatalog2006_10_31condition_group', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 923, 12), )

    
    condition_group = property(__condition_group.value, __condition_group.set, None, None)

    _ElementMap.update({
        __condition_group.name() : __condition_group
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_ProductSpecification_ConditionGroups = complexType_ProductSpecification_ConditionGroups
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification.ConditionGroups', complexType_ProductSpecification_ConditionGroups)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ConditionGroup with content type ELEMENT_ONLY
class complexType_ProductSpecification_ConditionGroup (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ConditionGroup with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification.ConditionGroup')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 927, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}brand-condition uses Python identifier brand_condition
    __brand_condition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'brand-condition'), 'brand_condition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ConditionGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31brand_condition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 929, 12), )

    
    brand_condition = property(__brand_condition.value, __brand_condition.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-id-condition uses Python identifier product_id_condition
    __product_id_condition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-id-condition'), 'product_id_condition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ConditionGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31product_id_condition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 930, 12), )

    
    product_id_condition = property(__product_id_condition.value, __product_id_condition.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-condition uses Python identifier category_condition
    __category_condition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-condition'), 'category_condition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ConditionGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31category_condition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 931, 12), )

    
    category_condition = property(__category_condition.value, __category_condition.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute-condition uses Python identifier attribute_condition
    __attribute_condition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute-condition'), 'attribute_condition', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ConditionGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute_condition', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 932, 12), )

    
    attribute_condition = property(__attribute_condition.value, __attribute_condition.set, None, None)

    _ElementMap.update({
        __brand_condition.name() : __brand_condition,
        __product_id_condition.name() : __product_id_condition,
        __category_condition.name() : __category_condition,
        __attribute_condition.name() : __attribute_condition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_ProductSpecification_ConditionGroup = complexType_ProductSpecification_ConditionGroup
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification.ConditionGroup', complexType_ProductSpecification_ConditionGroup)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.VariationAttribute.Values with content type ELEMENT_ONLY
class complexType_VariationAttribute_Values (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.VariationAttribute.Values with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.VariationAttribute.Values')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 166, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-attribute-value uses Python identifier variation_attribute_value
    __variation_attribute_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-value'), 'variation_attribute_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_Values_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_attribute_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 168, 12), )

    
    variation_attribute_value = property(__variation_attribute_value.value, __variation_attribute_value.set, None, None)

    
    # Attribute merge-mode uses Python identifier merge_mode
    __merge_mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'merge-mode'), 'merge_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_Values_merge_mode', _module_typeBindings.simpleType_MergeMode, unicode_default='replace')
    __merge_mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 170, 8)
    __merge_mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 170, 8)
    
    merge_mode = property(__merge_mode.value, __merge_mode.set, None, '\n                    Used to control if specified variation values will be added to variation attribute, removed from variation attribute or replace\n                    existing variation attribute value list.\n                    Attribute should only be used in import MERGE and UPDATE modes. In import REPLACE mode, using the "merge-mode" attribute is not\n                    sensible, because existing variation attribute values will always be removed from the variation attribute before importing the\n                    variation attribute values specified in the import file.\n                ')

    _ElementMap.update({
        __variation_attribute_value.name() : __variation_attribute_value
    })
    _AttributeMap.update({
        __merge_mode.name() : __merge_mode
    })
_module_typeBindings.complexType_VariationAttribute_Values = complexType_VariationAttribute_Values
Namespace.addCategoryObject('typeBinding', 'complexType.VariationAttribute.Values', complexType_VariationAttribute_Values)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Images with content type ELEMENT_ONLY
class complexType_Product_Images (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Images with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Images')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 306, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image-group uses Python identifier image_group
    __image_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image-group'), 'image_group', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Images_httpwww_demandware_comxmlimpexcatalog2006_10_31image_group', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 308, 12), )

    
    image_group = property(__image_group.value, __image_group.set, None, None)

    
    # Attribute merge-mode uses Python identifier merge_mode
    __merge_mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'merge-mode'), 'merge_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Images_merge_mode', _module_typeBindings.simpleType_MergeMode, unicode_default='merge')
    __merge_mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 310, 8)
    __merge_mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 310, 8)
    
    merge_mode = property(__merge_mode.value, __merge_mode.set, None, '\n                    Used to control if specified image groups will be merged to or replace the existing image specification.\n                    The values "merge" and "replace" are the only ones supported for the "merge-mode" attribute.\n                    Attribute should only be used in import MERGE and UPDATE modes. In import REPLACE mode, using the "merge-mode" attribute is not\n                    sensible, because existing image groups will always be removed before importing the image groups\n                    specified in the import file.\n                ')

    _ElementMap.update({
        __image_group.name() : __image_group
    })
    _AttributeMap.update({
        __merge_mode.name() : __merge_mode
    })
_module_typeBindings.complexType_Product_Images = complexType_Product_Images
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Images', complexType_Product_Images)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Variants with content type ELEMENT_ONLY
class complexType_Product_Variations_Variants (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Variants with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.Variants')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 545, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variant uses Python identifier variant
    __variant = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variant'), 'variant', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Variants_httpwww_demandware_comxmlimpexcatalog2006_10_31variant', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 547, 12), )

    
    variant = property(__variant.value, __variant.set, None, None)

    
    # Attribute merge-mode uses Python identifier merge_mode
    __merge_mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'merge-mode'), 'merge_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Variants_merge_mode', _module_typeBindings.simpleType_MergeMode, unicode_default='replace')
    __merge_mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 549, 8)
    __merge_mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 549, 8)
    
    merge_mode = property(__merge_mode.value, __merge_mode.set, None, 'Used to control if specified variants will be added to master, removed from master or replace existing variant list.\n                    Attribute should only be used in import MERGE and UPDATE modes. In import REPLACE mode, using the "merge-mode" attribute is not sensible,\n                    because existing variants will always be removed from the master before importing the variants specified in the import file.\n                ')

    _ElementMap.update({
        __variant.name() : __variant
    })
    _AttributeMap.update({
        __merge_mode.name() : __merge_mode
    })
_module_typeBindings.complexType_Product_Variations_Variants = complexType_Product_Variations_Variants
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.Variants', complexType_Product_Variations_Variants)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.VariationGroups with content type ELEMENT_ONLY
class complexType_Product_Variations_VariationGroups (pyxb.binding.basis.complexTypeDefinition):
    """Reserved for Beta users. Variation groups will not be created and a warning will be logged if the Variation Groups (Beta) feature is disabled."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.VariationGroups')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 564, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-group uses Python identifier variation_group
    __variation_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-group'), 'variation_group', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_VariationGroups_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_group', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 569, 12), )

    
    variation_group = property(__variation_group.value, __variation_group.set, None, None)

    
    # Attribute merge-mode uses Python identifier merge_mode
    __merge_mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'merge-mode'), 'merge_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_VariationGroups_merge_mode', _module_typeBindings.simpleType_MergeMode, unicode_default='replace')
    __merge_mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 571, 8)
    __merge_mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 571, 8)
    
    merge_mode = property(__merge_mode.value, __merge_mode.set, None, 'Used to control if specified variation groups will be added to master, removed from master or replace existing variation groups list.\n                    Attribute should only be used in import MERGE and UPDATE modes. In import REPLACE mode, using the "merge-mode" attribute is not sensible,\n                    because existing variation groups will always be removed from the master before importing the variation groups specified in the import file.\n                ')

    _ElementMap.update({
        __variation_group.name() : __variation_group
    })
    _AttributeMap.update({
        __merge_mode.name() : __merge_mode
    })
_module_typeBindings.complexType_Product_Variations_VariationGroups = complexType_Product_Variations_VariationGroups
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.VariationGroups', complexType_Product_Variations_VariationGroups)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PriceRefinementBucket with content type ELEMENT_ONLY
class complexType_PriceRefinementBucket (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PriceRefinementBucket with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PriceRefinementBucket')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 722, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PriceRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 724, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}threshold uses Python identifier threshold
    __threshold = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'threshold'), 'threshold', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PriceRefinementBucket_httpwww_demandware_comxmlimpexcatalog2006_10_31threshold', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 725, 12), )

    
    threshold = property(__threshold.value, __threshold.set, None, None)

    
    # Attribute currency uses Python identifier currency
    __currency = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'currency'), 'currency', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PriceRefinementBucket_currency', _module_typeBindings.simpleType_Currency, required=True)
    __currency._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 727, 8)
    __currency._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 727, 8)
    
    currency = property(__currency.value, __currency.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __threshold.name() : __threshold
    })
    _AttributeMap.update({
        __currency.name() : __currency
    })
_module_typeBindings.complexType_PriceRefinementBucket = complexType_PriceRefinementBucket
Namespace.addCategoryObject('typeBinding', 'complexType.PriceRefinementBucket', complexType_PriceRefinementBucket)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Value.Price with content type SIMPLE
class complexType_Option_Value_Price (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Value.Price with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.decimal
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Option.Value.Price')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 784, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.decimal
    
    # Attribute currency uses Python identifier currency
    __currency = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'currency'), 'currency', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_Price_currency', _module_typeBindings.simpleType_Currency, required=True)
    __currency._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 787, 16)
    __currency._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 787, 16)
    
    currency = property(__currency.value, __currency.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __currency.name() : __currency
    })
_module_typeBindings.complexType_Option_Value_Price = complexType_Option_Value_Price
Namespace.addCategoryObject('typeBinding', 'complexType.Option.Value.Price', complexType_Option_Value_Price)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.LocalizedText with content type SIMPLE
class sharedType_LocalizedText (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.LocalizedText with content type SIMPLE"""
    _TypeDefinition = simpleType_Generic_String
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.LocalizedText')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 809, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_Generic_String
    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_LocalizedText_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 812, 16)
    
    lang = property(__lang.value, __lang.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __lang.name() : __lang
    })
_module_typeBindings.sharedType_LocalizedText = sharedType_LocalizedText
Namespace.addCategoryObject('typeBinding', 'sharedType.LocalizedText', sharedType_LocalizedText)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductBrandFilter with content type ELEMENT_ONLY
class complexType_ProductSpecification_ProductBrandFilter (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductBrandFilter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification.ProductBrandFilter')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 936, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}brand uses Python identifier brand
    __brand = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'brand'), 'brand', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductBrandFilter_httpwww_demandware_comxmlimpexcatalog2006_10_31brand', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 938, 12), )

    
    brand = property(__brand.value, __brand.set, None, None)

    
    # Attribute operator uses Python identifier operator
    __operator = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'operator'), 'operator', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductBrandFilter_operator', _module_typeBindings.simpleType_BrandComparator)
    __operator._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 940, 8)
    __operator._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 940, 8)
    
    operator = property(__operator.value, __operator.set, None, None)

    _ElementMap.update({
        __brand.name() : __brand
    })
    _AttributeMap.update({
        __operator.name() : __operator
    })
_module_typeBindings.complexType_ProductSpecification_ProductBrandFilter = complexType_ProductSpecification_ProductBrandFilter
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification.ProductBrandFilter', complexType_ProductSpecification_ProductBrandFilter)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductIDFilter with content type ELEMENT_ONLY
class complexType_ProductSpecification_ProductIDFilter (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductIDFilter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification.ProductIDFilter')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 943, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductIDFilter_httpwww_demandware_comxmlimpexcatalog2006_10_31product_id', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 945, 12), )

    
    product_id = property(__product_id.value, __product_id.set, None, None)

    
    # Attribute operator uses Python identifier operator
    __operator = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'operator'), 'operator', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductIDFilter_operator', _module_typeBindings.simpleType_ProductIDComparator)
    __operator._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 947, 8)
    __operator._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 947, 8)
    
    operator = property(__operator.value, __operator.set, None, None)

    _ElementMap.update({
        __product_id.name() : __product_id
    })
    _AttributeMap.update({
        __operator.name() : __operator
    })
_module_typeBindings.complexType_ProductSpecification_ProductIDFilter = complexType_ProductSpecification_ProductIDFilter
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification.ProductIDFilter', complexType_ProductSpecification_ProductIDFilter)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 14, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}header uses Python identifier header
    __header = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'header'), 'header', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31header', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 16, 16), )

    
    header = property(__header.value, __header.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-attribute-definitions uses Python identifier product_attribute_definitions
    __product_attribute_definitions = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-attribute-definitions'), 'product_attribute_definitions', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31product_attribute_definitions', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 17, 16), )

    
    product_attribute_definitions = property(__product_attribute_definitions.value, __product_attribute_definitions.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category uses Python identifier category
    __category = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category'), 'category', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31category', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 18, 16), )

    
    category = property(__category.value, __category.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product uses Python identifier product
    __product = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product'), 'product', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31product', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 19, 16), )

    
    product = property(__product.value, __product.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-option uses Python identifier product_option
    __product_option = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-option'), 'product_option', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31product_option', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 20, 16), )

    
    product_option = property(__product_option.value, __product_option.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-attribute uses Python identifier variation_attribute
    __variation_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute'), 'variation_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 21, 16), )

    
    variation_attribute = property(__variation_attribute.value, __variation_attribute.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-assignment uses Python identifier category_assignment
    __category_assignment = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-assignment'), 'category_assignment', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31category_assignment', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 22, 16), )

    
    category_assignment = property(__category_assignment.value, __category_assignment.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}recommendation uses Python identifier recommendation
    __recommendation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'recommendation'), 'recommendation', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexcatalog2006_10_31recommendation', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 23, 16), )

    
    recommendation = property(__recommendation.value, __recommendation.set, None, None)

    
    # Attribute catalog-id uses Python identifier catalog_id
    __catalog_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'catalog-id'), 'catalog_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_CTD_ANON_catalog_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __catalog_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 25, 12)
    __catalog_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 25, 12)
    
    catalog_id = property(__catalog_id.value, __catalog_id.set, None, None)

    _ElementMap.update({
        __header.name() : __header,
        __product_attribute_definitions.name() : __product_attribute_definitions,
        __category.name() : __category,
        __product.name() : __product,
        __product_option.name() : __product_option,
        __variation_attribute.name() : __variation_attribute,
        __category_assignment.name() : __category_assignment,
        __recommendation.name() : __recommendation
    })
    _AttributeMap.update({
        __catalog_id.name() : __catalog_id
    })
_module_typeBindings.CTD_ANON = CTD_ANON


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.InternalLocation with content type EMPTY
class complexType_Image_InternalLocation (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Image.InternalLocation with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Image.InternalLocation')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 66, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute base-path uses Python identifier base_path
    __base_path = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'base-path'), 'base_path', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Image_InternalLocation_base_path', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __base_path._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 67, 8)
    __base_path._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 67, 8)
    
    base_path = property(__base_path.value, __base_path.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __base_path.name() : __base_path
    })
_module_typeBindings.complexType_Image_InternalLocation = complexType_Image_InternalLocation
Namespace.addCategoryObject('typeBinding', 'complexType.Image.InternalLocation', complexType_Image_InternalLocation)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeDefinition with content type ELEMENT_ONLY
class complexType_AttributeDefinition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeDefinition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AttributeDefinition')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 83, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 85, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 86, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}type uses Python identifier type
    __type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'type'), 'type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31type', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 87, 12), )

    
    type = property(__type.value, __type.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}localizable-flag uses Python identifier localizable_flag
    __localizable_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'localizable-flag'), 'localizable_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31localizable_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 88, 12), )

    
    localizable_flag = property(__localizable_flag.value, __localizable_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}site-specific-flag uses Python identifier site_specific_flag
    __site_specific_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'site-specific-flag'), 'site_specific_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31site_specific_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 89, 12), )

    
    site_specific_flag = property(__site_specific_flag.value, __site_specific_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}mandatory-flag uses Python identifier mandatory_flag
    __mandatory_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mandatory-flag'), 'mandatory_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31mandatory_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 90, 12), )

    
    mandatory_flag = property(__mandatory_flag.value, __mandatory_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}visible-flag uses Python identifier visible_flag
    __visible_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'visible-flag'), 'visible_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31visible_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 91, 12), )

    
    visible_flag = property(__visible_flag.value, __visible_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}externally-managed-flag uses Python identifier externally_managed_flag
    __externally_managed_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'externally-managed-flag'), 'externally_managed_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31externally_managed_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 92, 12), )

    
    externally_managed_flag = property(__externally_managed_flag.value, __externally_managed_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}min-length uses Python identifier min_length
    __min_length = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'min-length'), 'min_length', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31min_length', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 93, 12), )

    
    min_length = property(__min_length.value, __min_length.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}field-length uses Python identifier field_length
    __field_length = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'field-length'), 'field_length', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31field_length', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 94, 12), )

    
    field_length = property(__field_length.value, __field_length.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}field-height uses Python identifier field_height
    __field_height = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'field-height'), 'field_height', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31field_height', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 95, 12), )

    
    field_height = property(__field_height.value, __field_height.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}unit uses Python identifier unit
    __unit = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'unit'), 'unit', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31unit', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 96, 12), )

    
    unit = property(__unit.value, __unit.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}scale uses Python identifier scale
    __scale = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'scale'), 'scale', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31scale', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 97, 12), )

    
    scale = property(__scale.value, __scale.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}min-value uses Python identifier min_value
    __min_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'min-value'), 'min_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31min_value', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 98, 12), )

    
    min_value = property(__min_value.value, __min_value.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}max-value uses Python identifier max_value
    __max_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'max-value'), 'max_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31max_value', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 99, 12), )

    
    max_value = property(__max_value.value, __max_value.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}regex uses Python identifier regex
    __regex = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'regex'), 'regex', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31regex', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 100, 12), )

    
    regex = property(__regex.value, __regex.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}select-multiple-flag uses Python identifier select_multiple_flag
    __select_multiple_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'select-multiple-flag'), 'select_multiple_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31select_multiple_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 101, 12), )

    
    select_multiple_flag = property(__select_multiple_flag.value, __select_multiple_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value-definitions uses Python identifier value_definitions
    __value_definitions = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value-definitions'), 'value_definitions', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31value_definitions', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 102, 12), )

    
    value_definitions = property(__value_definitions.value, __value_definitions.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeDefinition_attribute_id', _module_typeBindings.simpleType_AttributeDefinition_ID, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 104, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 104, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __description.name() : __description,
        __type.name() : __type,
        __localizable_flag.name() : __localizable_flag,
        __site_specific_flag.name() : __site_specific_flag,
        __mandatory_flag.name() : __mandatory_flag,
        __visible_flag.name() : __visible_flag,
        __externally_managed_flag.name() : __externally_managed_flag,
        __min_length.name() : __min_length,
        __field_length.name() : __field_length,
        __field_height.name() : __field_height,
        __unit.name() : __unit,
        __scale.name() : __scale,
        __min_value.name() : __min_value,
        __max_value.name() : __max_value,
        __regex.name() : __regex,
        __select_multiple_flag.name() : __select_multiple_flag,
        __value_definitions.name() : __value_definitions
    })
    _AttributeMap.update({
        __attribute_id.name() : __attribute_id
    })
_module_typeBindings.complexType_AttributeDefinition = complexType_AttributeDefinition
Namespace.addCategoryObject('typeBinding', 'complexType.AttributeDefinition', complexType_AttributeDefinition)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Category with content type ELEMENT_ONLY
class complexType_Category (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Category with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Category')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 108, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 110, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 111, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}online-flag uses Python identifier online_flag
    __online_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-flag'), 'online_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31online_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 112, 12), )

    
    online_flag = property(__online_flag.value, __online_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}online-from uses Python identifier online_from
    __online_from = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-from'), 'online_from', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31online_from', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 113, 12), )

    
    online_from = property(__online_from.value, __online_from.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}online-to uses Python identifier online_to
    __online_to = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-to'), 'online_to', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31online_to', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 114, 12), )

    
    online_to = property(__online_to.value, __online_to.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-groups-display-mode uses Python identifier variation_groups_display_mode
    __variation_groups_display_mode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-groups-display-mode'), 'variation_groups_display_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_groups_display_mode', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 115, 12), )

    
    variation_groups_display_mode = property(__variation_groups_display_mode.value, __variation_groups_display_mode.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}parent uses Python identifier parent
    __parent = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'parent'), 'parent', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31parent', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 116, 12), )

    
    parent = property(__parent.value, __parent.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}position uses Python identifier position
    __position = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'position'), 'position', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31position', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 117, 12), )

    
    position = property(__position.value, __position.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}thumbnail uses Python identifier thumbnail
    __thumbnail = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'thumbnail'), 'thumbnail', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31thumbnail', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 118, 12), )

    
    thumbnail = property(__thumbnail.value, __thumbnail.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31image', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 119, 12), )

    
    image = property(__image.value, __image.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}template uses Python identifier template
    __template = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'template'), 'template', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31template', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 120, 12), )

    
    template = property(__template.value, __template.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}search-placement uses Python identifier search_placement
    __search_placement = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'search-placement'), 'search_placement', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31search_placement', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 121, 12), )

    
    search_placement = property(__search_placement.value, __search_placement.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}search-rank uses Python identifier search_rank
    __search_rank = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'search-rank'), 'search_rank', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31search_rank', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 122, 12), )

    
    search_rank = property(__search_rank.value, __search_rank.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sitemap-included-flag uses Python identifier sitemap_included_flag
    __sitemap_included_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sitemap-included-flag'), 'sitemap_included_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31sitemap_included_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 123, 12), )

    
    sitemap_included_flag = property(__sitemap_included_flag.value, __sitemap_included_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sitemap-changefrequency uses Python identifier sitemap_changefrequency
    __sitemap_changefrequency = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sitemap-changefrequency'), 'sitemap_changefrequency', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31sitemap_changefrequency', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 124, 12), )

    
    sitemap_changefrequency = property(__sitemap_changefrequency.value, __sitemap_changefrequency.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sitemap-priority uses Python identifier sitemap_priority
    __sitemap_priority = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sitemap-priority'), 'sitemap_priority', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31sitemap_priority', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 125, 12), )

    
    sitemap_priority = property(__sitemap_priority.value, __sitemap_priority.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-attributes uses Python identifier page_attributes
    __page_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-attributes'), 'page_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31page_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 126, 12), )

    
    page_attributes = property(__page_attributes.value, __page_attributes.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 127, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-links uses Python identifier category_links
    __category_links = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-links'), 'category_links', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31category_links', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 128, 12), )

    
    category_links = property(__category_links.value, __category_links.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute-groups uses Python identifier attribute_groups
    __attribute_groups = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute-groups'), 'attribute_groups', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute_groups', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 129, 12), )

    
    attribute_groups = property(__attribute_groups.value, __attribute_groups.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}refinement-definitions uses Python identifier refinement_definitions
    __refinement_definitions = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'refinement-definitions'), 'refinement_definitions', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31refinement_definitions', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 130, 12), )

    
    refinement_definitions = property(__refinement_definitions.value, __refinement_definitions.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-detail-page-meta-tag-rules uses Python identifier product_detail_page_meta_tag_rules
    __product_detail_page_meta_tag_rules = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-detail-page-meta-tag-rules'), 'product_detail_page_meta_tag_rules', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31product_detail_page_meta_tag_rules', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 131, 12), )

    
    product_detail_page_meta_tag_rules = property(__product_detail_page_meta_tag_rules.value, __product_detail_page_meta_tag_rules.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-listing-page-meta-tag-rules uses Python identifier product_listing_page_meta_tag_rules
    __product_listing_page_meta_tag_rules = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-listing-page-meta-tag-rules'), 'product_listing_page_meta_tag_rules', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31product_listing_page_meta_tag_rules', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 132, 12), )

    
    product_listing_page_meta_tag_rules = property(__product_listing_page_meta_tag_rules.value, __product_listing_page_meta_tag_rules.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-specification-rule uses Python identifier product_specification_rule
    __product_specification_rule = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-specification-rule'), 'product_specification_rule', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_httpwww_demandware_comxmlimpexcatalog2006_10_31product_specification_rule', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 133, 12), )

    
    product_specification_rule = property(__product_specification_rule.value, __product_specification_rule.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 135, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 135, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    
    # Attribute category-id uses Python identifier category_id
    __category_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'category-id'), 'category_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Category_category_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __category_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 136, 8)
    __category_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 136, 8)
    
    category_id = property(__category_id.value, __category_id.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __description.name() : __description,
        __online_flag.name() : __online_flag,
        __online_from.name() : __online_from,
        __online_to.name() : __online_to,
        __variation_groups_display_mode.name() : __variation_groups_display_mode,
        __parent.name() : __parent,
        __position.name() : __position,
        __thumbnail.name() : __thumbnail,
        __image.name() : __image,
        __template.name() : __template,
        __search_placement.name() : __search_placement,
        __search_rank.name() : __search_rank,
        __sitemap_included_flag.name() : __sitemap_included_flag,
        __sitemap_changefrequency.name() : __sitemap_changefrequency,
        __sitemap_priority.name() : __sitemap_priority,
        __page_attributes.name() : __page_attributes,
        __custom_attributes.name() : __custom_attributes,
        __category_links.name() : __category_links,
        __attribute_groups.name() : __attribute_groups,
        __refinement_definitions.name() : __refinement_definitions,
        __product_detail_page_meta_tag_rules.name() : __product_detail_page_meta_tag_rules,
        __product_listing_page_meta_tag_rules.name() : __product_listing_page_meta_tag_rules,
        __product_specification_rule.name() : __product_specification_rule
    })
    _AttributeMap.update({
        __mode.name() : __mode,
        __category_id.name() : __category_id
    })
_module_typeBindings.complexType_Category = complexType_Category
Namespace.addCategoryObject('typeBinding', 'complexType.Category', complexType_Category)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option with content type ELEMENT_ONLY
class complexType_Option (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Option')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 141, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 143, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 144, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_httpwww_demandware_comxmlimpexcatalog2006_10_31image', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 145, 12), )

    
    image = property(__image.value, __image.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sort-mode uses Python identifier sort_mode
    __sort_mode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sort-mode'), 'sort_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_httpwww_demandware_comxmlimpexcatalog2006_10_31sort_mode', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 146, 12), )

    
    sort_mode = property(__sort_mode.value, __sort_mode.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}option-values uses Python identifier option_values
    __option_values = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'option-values'), 'option_values', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_httpwww_demandware_comxmlimpexcatalog2006_10_31option_values', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 147, 12), )

    
    option_values = property(__option_values.value, __option_values.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 149, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 149, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    
    # Attribute option-id uses Python identifier option_id
    __option_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'option-id'), 'option_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_option_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __option_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 150, 8)
    __option_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 150, 8)
    
    option_id = property(__option_id.value, __option_id.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __description.name() : __description,
        __image.name() : __image,
        __sort_mode.name() : __sort_mode,
        __option_values.name() : __option_values
    })
    _AttributeMap.update({
        __mode.name() : __mode,
        __option_id.name() : __option_id
    })
_module_typeBindings.complexType_Option = complexType_Option
Namespace.addCategoryObject('typeBinding', 'complexType.Option', complexType_Option)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.VariationAttribute with content type ELEMENT_ONLY
class complexType_VariationAttribute (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.VariationAttribute with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.VariationAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 154, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 156, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-attribute-values uses Python identifier variation_attribute_values
    __variation_attribute_values = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-values'), 'variation_attribute_values', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_attribute_values', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 157, 12), )

    
    variation_attribute_values = property(__variation_attribute_values.value, __variation_attribute_values.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 159, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 159, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 160, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 160, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    
    # Attribute variation-attribute-id uses Python identifier variation_attribute_id
    __variation_attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'variation-attribute-id'), 'variation_attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_variation_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __variation_attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 161, 8)
    __variation_attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 161, 8)
    
    variation_attribute_id = property(__variation_attribute_id.value, __variation_attribute_id.set, None, None)

    
    # Attribute slicing-attribute uses Python identifier slicing_attribute
    __slicing_attribute = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'slicing-attribute'), 'slicing_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_slicing_attribute', pyxb.binding.datatypes.boolean)
    __slicing_attribute._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 162, 8)
    __slicing_attribute._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 162, 8)
    
    slicing_attribute = property(__slicing_attribute.value, __slicing_attribute.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __variation_attribute_values.name() : __variation_attribute_values
    })
    _AttributeMap.update({
        __mode.name() : __mode,
        __attribute_id.name() : __attribute_id,
        __variation_attribute_id.name() : __variation_attribute_id,
        __slicing_attribute.name() : __slicing_attribute
    })
_module_typeBindings.complexType_VariationAttribute = complexType_VariationAttribute
Namespace.addCategoryObject('typeBinding', 'complexType.VariationAttribute', complexType_VariationAttribute)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.VariationAttribute.Value with content type ELEMENT_ONLY
class complexType_VariationAttribute_Value (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.VariationAttribute.Value with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.VariationAttribute.Value')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 184, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-value uses Python identifier display_value
    __display_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-value'), 'display_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_Value_httpwww_demandware_comxmlimpexcatalog2006_10_31display_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 186, 12), )

    
    display_value = property(__display_value.value, __display_value.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_Value_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 187, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Attribute value uses Python identifier value_
    __value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'value'), 'value_', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_Value_value', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __value._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 189, 8)
    __value._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 189, 8)
    
    value_ = property(__value.value, __value.set, None, None)

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_VariationAttribute_Value_default', pyxb.binding.datatypes.boolean)
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 190, 8)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 190, 8)
    
    default = property(__default.value, __default.set, None, None)

    _ElementMap.update({
        __display_value.name() : __display_value,
        __description.name() : __description
    })
    _AttributeMap.update({
        __value.name() : __value,
        __default.name() : __default
    })
_module_typeBindings.complexType_VariationAttribute_Value = complexType_VariationAttribute_Value
Namespace.addCategoryObject('typeBinding', 'complexType.VariationAttribute.Value', complexType_VariationAttribute_Value)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product with content type ELEMENT_ONLY
class complexType_Product (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 195, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}ean uses Python identifier ean
    __ean = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ean'), 'ean', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31ean', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 197, 12), )

    
    ean = property(__ean.value, __ean.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}upc uses Python identifier upc
    __upc = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'upc'), 'upc', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31upc', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 198, 12), )

    
    upc = property(__upc.value, __upc.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}unit uses Python identifier unit
    __unit = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'unit'), 'unit', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31unit', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 199, 12), )

    
    unit = property(__unit.value, __unit.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}min-order-quantity uses Python identifier min_order_quantity
    __min_order_quantity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'min-order-quantity'), 'min_order_quantity', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31min_order_quantity', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 200, 12), )

    
    min_order_quantity = property(__min_order_quantity.value, __min_order_quantity.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}step-quantity uses Python identifier step_quantity
    __step_quantity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'step-quantity'), 'step_quantity', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31step_quantity', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 201, 12), )

    
    step_quantity = property(__step_quantity.value, __step_quantity.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 202, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}short-description uses Python identifier short_description
    __short_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'short-description'), 'short_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31short_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 203, 12), )

    
    short_description = property(__short_description.value, __short_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}long-description uses Python identifier long_description
    __long_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'long-description'), 'long_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31long_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 204, 12), )

    
    long_description = property(__long_description.value, __long_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-receipt-name uses Python identifier store_receipt_name
    __store_receipt_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-receipt-name'), 'store_receipt_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_receipt_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 205, 12), )

    
    store_receipt_name = property(__store_receipt_name.value, __store_receipt_name.set, None, 'Deprecated. Please use element receipt-name which held by element store-attributes (complexType.Product.StoreAttributes).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-tax-class uses Python identifier store_tax_class
    __store_tax_class = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-tax-class'), 'store_tax_class', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_tax_class', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 210, 12), )

    
    store_tax_class = property(__store_tax_class.value, __store_tax_class.set, None, 'Deprecated. Please use element tax-class which held by element store-attributes (complexType.Product.StoreAttributes).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-force-price-flag uses Python identifier store_force_price_flag
    __store_force_price_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-force-price-flag'), 'store_force_price_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_force_price_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 215, 12), )

    
    store_force_price_flag = property(__store_force_price_flag.value, __store_force_price_flag.set, None, 'Deprecated. Please use element force-price-flag which held by element store-attributes (complexType.Product.StoreAttributes).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-non-inventory-flag uses Python identifier store_non_inventory_flag
    __store_non_inventory_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-non-inventory-flag'), 'store_non_inventory_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_non_inventory_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 220, 12), )

    
    store_non_inventory_flag = property(__store_non_inventory_flag.value, __store_non_inventory_flag.set, None, 'Deprecated. Please use element non-inventory-flag which held by element store-attributes (complexType.Product.StoreAttributes).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-non-revenue-flag uses Python identifier store_non_revenue_flag
    __store_non_revenue_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-non-revenue-flag'), 'store_non_revenue_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_non_revenue_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 225, 12), )

    
    store_non_revenue_flag = property(__store_non_revenue_flag.value, __store_non_revenue_flag.set, None, 'Deprecated. Please use element non-revenue-flag which held by element store-attributes (complexType.Product.StoreAttributes).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-non-discountable-flag uses Python identifier store_non_discountable_flag
    __store_non_discountable_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-non-discountable-flag'), 'store_non_discountable_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_non_discountable_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 230, 12), )

    
    store_non_discountable_flag = property(__store_non_discountable_flag.value, __store_non_discountable_flag.set, None, 'Deprecated. Please use element non-discountable-flag which held by element store-attributes (complexType.Product.StoreAttributes).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}online-flag uses Python identifier online_flag
    __online_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-flag'), 'online_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31online_flag', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 235, 12), )

    
    online_flag = property(__online_flag.value, __online_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}online-from uses Python identifier online_from
    __online_from = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-from'), 'online_from', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31online_from', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 236, 12), )

    
    online_from = property(__online_from.value, __online_from.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}online-to uses Python identifier online_to
    __online_to = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-to'), 'online_to', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31online_to', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 237, 12), )

    
    online_to = property(__online_to.value, __online_to.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}available-flag uses Python identifier available_flag
    __available_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'available-flag'), 'available_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31available_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 238, 12), )

    
    available_flag = property(__available_flag.value, __available_flag.set, None, 'Deprecated. Use inventory feature to maintain product availability.')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}searchable-flag uses Python identifier searchable_flag
    __searchable_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'searchable-flag'), 'searchable_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31searchable_flag', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 243, 12), )

    
    searchable_flag = property(__searchable_flag.value, __searchable_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}searchable-if-unavailable-flag uses Python identifier searchable_if_unavailable_flag
    __searchable_if_unavailable_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'searchable-if-unavailable-flag'), 'searchable_if_unavailable_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31searchable_if_unavailable_flag', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 244, 12), )

    
    searchable_if_unavailable_flag = property(__searchable_if_unavailable_flag.value, __searchable_if_unavailable_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}images uses Python identifier images
    __images = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'images'), 'images', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31images', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 245, 12), )

    
    images = property(__images.value, __images.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31image', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 246, 12), )

    
    image = property(__image.value, __image.set, None, 'Deprecated. Please use the element images (complexType.Product.Images).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}thumbnail uses Python identifier thumbnail
    __thumbnail = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'thumbnail'), 'thumbnail', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31thumbnail', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 251, 12), )

    
    thumbnail = property(__thumbnail.value, __thumbnail.set, None, 'Deprecated. Please use the element images (complexType.Product.Images).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}template uses Python identifier template
    __template = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'template'), 'template', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31template', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 256, 12), )

    
    template = property(__template.value, __template.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}tax-class-id uses Python identifier tax_class_id
    __tax_class_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'tax-class-id'), 'tax_class_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31tax_class_id', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 257, 12), )

    
    tax_class_id = property(__tax_class_id.value, __tax_class_id.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}brand uses Python identifier brand
    __brand = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'brand'), 'brand', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31brand', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 258, 12), )

    
    brand = property(__brand.value, __brand.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}manufacturer-name uses Python identifier manufacturer_name
    __manufacturer_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'manufacturer-name'), 'manufacturer_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31manufacturer_name', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 259, 12), )

    
    manufacturer_name = property(__manufacturer_name.value, __manufacturer_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}manufacturer-sku uses Python identifier manufacturer_sku
    __manufacturer_sku = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'manufacturer-sku'), 'manufacturer_sku', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31manufacturer_sku', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 260, 12), )

    
    manufacturer_sku = property(__manufacturer_sku.value, __manufacturer_sku.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}search-placement uses Python identifier search_placement
    __search_placement = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'search-placement'), 'search_placement', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31search_placement', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 261, 12), )

    
    search_placement = property(__search_placement.value, __search_placement.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}search-rank uses Python identifier search_rank
    __search_rank = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'search-rank'), 'search_rank', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31search_rank', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 262, 12), )

    
    search_rank = property(__search_rank.value, __search_rank.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sitemap-included-flag uses Python identifier sitemap_included_flag
    __sitemap_included_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sitemap-included-flag'), 'sitemap_included_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31sitemap_included_flag', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 263, 12), )

    
    sitemap_included_flag = property(__sitemap_included_flag.value, __sitemap_included_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sitemap-changefrequency uses Python identifier sitemap_changefrequency
    __sitemap_changefrequency = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sitemap-changefrequency'), 'sitemap_changefrequency', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31sitemap_changefrequency', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 264, 12), )

    
    sitemap_changefrequency = property(__sitemap_changefrequency.value, __sitemap_changefrequency.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sitemap-priority uses Python identifier sitemap_priority
    __sitemap_priority = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sitemap-priority'), 'sitemap_priority', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31sitemap_priority', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 265, 12), )

    
    sitemap_priority = property(__sitemap_priority.value, __sitemap_priority.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}page-attributes uses Python identifier page_attributes
    __page_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'page-attributes'), 'page_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31page_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 266, 12), )

    
    page_attributes = property(__page_attributes.value, __page_attributes.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 267, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}bundled-products uses Python identifier bundled_products
    __bundled_products = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'bundled-products'), 'bundled_products', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31bundled_products', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 268, 12), )

    
    bundled_products = property(__bundled_products.value, __bundled_products.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}retail-set-products uses Python identifier retail_set_products
    __retail_set_products = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'retail-set-products'), 'retail_set_products', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31retail_set_products', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 269, 12), )

    
    retail_set_products = property(__retail_set_products.value, __retail_set_products.set, None, 'Deprecated please use the element product-set-products (complexType.Product.ProductSetProducts).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-set-products uses Python identifier product_set_products
    __product_set_products = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-set-products'), 'product_set_products', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31product_set_products', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 274, 12), )

    
    product_set_products = property(__product_set_products.value, __product_set_products.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}options uses Python identifier options
    __options = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'options'), 'options', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31options', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 275, 12), )

    
    options = property(__options.value, __options.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variations uses Python identifier variations
    __variations = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variations'), 'variations', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31variations', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 276, 12), )

    
    variations = property(__variations.value, __variations.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-links uses Python identifier product_links
    __product_links = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-links'), 'product_links', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31product_links', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 277, 12), )

    
    product_links = property(__product_links.value, __product_links.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-links uses Python identifier category_links
    __category_links = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-links'), 'category_links', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31category_links', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 278, 12), )

    
    category_links = property(__category_links.value, __category_links.set, None, 'Deprecated please use the elements category-assignment (complexType.CategoryAssignment) and classification-category (complexType.Product.ClassificationCategory).')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}classification-category uses Python identifier classification_category
    __classification_category = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'classification-category'), 'classification_category', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31classification_category', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 283, 12), )

    
    classification_category = property(__classification_category.value, __classification_category.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}pinterest-enabled-flag uses Python identifier pinterest_enabled_flag
    __pinterest_enabled_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'pinterest-enabled-flag'), 'pinterest_enabled_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31pinterest_enabled_flag', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 284, 12), )

    
    pinterest_enabled_flag = property(__pinterest_enabled_flag.value, __pinterest_enabled_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}facebook-enabled-flag uses Python identifier facebook_enabled_flag
    __facebook_enabled_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'facebook-enabled-flag'), 'facebook_enabled_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31facebook_enabled_flag', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 285, 12), )

    
    facebook_enabled_flag = property(__facebook_enabled_flag.value, __facebook_enabled_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-detail-page-meta-tag-rules uses Python identifier product_detail_page_meta_tag_rules
    __product_detail_page_meta_tag_rules = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-detail-page-meta-tag-rules'), 'product_detail_page_meta_tag_rules', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31product_detail_page_meta_tag_rules', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 286, 12), )

    
    product_detail_page_meta_tag_rules = property(__product_detail_page_meta_tag_rules.value, __product_detail_page_meta_tag_rules.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}store-attributes uses Python identifier store_attributes
    __store_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-attributes'), 'store_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_httpwww_demandware_comxmlimpexcatalog2006_10_31store_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 287, 12), )

    
    store_attributes = property(__store_attributes.value, __store_attributes.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 289, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 289, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 290, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 290, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    _ElementMap.update({
        __ean.name() : __ean,
        __upc.name() : __upc,
        __unit.name() : __unit,
        __min_order_quantity.name() : __min_order_quantity,
        __step_quantity.name() : __step_quantity,
        __display_name.name() : __display_name,
        __short_description.name() : __short_description,
        __long_description.name() : __long_description,
        __store_receipt_name.name() : __store_receipt_name,
        __store_tax_class.name() : __store_tax_class,
        __store_force_price_flag.name() : __store_force_price_flag,
        __store_non_inventory_flag.name() : __store_non_inventory_flag,
        __store_non_revenue_flag.name() : __store_non_revenue_flag,
        __store_non_discountable_flag.name() : __store_non_discountable_flag,
        __online_flag.name() : __online_flag,
        __online_from.name() : __online_from,
        __online_to.name() : __online_to,
        __available_flag.name() : __available_flag,
        __searchable_flag.name() : __searchable_flag,
        __searchable_if_unavailable_flag.name() : __searchable_if_unavailable_flag,
        __images.name() : __images,
        __image.name() : __image,
        __thumbnail.name() : __thumbnail,
        __template.name() : __template,
        __tax_class_id.name() : __tax_class_id,
        __brand.name() : __brand,
        __manufacturer_name.name() : __manufacturer_name,
        __manufacturer_sku.name() : __manufacturer_sku,
        __search_placement.name() : __search_placement,
        __search_rank.name() : __search_rank,
        __sitemap_included_flag.name() : __sitemap_included_flag,
        __sitemap_changefrequency.name() : __sitemap_changefrequency,
        __sitemap_priority.name() : __sitemap_priority,
        __page_attributes.name() : __page_attributes,
        __custom_attributes.name() : __custom_attributes,
        __bundled_products.name() : __bundled_products,
        __retail_set_products.name() : __retail_set_products,
        __product_set_products.name() : __product_set_products,
        __options.name() : __options,
        __variations.name() : __variations,
        __product_links.name() : __product_links,
        __category_links.name() : __category_links,
        __classification_category.name() : __classification_category,
        __pinterest_enabled_flag.name() : __pinterest_enabled_flag,
        __facebook_enabled_flag.name() : __facebook_enabled_flag,
        __product_detail_page_meta_tag_rules.name() : __product_detail_page_meta_tag_rules,
        __store_attributes.name() : __store_attributes
    })
    _AttributeMap.update({
        __mode.name() : __mode,
        __product_id.name() : __product_id
    })
_module_typeBindings.complexType_Product = complexType_Product
Namespace.addCategoryObject('typeBinding', 'complexType.Product', complexType_Product)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ImageGroup with content type ELEMENT_ONLY
class complexType_Product_ImageGroup (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ImageGroup with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ImageGroup')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 323, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation uses Python identifier variation
    __variation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation'), 'variation', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ImageGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31variation', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 325, 12), )

    
    variation = property(__variation.value, __variation.set, None, '\n                        This element, only relevant for variation masters, represents an ordered list of variation\n                        attribute values.  It is used to identify a set of variants to which the images in this group apply.\n\n                        - The image groups related to the product master will have zero of these elements.  These\n                          represent the "fallback" images.\n                        - If an image group applies for all the red variants, for example, it will have one of these\n                          elements, namely "color=red".\n                        - If an image group applies for all the red, leather variants, for example, it will have two\n                          of these elements, namely "color=red, fabric=leather".\n\n                        The list is ordered so that, within a master product, the same variation attribute must always\n                        appear at the same index of the list.  In our example, "color" would always be specified first\n                        for each image group, and then "fabric" would always appear second.  This ensures there are\n                        clear fallback rules to identify which images relate to each variant. When retrieving the images\n                        for a variant at runtime, the system chooses the most specific matching image groups possible.\n                    ')

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ImageGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31image', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 346, 12), )

    
    image = property(__image.value, __image.set, None, None)

    
    # Attribute view-type uses Python identifier view_type
    __view_type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'view-type'), 'view_type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ImageGroup_view_type', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __view_type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 348, 8)
    __view_type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 348, 8)
    
    view_type = property(__view_type.value, __view_type.set, None, None)

    
    # Attribute variation-value uses Python identifier variation_value
    __variation_value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'variation-value'), 'variation_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ImageGroup_variation_value', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __variation_value._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 349, 8)
    __variation_value._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 349, 8)
    
    variation_value = property(__variation_value.value, __variation_value.set, None, 'Deprecated. Please use the variation elements (complexType.Product.ImageGroup.VariationAttributeValue).')

    _ElementMap.update({
        __variation.name() : __variation,
        __image.name() : __image
    })
    _AttributeMap.update({
        __view_type.name() : __view_type,
        __variation_value.name() : __variation_value
    })
_module_typeBindings.complexType_Product_ImageGroup = complexType_Product_ImageGroup
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ImageGroup', complexType_Product_ImageGroup)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ImageGroup.VariationAttributeValue with content type EMPTY
class complexType_Product_ImageGroup_VariationAttributeValue (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ImageGroup.VariationAttributeValue with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ImageGroup.VariationAttributeValue')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 356, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ImageGroup_VariationAttributeValue_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 357, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 357, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    
    # Attribute value uses Python identifier value_
    __value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'value'), 'value_', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ImageGroup_VariationAttributeValue_value', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __value._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 358, 8)
    __value._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 358, 8)
    
    value_ = property(__value.value, __value.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __attribute_id.name() : __attribute_id,
        __value.name() : __value
    })
_module_typeBindings.complexType_Product_ImageGroup_VariationAttributeValue = complexType_Product_ImageGroup_VariationAttributeValue
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ImageGroup.VariationAttributeValue', complexType_Product_ImageGroup_VariationAttributeValue)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Image with content type ELEMENT_ONLY
class complexType_Product_Image (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Image with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Image')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 361, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}alt uses Python identifier alt
    __alt = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'alt'), 'alt', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Image_httpwww_demandware_comxmlimpexcatalog2006_10_31alt', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 363, 12), )

    
    alt = property(__alt.value, __alt.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}title uses Python identifier title
    __title = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'title'), 'title', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Image_httpwww_demandware_comxmlimpexcatalog2006_10_31title', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 364, 12), )

    
    title = property(__title.value, __title.set, None, None)

    
    # Attribute path uses Python identifier path
    __path = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'path'), 'path', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Image_path', _module_typeBindings.simpleType_Generic_NonEmptyString_1800, required=True)
    __path._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 366, 8)
    __path._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 366, 8)
    
    path = property(__path.value, __path.set, None, None)

    _ElementMap.update({
        __alt.name() : __alt,
        __title.name() : __title
    })
    _AttributeMap.update({
        __path.name() : __path
    })
_module_typeBindings.complexType_Product_Image = complexType_Product_Image
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Image', complexType_Product_Image)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment with content type ELEMENT_ONLY
class complexType_CategoryAssignment (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.CategoryAssignment')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 370, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 372, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}short-description uses Python identifier short_description
    __short_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'short-description'), 'short_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31short_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 373, 12), )

    
    short_description = property(__short_description.value, __short_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}long-description uses Python identifier long_description
    __long_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'long-description'), 'long_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31long_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 374, 12), )

    
    long_description = property(__long_description.value, __long_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}callout-message uses Python identifier callout_message
    __callout_message = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'callout-message'), 'callout_message', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31callout_message', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 375, 12), )

    
    callout_message = property(__callout_message.value, __callout_message.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31image', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 376, 12), )

    
    image = property(__image.value, __image.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}position uses Python identifier position
    __position = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'position'), 'position', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31position', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 377, 12), )

    
    position = property(__position.value, __position.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}primary-flag uses Python identifier primary_flag
    __primary_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'primary-flag'), 'primary_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31primary_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 378, 12), )

    
    primary_flag = property(__primary_flag.value, __primary_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 379, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-assignments uses Python identifier variation_assignments
    __variation_assignments = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-assignments'), 'variation_assignments', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_assignments', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 380, 12), )

    
    variation_assignments = property(__variation_assignments.value, __variation_assignments.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 382, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 382, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    
    # Attribute category-id uses Python identifier category_id
    __category_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'category-id'), 'category_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_category_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __category_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 383, 8)
    __category_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 383, 8)
    
    category_id = property(__category_id.value, __category_id.set, None, None)

    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 384, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 384, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __short_description.name() : __short_description,
        __long_description.name() : __long_description,
        __callout_message.name() : __callout_message,
        __image.name() : __image,
        __position.name() : __position,
        __primary_flag.name() : __primary_flag,
        __custom_attributes.name() : __custom_attributes,
        __variation_assignments.name() : __variation_assignments
    })
    _AttributeMap.update({
        __mode.name() : __mode,
        __category_id.name() : __category_id,
        __product_id.name() : __product_id
    })
_module_typeBindings.complexType_CategoryAssignment = complexType_CategoryAssignment
Namespace.addCategoryObject('typeBinding', 'complexType.CategoryAssignment', complexType_CategoryAssignment)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Recommendation with content type ELEMENT_ONLY
class complexType_Recommendation (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Recommendation with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Recommendation')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 388, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 390, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}short-description uses Python identifier short_description
    __short_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'short-description'), 'short_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_httpwww_demandware_comxmlimpexcatalog2006_10_31short_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 391, 12), )

    
    short_description = property(__short_description.value, __short_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}long-description uses Python identifier long_description
    __long_description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'long-description'), 'long_description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_httpwww_demandware_comxmlimpexcatalog2006_10_31long_description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 392, 12), )

    
    long_description = property(__long_description.value, __long_description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}callout-message uses Python identifier callout_message
    __callout_message = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'callout-message'), 'callout_message', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_httpwww_demandware_comxmlimpexcatalog2006_10_31callout_message', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 393, 12), )

    
    callout_message = property(__callout_message.value, __callout_message.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_httpwww_demandware_comxmlimpexcatalog2006_10_31image', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 394, 12), )

    
    image = property(__image.value, __image.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_httpwww_demandware_comxmlimpexcatalog2006_10_31custom_attributes', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 395, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Attribute source-id uses Python identifier source_id
    __source_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'source-id'), 'source_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_source_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __source_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 397, 8)
    __source_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 397, 8)
    
    source_id = property(__source_id.value, __source_id.set, None, None)

    
    # Attribute source-type uses Python identifier source_type
    __source_type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'source-type'), 'source_type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_source_type', _module_typeBindings.simpleType_RecommendationSourceType)
    __source_type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 398, 8)
    __source_type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 398, 8)
    
    source_type = property(__source_type.value, __source_type.set, None, None)

    
    # Attribute target-id uses Python identifier target_id
    __target_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'target-id'), 'target_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_target_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __target_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 399, 8)
    __target_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 399, 8)
    
    target_id = property(__target_id.value, __target_id.set, None, None)

    
    # Attribute target-type uses Python identifier target_type
    __target_type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'target-type'), 'target_type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_target_type', _module_typeBindings.simpleType_RecommendationTargetType)
    __target_type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 400, 8)
    __target_type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 400, 8)
    
    target_type = property(__target_type.value, __target_type.set, None, None)

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_type', _module_typeBindings.simpleType_RecommendationType)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 401, 8)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 401, 8)
    
    type = property(__type.value, __type.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Recommendation_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 402, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 402, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __short_description.name() : __short_description,
        __long_description.name() : __long_description,
        __callout_message.name() : __callout_message,
        __image.name() : __image,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        __source_id.name() : __source_id,
        __source_type.name() : __source_type,
        __target_id.name() : __target_id,
        __target_type.name() : __target_type,
        __type.name() : __type,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_Recommendation = complexType_Recommendation
Namespace.addCategoryObject('typeBinding', 'complexType.Recommendation', complexType_Recommendation)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryLink with content type EMPTY
class complexType_CategoryLink (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryLink with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.CategoryLink')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 412, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute category-id uses Python identifier category_id
    __category_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'category-id'), 'category_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryLink_category_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __category_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 413, 8)
    __category_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 413, 8)
    
    category_id = property(__category_id.value, __category_id.set, None, None)

    
    # Attribute catalog-id uses Python identifier catalog_id
    __catalog_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'catalog-id'), 'catalog_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryLink_catalog_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __catalog_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 414, 8)
    __catalog_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 414, 8)
    
    catalog_id = property(__catalog_id.value, __catalog_id.set, None, None)

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryLink_type', _module_typeBindings.simpleType_Category_LinkType, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 415, 8)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 415, 8)
    
    type = property(__type.value, __type.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __category_id.name() : __category_id,
        __catalog_id.name() : __catalog_id,
        __type.name() : __type
    })
_module_typeBindings.complexType_CategoryLink = complexType_CategoryLink
Namespace.addCategoryObject('typeBinding', 'complexType.CategoryLink', complexType_CategoryLink)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.BundledProduct with content type ELEMENT_ONLY
class complexType_Product_BundledProduct (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.BundledProduct with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.BundledProduct')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 425, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}quantity uses Python identifier quantity
    __quantity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'quantity'), 'quantity', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_BundledProduct_httpwww_demandware_comxmlimpexcatalog2006_10_31quantity', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 427, 12), )

    
    quantity = property(__quantity.value, __quantity.set, None, None)

    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_BundledProduct_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 429, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 429, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    _ElementMap.update({
        __quantity.name() : __quantity
    })
    _AttributeMap.update({
        __product_id.name() : __product_id
    })
_module_typeBindings.complexType_Product_BundledProduct = complexType_Product_BundledProduct
Namespace.addCategoryObject('typeBinding', 'complexType.Product.BundledProduct', complexType_Product_BundledProduct)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.RetailSetProduct with content type ELEMENT_ONLY
class complexType_Product_RetailSetProduct (pyxb.binding.basis.complexTypeDefinition):
    """Deprecated please use the element product-set-product (complexType.Product.ProductSetProduct) see complexType.Product.ProductSetProducts."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.RetailSetProduct')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 442, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}quantity uses Python identifier quantity
    __quantity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'quantity'), 'quantity', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_RetailSetProduct_httpwww_demandware_comxmlimpexcatalog2006_10_31quantity', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 447, 12), )

    
    quantity = property(__quantity.value, __quantity.set, None, None)

    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_RetailSetProduct_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 449, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 449, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    _ElementMap.update({
        __quantity.name() : __quantity
    })
    _AttributeMap.update({
        __product_id.name() : __product_id
    })
_module_typeBindings.complexType_Product_RetailSetProduct = complexType_Product_RetailSetProduct
Namespace.addCategoryObject('typeBinding', 'complexType.Product.RetailSetProduct', complexType_Product_RetailSetProduct)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductSetProduct with content type EMPTY
class complexType_Product_ProductSetProduct (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductSetProduct with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ProductSetProduct')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 459, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ProductSetProduct_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 460, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 460, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __product_id.name() : __product_id
    })
_module_typeBindings.complexType_Product_ProductSetProduct = complexType_Product_ProductSetProduct
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ProductSetProduct', complexType_Product_ProductSetProduct)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.CategoryLink with content type ELEMENT_ONLY
class complexType_Product_CategoryLink (pyxb.binding.basis.complexTypeDefinition):
    """Deprecated please use the element category-assignment (complexType.CategoryAssignment)."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.CategoryLink')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 474, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}position uses Python identifier position
    __position = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'position'), 'position', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_CategoryLink_httpwww_demandware_comxmlimpexcatalog2006_10_31position', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 479, 12), )

    
    position = property(__position.value, __position.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}primary-flag uses Python identifier primary_flag
    __primary_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'primary-flag'), 'primary_flag', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_CategoryLink_httpwww_demandware_comxmlimpexcatalog2006_10_31primary_flag', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 480, 12), )

    
    primary_flag = property(__primary_flag.value, __primary_flag.set, None, None)

    
    # Attribute category-id uses Python identifier category_id
    __category_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'category-id'), 'category_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_CategoryLink_category_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __category_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 482, 8)
    __category_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 482, 8)
    
    category_id = property(__category_id.value, __category_id.set, None, None)

    
    # Attribute catalog-id uses Python identifier catalog_id
    __catalog_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'catalog-id'), 'catalog_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_CategoryLink_catalog_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __catalog_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 483, 8)
    __catalog_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 483, 8)
    
    catalog_id = property(__catalog_id.value, __catalog_id.set, None, None)

    _ElementMap.update({
        __position.name() : __position,
        __primary_flag.name() : __primary_flag
    })
    _AttributeMap.update({
        __category_id.name() : __category_id,
        __catalog_id.name() : __catalog_id
    })
_module_typeBindings.complexType_Product_CategoryLink = complexType_Product_CategoryLink
Namespace.addCategoryObject('typeBinding', 'complexType.Product.CategoryLink', complexType_Product_CategoryLink)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ClassificationCategory with content type SIMPLE
class complexType_Product_ClassificationCategory (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ClassificationCategory with content type SIMPLE"""
    _TypeDefinition = simpleType_Generic_String_256
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ClassificationCategory')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 486, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_Generic_String_256
    
    # Attribute catalog-id uses Python identifier catalog_id
    __catalog_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'catalog-id'), 'catalog_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ClassificationCategory_catalog_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __catalog_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 489, 16)
    __catalog_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 489, 16)
    
    catalog_id = property(__catalog_id.value, __catalog_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __catalog_id.name() : __catalog_id
    })
_module_typeBindings.complexType_Product_ClassificationCategory = complexType_Product_ClassificationCategory
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ClassificationCategory', complexType_Product_ClassificationCategory)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductLink with content type EMPTY
class complexType_Product_ProductLink (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.ProductLink with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.ProductLink')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 501, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ProductLink_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 502, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 502, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_ProductLink_type', _module_typeBindings.simpleType_Product_LinkType, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 503, 8)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 503, 8)
    
    type = property(__type.value, __type.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __product_id.name() : __product_id,
        __type.name() : __type
    })
_module_typeBindings.complexType_Product_ProductLink = complexType_Product_ProductLink
Namespace.addCategoryObject('typeBinding', 'complexType.Product.ProductLink', complexType_Product_ProductLink)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.VariationAttribute.Reference with content type ELEMENT_ONLY
class complexType_Product_VariationAttribute_Reference (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.VariationAttribute.Reference with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.VariationAttribute.Reference')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 537, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_VariationAttribute_Reference_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 539, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_VariationAttribute_Reference_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 541, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 541, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    
    # Attribute variation-attribute-id uses Python identifier variation_attribute_id
    __variation_attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'variation-attribute-id'), 'variation_attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_VariationAttribute_Reference_variation_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __variation_attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 542, 8)
    __variation_attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 542, 8)
    
    variation_attribute_id = property(__variation_attribute_id.value, __variation_attribute_id.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name
    })
    _AttributeMap.update({
        __attribute_id.name() : __attribute_id,
        __variation_attribute_id.name() : __variation_attribute_id
    })
_module_typeBindings.complexType_Product_VariationAttribute_Reference = complexType_Product_VariationAttribute_Reference
Namespace.addCategoryObject('typeBinding', 'complexType.Product.VariationAttribute.Reference', complexType_Product_VariationAttribute_Reference)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Variant with content type EMPTY
class complexType_Product_Variations_Variant (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Variant with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.Variant')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 559, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Variant_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 560, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 560, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Variant_default', pyxb.binding.datatypes.boolean)
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 561, 8)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 561, 8)
    
    default = property(__default.value, __default.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __product_id.name() : __product_id,
        __default.name() : __default
    })
_module_typeBindings.complexType_Product_Variations_Variant = complexType_Product_Variations_Variant
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.Variant', complexType_Product_Variations_Variant)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.VariationGroup with content type EMPTY
class complexType_Product_Variations_VariationGroup (pyxb.binding.basis.complexTypeDefinition):
    """Reserved for Beta users. Variation groups will not be created and a warning will be logged if the Variation Groups (Beta) feature is disabled."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.VariationGroup')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 582, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_VariationGroup_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 586, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 586, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __product_id.name() : __product_id
    })
_module_typeBindings.complexType_Product_Variations_VariationGroup = complexType_Product_Variations_VariationGroup
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.VariationGroup', complexType_Product_Variations_VariationGroup)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment.VariationAssignment with content type ELEMENT_ONLY
class complexType_CategoryAssignment_VariationAssignment (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.CategoryAssignment.VariationAssignment with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.CategoryAssignment.VariationAssignment')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 598, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}variation-assignment-values uses Python identifier variation_assignment_values
    __variation_assignment_values = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment-values'), 'variation_assignment_values', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_VariationAssignment_httpwww_demandware_comxmlimpexcatalog2006_10_31variation_assignment_values', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 600, 12), )

    
    variation_assignment_values = property(__variation_assignment_values.value, __variation_assignment_values.set, None, None)

    
    # Attribute variation-attribute-id uses Python identifier variation_attribute_id
    __variation_attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'variation-attribute-id'), 'variation_attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_VariationAssignment_variation_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __variation_attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 602, 8)
    __variation_attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 602, 8)
    
    variation_attribute_id = property(__variation_attribute_id.value, __variation_attribute_id.set, None, None)

    
    # Attribute slicing-attribute uses Python identifier slicing_attribute
    __slicing_attribute = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'slicing-attribute'), 'slicing_attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_CategoryAssignment_VariationAssignment_slicing_attribute', pyxb.binding.datatypes.boolean)
    __slicing_attribute._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 603, 8)
    __slicing_attribute._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 603, 8)
    
    slicing_attribute = property(__slicing_attribute.value, __slicing_attribute.set, None, None)

    _ElementMap.update({
        __variation_assignment_values.name() : __variation_assignment_values
    })
    _AttributeMap.update({
        __variation_attribute_id.name() : __variation_attribute_id,
        __slicing_attribute.name() : __slicing_attribute
    })
_module_typeBindings.complexType_CategoryAssignment_VariationAssignment = complexType_CategoryAssignment_VariationAssignment
Namespace.addCategoryObject('typeBinding', 'complexType.CategoryAssignment.VariationAssignment', complexType_CategoryAssignment_VariationAssignment)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeGroup with content type ELEMENT_ONLY
class complexType_AttributeGroup (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeGroup with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AttributeGroup')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 620, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 622, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31description', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 623, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute uses Python identifier attribute
    __attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute'), 'attribute', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeGroup_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 624, 12), )

    
    attribute = property(__attribute.value, __attribute.set, None, None)

    
    # Attribute group-id uses Python identifier group_id
    __group_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'group-id'), 'group_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeGroup_group_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __group_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 626, 8)
    __group_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 626, 8)
    
    group_id = property(__group_id.value, __group_id.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __description.name() : __description,
        __attribute.name() : __attribute
    })
    _AttributeMap.update({
        __group_id.name() : __group_id
    })
_module_typeBindings.complexType_AttributeGroup = complexType_AttributeGroup
Namespace.addCategoryObject('typeBinding', 'complexType.AttributeGroup', complexType_AttributeGroup)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeReference with content type EMPTY
class complexType_AttributeReference (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeReference with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AttributeReference')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 629, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeReference_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 630, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 630, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    
    # Attribute system uses Python identifier system
    __system = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'system'), 'system', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_AttributeReference_system', pyxb.binding.datatypes.boolean, unicode_default='false')
    __system._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 631, 8)
    __system._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 631, 8)
    
    system = property(__system.value, __system.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __attribute_id.name() : __attribute_id,
        __system.name() : __system
    })
_module_typeBindings.complexType_AttributeReference = complexType_AttributeReference
Namespace.addCategoryObject('typeBinding', 'complexType.AttributeReference', complexType_AttributeReference)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Attributes.Attribute.Value with content type ELEMENT_ONLY
class complexType_Product_Variations_Attributes_Attribute_Value (pyxb.binding.basis.complexTypeDefinition):
    """Deprecated please use the element variation-attribute-value (complexType.VariationAttribute.Value) see complexType.VariationAttribute.Values."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.Attributes.Attribute.Value')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 656, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-value uses Python identifier display_value
    __display_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-value'), 'display_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_Attribute_Value_httpwww_demandware_comxmlimpexcatalog2006_10_31display_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 661, 12), )

    
    display_value = property(__display_value.value, __display_value.set, None, None)

    
    # Attribute value-id uses Python identifier value_id
    __value_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'value-id'), 'value_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_Attribute_Value_value_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __value_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 663, 8)
    __value_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 663, 8)
    
    value_id = property(__value_id.value, __value_id.set, None, None)

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_Attribute_Value_default', pyxb.binding.datatypes.boolean)
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 664, 8)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 664, 8)
    
    default = property(__default.value, __default.set, None, None)

    _ElementMap.update({
        __display_value.name() : __display_value
    })
    _AttributeMap.update({
        __value_id.name() : __value_id,
        __default.name() : __default
    })
_module_typeBindings.complexType_Product_Variations_Attributes_Attribute_Value = complexType_Product_Variations_Attributes_Attribute_Value
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.Attributes.Attribute.Value', complexType_Product_Variations_Attributes_Attribute_Value)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.RefinementDefinition with content type ELEMENT_ONLY
class complexType_RefinementDefinition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.RefinementDefinition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.RefinementDefinition')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 675, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 677, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value-set uses Python identifier value_set
    __value_set = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value-set'), 'value_set', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31value_set', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 678, 12), )

    
    value_set = property(__value_set.value, __value_set.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sort-mode uses Python identifier sort_mode
    __sort_mode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sort-mode'), 'sort_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31sort_mode', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 679, 12), )

    
    sort_mode = property(__sort_mode.value, __sort_mode.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}sort-direction uses Python identifier sort_direction
    __sort_direction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sort-direction'), 'sort_direction', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31sort_direction', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 680, 12), )

    
    sort_direction = property(__sort_direction.value, __sort_direction.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}unbucketed-values-mode uses Python identifier unbucketed_values_mode
    __unbucketed_values_mode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'unbucketed-values-mode'), 'unbucketed_values_mode', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31unbucketed_values_mode', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 681, 12), )

    
    unbucketed_values_mode = property(__unbucketed_values_mode.value, __unbucketed_values_mode.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}cutoff-threshold uses Python identifier cutoff_threshold
    __cutoff_threshold = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'cutoff-threshold'), 'cutoff_threshold', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31cutoff_threshold', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 682, 12), )

    
    cutoff_threshold = property(__cutoff_threshold.value, __cutoff_threshold.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}bucket-definitions uses Python identifier bucket_definitions
    __bucket_definitions = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'bucket-definitions'), 'bucket_definitions', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_httpwww_demandware_comxmlimpexcatalog2006_10_31bucket_definitions', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 683, 12), )

    
    bucket_definitions = property(__bucket_definitions.value, __bucket_definitions.set, None, None)

    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_type', _module_typeBindings.simpleType_Refinement_Type, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 685, 8)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 685, 8)
    
    type = property(__type.value, __type.set, None, None)

    
    # Attribute bucket-type uses Python identifier bucket_type
    __bucket_type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'bucket-type'), 'bucket_type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_bucket_type', _module_typeBindings.simpleType_Bucket_Type)
    __bucket_type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 686, 8)
    __bucket_type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 686, 8)
    
    bucket_type = property(__bucket_type.value, __bucket_type.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 687, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 687, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    
    # Attribute system uses Python identifier system
    __system = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'system'), 'system', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_RefinementDefinition_system', pyxb.binding.datatypes.boolean, unicode_default='false')
    __system._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 688, 8)
    __system._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 688, 8)
    
    system = property(__system.value, __system.set, None, None)

    _ElementMap.update({
        __display_name.name() : __display_name,
        __value_set.name() : __value_set,
        __sort_mode.name() : __sort_mode,
        __sort_direction.name() : __sort_direction,
        __unbucketed_values_mode.name() : __unbucketed_values_mode,
        __cutoff_threshold.name() : __cutoff_threshold,
        __bucket_definitions.name() : __bucket_definitions
    })
    _AttributeMap.update({
        __type.name() : __type,
        __bucket_type.name() : __bucket_type,
        __attribute_id.name() : __attribute_id,
        __system.name() : __system
    })
_module_typeBindings.complexType_RefinementDefinition = complexType_RefinementDefinition
Namespace.addCategoryObject('typeBinding', 'complexType.RefinementDefinition', complexType_RefinementDefinition)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.BlockedRefinementDefinition with content type EMPTY
class complexType_BlockedRefinementDefinition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.BlockedRefinementDefinition with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.BlockedRefinementDefinition')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 691, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute type uses Python identifier type
    __type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'type'), 'type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_BlockedRefinementDefinition_type', _module_typeBindings.simpleType_Refinement_Type, required=True)
    __type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 692, 8)
    __type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 692, 8)
    
    type = property(__type.value, __type.set, None, None)

    
    # Attribute bucket-type uses Python identifier bucket_type
    __bucket_type = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'bucket-type'), 'bucket_type', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_BlockedRefinementDefinition_bucket_type', _module_typeBindings.simpleType_Bucket_Type)
    __bucket_type._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 693, 8)
    __bucket_type._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 693, 8)
    
    bucket_type = property(__bucket_type.value, __bucket_type.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_BlockedRefinementDefinition_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 694, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 694, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    
    # Attribute system uses Python identifier system
    __system = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'system'), 'system', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_BlockedRefinementDefinition_system', pyxb.binding.datatypes.boolean, unicode_default='false')
    __system._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 695, 8)
    __system._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 695, 8)
    
    system = property(__system.value, __system.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __type.name() : __type,
        __bucket_type.name() : __bucket_type,
        __attribute_id.name() : __attribute_id,
        __system.name() : __system
    })
_module_typeBindings.complexType_BlockedRefinementDefinition = complexType_BlockedRefinementDefinition
Namespace.addCategoryObject('typeBinding', 'complexType.BlockedRefinementDefinition', complexType_BlockedRefinementDefinition)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Option.Reference with content type EMPTY
class complexType_Product_Option_Reference (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Option.Reference with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Option.Reference')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 756, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute option-id uses Python identifier option_id
    __option_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'option-id'), 'option_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Option_Reference_option_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __option_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 757, 8)
    __option_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 757, 8)
    
    option_id = property(__option_id.value, __option_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __option_id.name() : __option_id
    })
_module_typeBindings.complexType_Product_Option_Reference = complexType_Product_Option_Reference
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Option.Reference', complexType_Product_Option_Reference)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Value with content type ELEMENT_ONLY
class complexType_Option_Value (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Option.Value with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Option.Value')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 767, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}display-value uses Python identifier display_value
    __display_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-value'), 'display_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_httpwww_demandware_comxmlimpexcatalog2006_10_31display_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 769, 12), )

    
    display_value = property(__display_value.value, __display_value.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}product-id-modifier uses Python identifier product_id_modifier
    __product_id_modifier = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'product-id-modifier'), 'product_id_modifier', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_httpwww_demandware_comxmlimpexcatalog2006_10_31product_id_modifier', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 770, 12), )

    
    product_id_modifier = property(__product_id_modifier.value, __product_id_modifier.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}option-value-prices uses Python identifier option_value_prices
    __option_value_prices = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'option-value-prices'), 'option_value_prices', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_httpwww_demandware_comxmlimpexcatalog2006_10_31option_value_prices', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 771, 12), )

    
    option_value_prices = property(__option_value_prices.value, __option_value_prices.set, None, None)

    
    # Attribute value-id uses Python identifier value_id
    __value_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'value-id'), 'value_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_value_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __value_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 773, 8)
    __value_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 773, 8)
    
    value_id = property(__value_id.value, __value_id.set, None, None)

    
    # Attribute default uses Python identifier default
    __default = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'default'), 'default', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Option_Value_default', pyxb.binding.datatypes.boolean)
    __default._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 774, 8)
    __default._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 774, 8)
    
    default = property(__default.value, __default.set, None, None)

    _ElementMap.update({
        __display_value.name() : __display_value,
        __product_id_modifier.name() : __product_id_modifier,
        __option_value_prices.name() : __option_value_prices
    })
    _AttributeMap.update({
        __value_id.name() : __value_id,
        __default.name() : __default
    })
_module_typeBindings.complexType_Option_Value = complexType_Option_Value
Namespace.addCategoryObject('typeBinding', 'complexType.Option.Value', complexType_Option_Value)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.LocalizedString with content type SIMPLE
class sharedType_LocalizedString (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.LocalizedString with content type SIMPLE"""
    _TypeDefinition = simpleType_Generic_String_4000
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.LocalizedString')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 817, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_Generic_String_4000
    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_LocalizedString_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 820, 16)
    
    lang = property(__lang.value, __lang.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __lang.name() : __lang
    })
_module_typeBindings.sharedType_LocalizedString = sharedType_LocalizedString
Namespace.addCategoryObject('typeBinding', 'sharedType.LocalizedString', sharedType_LocalizedString)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttribute with content type MIXED
class sharedType_CustomAttribute (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttribute with content type MIXED"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_MIXED
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 831, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_CustomAttribute_httpwww_demandware_comxmlimpexcatalog2006_10_31value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 833, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_CustomAttribute_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 836, 8)
    
    lang = property(__lang.value, __lang.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_CustomAttribute_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 835, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 835, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        __lang.name() : __lang,
        __attribute_id.name() : __attribute_id
    })
_module_typeBindings.sharedType_CustomAttribute = sharedType_CustomAttribute
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttribute', sharedType_CustomAttribute)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificBoolean with content type SIMPLE
class sharedType_SiteSpecificBoolean (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificBoolean with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.boolean
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificBoolean')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 853, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.boolean
    
    # Attribute site-id uses Python identifier site_id
    __site_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'site-id'), 'site_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificBoolean_site_id', _module_typeBindings.simpleType_Generic_NonEmptyString_32)
    __site_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 856, 16)
    __site_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 856, 16)
    
    site_id = property(__site_id.value, __site_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __site_id.name() : __site_id
    })
_module_typeBindings.sharedType_SiteSpecificBoolean = sharedType_SiteSpecificBoolean
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificBoolean', sharedType_SiteSpecificBoolean)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificInt with content type SIMPLE
class sharedType_SiteSpecificInt (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificInt with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.int
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificInt')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 861, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.int
    
    # Attribute site-id uses Python identifier site_id
    __site_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'site-id'), 'site_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificInt_site_id', _module_typeBindings.simpleType_Generic_NonEmptyString_32)
    __site_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 864, 16)
    __site_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 864, 16)
    
    site_id = property(__site_id.value, __site_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __site_id.name() : __site_id
    })
_module_typeBindings.sharedType_SiteSpecificInt = sharedType_SiteSpecificInt
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificInt', sharedType_SiteSpecificInt)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificDateTime with content type SIMPLE
class sharedType_SiteSpecificDateTime (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificDateTime with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.dateTime
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificDateTime')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 869, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.dateTime
    
    # Attribute site-id uses Python identifier site_id
    __site_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'site-id'), 'site_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificDateTime_site_id', _module_typeBindings.simpleType_Generic_NonEmptyString_32)
    __site_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 872, 16)
    __site_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 872, 16)
    
    site_id = property(__site_id.value, __site_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __site_id.name() : __site_id
    })
_module_typeBindings.sharedType_SiteSpecificDateTime = sharedType_SiteSpecificDateTime
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificDateTime', sharedType_SiteSpecificDateTime)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificSiteMapChangeFrequency with content type SIMPLE
class sharedType_SiteSpecificSiteMapChangeFrequency (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificSiteMapChangeFrequency with content type SIMPLE"""
    _TypeDefinition = simpleType_SiteMapChangeFrequency
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificSiteMapChangeFrequency')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 877, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_SiteMapChangeFrequency
    
    # Attribute site-id uses Python identifier site_id
    __site_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'site-id'), 'site_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificSiteMapChangeFrequency_site_id', _module_typeBindings.simpleType_Generic_NonEmptyString_32)
    __site_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 880, 16)
    __site_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 880, 16)
    
    site_id = property(__site_id.value, __site_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __site_id.name() : __site_id
    })
_module_typeBindings.sharedType_SiteSpecificSiteMapChangeFrequency = sharedType_SiteSpecificSiteMapChangeFrequency
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificSiteMapChangeFrequency', sharedType_SiteSpecificSiteMapChangeFrequency)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificSiteMapPriority with content type SIMPLE
class sharedType_SiteSpecificSiteMapPriority (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificSiteMapPriority with content type SIMPLE"""
    _TypeDefinition = simpleType_SiteMapPriority
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificSiteMapPriority')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 885, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_SiteMapPriority
    
    # Attribute site-id uses Python identifier site_id
    __site_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'site-id'), 'site_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificSiteMapPriority_site_id', _module_typeBindings.simpleType_Generic_NonEmptyString_32)
    __site_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 888, 16)
    __site_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 888, 16)
    
    site_id = property(__site_id.value, __site_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __site_id.name() : __site_id
    })
_module_typeBindings.sharedType_SiteSpecificSiteMapPriority = sharedType_SiteSpecificSiteMapPriority
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificSiteMapPriority', sharedType_SiteSpecificSiteMapPriority)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PageMetaTagRule with content type EMPTY
class complexType_PageMetaTagRule (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.PageMetaTagRule with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PageMetaTagRule')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 899, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute rule-id uses Python identifier rule_id
    __rule_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'rule-id'), 'rule_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_PageMetaTagRule_rule_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __rule_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 900, 8)
    __rule_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 900, 8)
    
    rule_id = property(__rule_id.value, __rule_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __rule_id.name() : __rule_id
    })
_module_typeBindings.complexType_PageMetaTagRule = complexType_PageMetaTagRule
Namespace.addCategoryObject('typeBinding', 'complexType.PageMetaTagRule', complexType_PageMetaTagRule)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductCategoryFilter with content type ELEMENT_ONLY
class complexType_ProductSpecification_ProductCategoryFilter (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductCategoryFilter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification.ProductCategoryFilter')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 950, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}category-id uses Python identifier category_id
    __category_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'category-id'), 'category_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductCategoryFilter_httpwww_demandware_comxmlimpexcatalog2006_10_31category_id', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 952, 12), )

    
    category_id = property(__category_id.value, __category_id.set, None, None)

    
    # Attribute catalog-id uses Python identifier catalog_id
    __catalog_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'catalog-id'), 'catalog_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductCategoryFilter_catalog_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __catalog_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 954, 8)
    __catalog_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 954, 8)
    
    catalog_id = property(__catalog_id.value, __catalog_id.set, None, None)

    
    # Attribute operator uses Python identifier operator
    __operator = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'operator'), 'operator', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductCategoryFilter_operator', _module_typeBindings.simpleType_CategoryIDComparator)
    __operator._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 955, 8)
    __operator._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 955, 8)
    
    operator = property(__operator.value, __operator.set, None, None)

    _ElementMap.update({
        __category_id.name() : __category_id
    })
    _AttributeMap.update({
        __catalog_id.name() : __catalog_id,
        __operator.name() : __operator
    })
_module_typeBindings.complexType_ProductSpecification_ProductCategoryFilter = complexType_ProductSpecification_ProductCategoryFilter
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification.ProductCategoryFilter', complexType_ProductSpecification_ProductCategoryFilter)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductAttributeFilter with content type ELEMENT_ONLY
class complexType_ProductSpecification_ProductAttributeFilter (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.ProductSpecification.ProductAttributeFilter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.ProductSpecification.ProductAttributeFilter')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 958, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}attribute-value uses Python identifier attribute_value
    __attribute_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute-value'), 'attribute_value', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductAttributeFilter_httpwww_demandware_comxmlimpexcatalog2006_10_31attribute_value', True, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 960, 12), )

    
    attribute_value = property(__attribute_value.value, __attribute_value.set, None, None)

    
    # Attribute operator uses Python identifier operator
    __operator = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'operator'), 'operator', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductAttributeFilter_operator', _module_typeBindings.simpleType_AttributeComparator)
    __operator._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 962, 8)
    __operator._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 962, 8)
    
    operator = property(__operator.value, __operator.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_ProductSpecification_ProductAttributeFilter_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 963, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 963, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    _ElementMap.update({
        __attribute_value.name() : __attribute_value
    })
    _AttributeMap.update({
        __operator.name() : __operator,
        __attribute_id.name() : __attribute_id
    })
_module_typeBindings.complexType_ProductSpecification_ProductAttributeFilter = complexType_ProductSpecification_ProductAttributeFilter
Namespace.addCategoryObject('typeBinding', 'complexType.ProductSpecification.ProductAttributeFilter', complexType_ProductSpecification_ProductAttributeFilter)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.Product.Variations.Attributes.Attribute with content type ELEMENT_ONLY
class complexType_Product_Variations_Attributes_Attribute (complexType_AttributeReference):
    """Deprecated please use the elements variation-attribute (complexType.VariationAttribute) or shared-variation-attribute (complexType.Product.VariationAttribute.Reference)."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Product.Variations.Attributes.Attribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 634, 4)
    _ElementMap = complexType_AttributeReference._ElementMap.copy()
    _AttributeMap = complexType_AttributeReference._AttributeMap.copy()
    # Base type is complexType_AttributeReference
    
    # Element {http://www.demandware.com/xml/impex/catalog/2006-10-31}values uses Python identifier values
    __values = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'values'), 'values', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_complexType_Product_Variations_Attributes_Attribute_httpwww_demandware_comxmlimpexcatalog2006_10_31values', False, pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 641, 20), )

    
    values = property(__values.value, __values.set, None, None)

    
    # Attribute attribute_id inherited from {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeReference
    
    # Attribute system inherited from {http://www.demandware.com/xml/impex/catalog/2006-10-31}complexType.AttributeReference
    _ElementMap.update({
        __values.name() : __values
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_Product_Variations_Attributes_Attribute = complexType_Product_Variations_Attributes_Attribute
Namespace.addCategoryObject('typeBinding', 'complexType.Product.Variations.Attributes.Attribute', complexType_Product_Variations_Attributes_Attribute)


# Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificCustomAttribute with content type MIXED
class sharedType_SiteSpecificCustomAttribute (sharedType_CustomAttribute):
    """Complex type {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.SiteSpecificCustomAttribute with content type MIXED"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_MIXED
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.SiteSpecificCustomAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 845, 4)
    _ElementMap = sharedType_CustomAttribute._ElementMap.copy()
    _AttributeMap = sharedType_CustomAttribute._AttributeMap.copy()
    # Base type is sharedType_CustomAttribute
    
    # Element value_ ({http://www.demandware.com/xml/impex/catalog/2006-10-31}value) inherited from {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttribute
    
    # Attribute lang inherited from {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttribute
    
    # Attribute attribute_id inherited from {http://www.demandware.com/xml/impex/catalog/2006-10-31}sharedType.CustomAttribute
    
    # Attribute site-id uses Python identifier site_id
    __site_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'site-id'), 'site_id', '__httpwww_demandware_comxmlimpexcatalog2006_10_31_sharedType_SiteSpecificCustomAttribute_site_id', _module_typeBindings.simpleType_Generic_NonEmptyString_32)
    __site_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 848, 16)
    __site_id._UseLocation = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 848, 16)
    
    site_id = property(__site_id.value, __site_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __site_id.name() : __site_id
    })
_module_typeBindings.sharedType_SiteSpecificCustomAttribute = sharedType_SiteSpecificCustomAttribute
Namespace.addCategoryObject('typeBinding', 'sharedType.SiteSpecificCustomAttribute', sharedType_SiteSpecificCustomAttribute)


header = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'header'), complexType_Header, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 30, 4))
Namespace.addCategoryObject('elementBinding', header.name().localName(), header)

product_attribute_definitions = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-attribute-definitions'), complexType_AttributeDefinitions, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 31, 4))
Namespace.addCategoryObject('elementBinding', product_attribute_definitions.name().localName(), product_attribute_definitions)

catalog = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'catalog'), CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 13, 4))
Namespace.addCategoryObject('elementBinding', catalog.name().localName(), catalog)

category = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category'), complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 32, 4))
Namespace.addCategoryObject('elementBinding', category.name().localName(), category)

product_option = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-option'), complexType_Option, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 33, 4))
Namespace.addCategoryObject('elementBinding', product_option.name().localName(), product_option)

variation_attribute = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute'), complexType_VariationAttribute, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 34, 4))
Namespace.addCategoryObject('elementBinding', variation_attribute.name().localName(), variation_attribute)

product = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product'), complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 35, 4))
Namespace.addCategoryObject('elementBinding', product.name().localName(), product)

category_assignment = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-assignment'), complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 36, 4))
Namespace.addCategoryObject('elementBinding', category_assignment.name().localName(), category_assignment)

recommendation = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'recommendation'), complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 37, 4))
Namespace.addCategoryObject('elementBinding', recommendation.name().localName(), recommendation)



complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image-settings'), complexType_Image_Settings, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 42, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 43, 12)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 42, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 43, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image-settings')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 42, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 43, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Header._Automaton = _BuildAutomaton()




complexType_Image_Settings._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'internal-location'), complexType_Image_InternalLocation, scope=complexType_Image_Settings, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 50, 16)))

complexType_Image_Settings._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'external-location'), complexType_Image_ExternalLocation, scope=complexType_Image_Settings, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 51, 16)))

complexType_Image_Settings._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'view-types'), complexType_Image_ViewTypes, scope=complexType_Image_Settings, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 53, 12)))

complexType_Image_Settings._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-id'), simpleType_Generic_NonEmptyString_256, scope=complexType_Image_Settings, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 54, 12)))

complexType_Image_Settings._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'alt-pattern'), simpleType_Generic_String_256, scope=complexType_Image_Settings, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 55, 12)))

complexType_Image_Settings._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'title-pattern'), simpleType_Generic_String_256, scope=complexType_Image_Settings, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 56, 12)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 50, 16))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 51, 16))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 53, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 54, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 55, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 56, 12))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_Settings._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'internal-location')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 50, 16))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_Settings._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'external-location')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 51, 16))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_Settings._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'view-types')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 53, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_Settings._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 54, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_Settings._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'alt-pattern')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 55, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_Settings._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'title-pattern')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 56, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Image_Settings._Automaton = _BuildAutomaton_()




complexType_Image_ViewTypes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'view-type'), simpleType_Generic_NonEmptyString_256, scope=complexType_Image_ViewTypes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 62, 12)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 62, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_ViewTypes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'view-type')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 62, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Image_ViewTypes._Automaton = _BuildAutomaton_2()




complexType_Image_ExternalLocation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'http-url'), simpleType_Generic_NonEmptyString_256, scope=complexType_Image_ExternalLocation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 72, 12)))

complexType_Image_ExternalLocation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'https-url'), simpleType_Generic_NonEmptyString_256, scope=complexType_Image_ExternalLocation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 73, 12)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 73, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_Image_ExternalLocation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'http-url')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 72, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Image_ExternalLocation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'https-url')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 73, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_Image_ExternalLocation._Automaton = _BuildAutomaton_3()




complexType_AttributeDefinitions._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute-definition'), complexType_AttributeDefinition, scope=complexType_AttributeDefinitions, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 79, 12)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 79, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinitions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute-definition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 79, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_AttributeDefinitions._Automaton = _BuildAutomaton_4()




complexType_Product_StoreAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'receipt-name'), sharedType_LocalizedText, scope=complexType_Product_StoreAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 296, 12)))

complexType_Product_StoreAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'tax-class'), simpleType_Generic_String_60, scope=complexType_Product_StoreAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 297, 12)))

complexType_Product_StoreAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'force-price-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product_StoreAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 298, 12)))

complexType_Product_StoreAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'non-inventory-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product_StoreAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 299, 12)))

complexType_Product_StoreAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'non-revenue-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product_StoreAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 300, 12)))

complexType_Product_StoreAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'non-discountable-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product_StoreAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 301, 12)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 296, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 297, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 298, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 299, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 300, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 301, 12))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_StoreAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'receipt-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 296, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_StoreAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'tax-class')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 297, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_StoreAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'force-price-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 298, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_StoreAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'non-inventory-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 299, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_StoreAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'non-revenue-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 300, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_StoreAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'non-discountable-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 301, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_StoreAttributes._Automaton = _BuildAutomaton_5()




complexType_CategoryLinks._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-link'), complexType_CategoryLink, scope=complexType_CategoryLinks, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 408, 12)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 408, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryLinks._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-link')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 408, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_CategoryLinks._Automaton = _BuildAutomaton_6()




complexType_Product_BundledProducts._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'bundled-product'), complexType_Product_BundledProduct, scope=complexType_Product_BundledProducts, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 421, 12)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 421, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_BundledProducts._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'bundled-product')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 421, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_BundledProducts._Automaton = _BuildAutomaton_7()




complexType_Product_RetailSetProducts._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'retail-set-product'), complexType_Product_RetailSetProduct, scope=complexType_Product_RetailSetProducts, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 438, 12)))

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 438, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_RetailSetProducts._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'retail-set-product')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 438, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_RetailSetProducts._Automaton = _BuildAutomaton_8()




complexType_Product_ProductSetProducts._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-set-product'), complexType_Product_ProductSetProduct, scope=complexType_Product_ProductSetProducts, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 455, 12)))

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 455, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_ProductSetProducts._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-set-product')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 455, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_ProductSetProducts._Automaton = _BuildAutomaton_9()




complexType_Product_CategoryLinks._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'classification-link'), complexType_Product_CategoryLink, scope=complexType_Product_CategoryLinks, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 469, 12)))

complexType_Product_CategoryLinks._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-link'), complexType_Product_CategoryLink, scope=complexType_Product_CategoryLinks, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 470, 12)))

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 469, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 470, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_CategoryLinks._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'classification-link')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 469, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_CategoryLinks._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-link')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 470, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_CategoryLinks._Automaton = _BuildAutomaton_10()




complexType_Product_ProductLinks._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-link'), complexType_Product_ProductLink, scope=complexType_Product_ProductLinks, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 497, 12)))

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 497, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_ProductLinks._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-link')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 497, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_ProductLinks._Automaton = _BuildAutomaton_11()




complexType_PageAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-title'), sharedType_LocalizedString, scope=complexType_PageAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 509, 12)))

complexType_PageAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-description'), sharedType_LocalizedString, scope=complexType_PageAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 510, 12)))

complexType_PageAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-keywords'), sharedType_LocalizedString, scope=complexType_PageAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 511, 12)))

complexType_PageAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-url'), sharedType_LocalizedString, scope=complexType_PageAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 512, 12)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 509, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 510, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 511, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 512, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PageAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-title')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 509, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PageAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 510, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PageAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-keywords')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 511, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PageAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-url')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 512, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_PageAttributes._Automaton = _BuildAutomaton_12()




complexType_Product_Variations._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attributes'), complexType_Product_Variations_Attributes, scope=complexType_Product_Variations, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 519, 12)))

complexType_Product_Variations._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variants'), complexType_Product_Variations_Variants, scope=complexType_Product_Variations, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 520, 12)))

complexType_Product_Variations._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-groups'), complexType_Product_Variations_VariationGroups, scope=complexType_Product_Variations, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 521, 12)))

def _BuildAutomaton_13 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_13
    del _BuildAutomaton_13
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 519, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=2, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 520, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 521, 12))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 519, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variants')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 520, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-groups')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 521, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations._Automaton = _BuildAutomaton_13()




complexType_Product_Variations_Attributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute'), complexType_Product_Variations_Attributes_Attribute, scope=complexType_Product_Variations_Attributes, documentation='Deprecated please use the elements variation-attribute (complexType.VariationAttribute) or shared-variation-attribute (complexType.Product.VariationAttribute.Reference).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 527, 12)))

complexType_Product_Variations_Attributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute'), complexType_VariationAttribute, scope=complexType_Product_Variations_Attributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 532, 12)))

complexType_Product_Variations_Attributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'shared-variation-attribute'), complexType_Product_VariationAttribute_Reference, scope=complexType_Product_Variations_Attributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 533, 12)))

def _BuildAutomaton_14 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_14
    del _BuildAutomaton_14
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 526, 8))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Attributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 527, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Attributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 532, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Attributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'shared-variation-attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 533, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations_Attributes._Automaton = _BuildAutomaton_14()




complexType_CategoryAssignment_VariationAssignments._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment'), complexType_CategoryAssignment_VariationAssignment, scope=complexType_CategoryAssignment_VariationAssignments, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 593, 12)))

def _BuildAutomaton_15 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_15
    del _BuildAutomaton_15
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 593, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment_VariationAssignments._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 593, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_CategoryAssignment_VariationAssignments._Automaton = _BuildAutomaton_15()




complexType_CategoryAssignment_VariationAssignment_Values._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment-value'), simpleType_Generic_NonEmptyString_256, scope=complexType_CategoryAssignment_VariationAssignment_Values, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 609, 12)))

def _BuildAutomaton_16 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_16
    del _BuildAutomaton_16
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 609, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment_VariationAssignment_Values._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 609, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_CategoryAssignment_VariationAssignment_Values._Automaton = _BuildAutomaton_16()




complexType_AttributeGroups._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute-group'), complexType_AttributeGroup, scope=complexType_AttributeGroups, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 616, 12)))

def _BuildAutomaton_17 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_17
    del _BuildAutomaton_17
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 616, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeGroups._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute-group')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 616, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_AttributeGroups._Automaton = _BuildAutomaton_17()




complexType_Product_Variations_Attributes_Attribute_Values._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), complexType_Product_Variations_Attributes_Attribute_Value, scope=complexType_Product_Variations_Attributes_Attribute_Values, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 652, 12)))

def _BuildAutomaton_18 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_18
    del _BuildAutomaton_18
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 652, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Attributes_Attribute_Values._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 652, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations_Attributes_Attribute_Values._Automaton = _BuildAutomaton_18()




complexType_RefinementDefinitions._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'refinement-definition'), complexType_RefinementDefinition, scope=complexType_RefinementDefinitions, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 670, 12)))

complexType_RefinementDefinitions._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'blocked-refinement-definition'), complexType_BlockedRefinementDefinition, scope=complexType_RefinementDefinitions, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 671, 12)))

def _BuildAutomaton_19 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_19
    del _BuildAutomaton_19
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 670, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 671, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinitions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'refinement-definition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 670, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinitions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'blocked-refinement-definition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 671, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_RefinementDefinitions._Automaton = _BuildAutomaton_19()




complexType_RefinementBuckets._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute-bucket'), complexType_AttributeRefinementBucket, scope=complexType_RefinementBuckets, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 700, 12)))

complexType_RefinementBuckets._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-bucket'), complexType_PriceRefinementBucket, scope=complexType_RefinementBuckets, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 701, 12)))

complexType_RefinementBuckets._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'threshold-bucket'), complexType_ThresholdRefinementBucket, scope=complexType_RefinementBuckets, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 702, 12)))

complexType_RefinementBuckets._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'period-bucket'), complexType_PeriodRefinementBucket, scope=complexType_RefinementBuckets, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 703, 12)))

def _BuildAutomaton_20 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_20
    del _BuildAutomaton_20
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 700, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 701, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 702, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 703, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementBuckets._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute-bucket')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 700, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementBuckets._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'price-bucket')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 701, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementBuckets._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'threshold-bucket')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 702, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementBuckets._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'period-bucket')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 703, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_RefinementBuckets._Automaton = _BuildAutomaton_20()




complexType_AttributeRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_AttributeRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 709, 12)))

complexType_AttributeRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value-list'), sharedType_LocalizedText, scope=complexType_AttributeRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 710, 12)))

complexType_AttributeRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'presentation-id'), simpleType_Generic_NonEmptyString_256, scope=complexType_AttributeRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 711, 12)))

complexType_AttributeRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_AttributeRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 712, 12)))

def _BuildAutomaton_21 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_21
    del _BuildAutomaton_21
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 709, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 710, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 711, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 712, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 709, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value-list')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 710, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'presentation-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 711, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 712, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_AttributeRefinementBucket._Automaton = _BuildAutomaton_21()




complexType_ThresholdRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_ThresholdRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 732, 12)))

complexType_ThresholdRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'threshold'), sharedType_LocalizedText, scope=complexType_ThresholdRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 733, 12)))

complexType_ThresholdRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'presentation-id'), simpleType_Generic_NonEmptyString_256, scope=complexType_ThresholdRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 734, 12)))

complexType_ThresholdRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_ThresholdRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 735, 12)))

def _BuildAutomaton_22 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_22
    del _BuildAutomaton_22
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 732, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 733, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 734, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 735, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ThresholdRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 732, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ThresholdRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'threshold')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 733, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ThresholdRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'presentation-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 734, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ThresholdRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 735, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ThresholdRefinementBucket._Automaton = _BuildAutomaton_22()




complexType_PeriodRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_PeriodRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 741, 12)))

complexType_PeriodRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'duration-in-days'), pyxb.binding.datatypes.int, scope=complexType_PeriodRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 742, 12)))

complexType_PeriodRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'presentation-id'), simpleType_Generic_NonEmptyString_256, scope=complexType_PeriodRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 743, 12)))

complexType_PeriodRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_PeriodRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 744, 12)))

def _BuildAutomaton_23 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_23
    del _BuildAutomaton_23
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 741, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 743, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 744, 12))
    counters.add(cc_2)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_PeriodRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 741, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_PeriodRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'duration-in-days')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 742, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PeriodRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'presentation-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 743, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PeriodRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 744, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_PeriodRefinementBucket._Automaton = _BuildAutomaton_23()




complexType_Product_Options._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'option'), complexType_Option, scope=complexType_Product_Options, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 751, 12)))

complexType_Product_Options._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'shared-option'), complexType_Product_Option_Reference, scope=complexType_Product_Options, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 752, 12)))

def _BuildAutomaton_24 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_24
    del _BuildAutomaton_24
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 750, 8))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Options._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'option')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 751, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Options._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'shared-option')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 752, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Options._Automaton = _BuildAutomaton_24()




complexType_Option_Values._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'option-value'), complexType_Option_Value, scope=complexType_Option_Values, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 763, 12)))

def _BuildAutomaton_25 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_25
    del _BuildAutomaton_25
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 763, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option_Values._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'option-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 763, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Option_Values._Automaton = _BuildAutomaton_25()




complexType_Option_Value_Prices._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'option-value-price'), complexType_Option_Value_Price, scope=complexType_Option_Value_Prices, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 780, 12)))

def _BuildAutomaton_26 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_26
    del _BuildAutomaton_26
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 780, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option_Value_Prices._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'option-value-price')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 780, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Option_Value_Prices._Automaton = _BuildAutomaton_26()




complexType_ValueDefinitions._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value-definition'), complexType_ValueDefinition, scope=complexType_ValueDefinitions, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 795, 12)))

def _BuildAutomaton_27 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_27
    del _BuildAutomaton_27
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 795, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ValueDefinitions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value-definition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 795, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ValueDefinitions._Automaton = _BuildAutomaton_27()




complexType_ValueDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display'), sharedType_LocalizedString, scope=complexType_ValueDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 802, 12)))

complexType_ValueDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), simpleType_Generic_NonEmptyString_4000, scope=complexType_ValueDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 803, 12)))

def _BuildAutomaton_28 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_28
    del _BuildAutomaton_28
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 802, 12))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_ValueDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 802, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_ValueDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 803, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_ValueDefinition._Automaton = _BuildAutomaton_28()




sharedType_CustomAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), sharedType_CustomAttribute, scope=sharedType_CustomAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 827, 12)))

def _BuildAutomaton_29 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_29
    del _BuildAutomaton_29
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 827, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 827, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttributes._Automaton = _BuildAutomaton_29()




sharedType_SiteSpecificCustomAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), sharedType_SiteSpecificCustomAttribute, scope=sharedType_SiteSpecificCustomAttributes, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 841, 12)))

def _BuildAutomaton_30 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_30
    del _BuildAutomaton_30
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 841, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_SiteSpecificCustomAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 841, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_SiteSpecificCustomAttributes._Automaton = _BuildAutomaton_30()




complexType_PageMetaTagRules._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-meta-tag-rule'), complexType_PageMetaTagRule, scope=complexType_PageMetaTagRules, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 895, 12)))

def _BuildAutomaton_31 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_31
    del _BuildAutomaton_31
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=50, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 895, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PageMetaTagRules._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-meta-tag-rule')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 895, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_PageMetaTagRules._Automaton = _BuildAutomaton_31()




complexType_ProductSpecificationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), simpleType_Generic_NonEmptyString_256, scope=complexType_ProductSpecificationRule, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 906, 12)))

complexType_ProductSpecificationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), simpleType_Generic_NonEmptyString_4000, scope=complexType_ProductSpecificationRule, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 907, 12)))

complexType_ProductSpecificationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'enabled-flag'), pyxb.binding.datatypes.boolean, scope=complexType_ProductSpecificationRule, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 908, 12)))

complexType_ProductSpecificationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'last-calculated'), pyxb.binding.datatypes.dateTime, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_ProductSpecificationRule, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 909, 12)))

complexType_ProductSpecificationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-specification'), complexType_ProductSpecification, scope=complexType_ProductSpecificationRule, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 910, 12)))

def _BuildAutomaton_32 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_32
    del _BuildAutomaton_32
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 907, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 909, 12))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecificationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 906, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecificationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 907, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecificationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'enabled-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 908, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecificationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'last-calculated')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 909, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecificationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-specification')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 910, 12))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_ProductSpecificationRule._Automaton = _BuildAutomaton_32()




complexType_ProductSpecification._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'included-products'), complexType_ProductSpecification_ConditionGroups, scope=complexType_ProductSpecification, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 916, 12)))

complexType_ProductSpecification._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'excluded-products'), complexType_ProductSpecification_ConditionGroups, scope=complexType_ProductSpecification, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 917, 12)))

def _BuildAutomaton_33 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_33
    del _BuildAutomaton_33
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 916, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 917, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'included-products')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 916, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'excluded-products')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 917, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ProductSpecification._Automaton = _BuildAutomaton_33()




complexType_ProductSpecification_ConditionGroups._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'condition-group'), complexType_ProductSpecification_ConditionGroup, scope=complexType_ProductSpecification_ConditionGroups, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 923, 12)))

def _BuildAutomaton_34 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_34
    del _BuildAutomaton_34
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 923, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ConditionGroups._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'condition-group')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 923, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ProductSpecification_ConditionGroups._Automaton = _BuildAutomaton_34()




complexType_ProductSpecification_ConditionGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'brand-condition'), complexType_ProductSpecification_ProductBrandFilter, scope=complexType_ProductSpecification_ConditionGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 929, 12)))

complexType_ProductSpecification_ConditionGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-id-condition'), complexType_ProductSpecification_ProductIDFilter, scope=complexType_ProductSpecification_ConditionGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 930, 12)))

complexType_ProductSpecification_ConditionGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-condition'), complexType_ProductSpecification_ProductCategoryFilter, scope=complexType_ProductSpecification_ConditionGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 931, 12)))

complexType_ProductSpecification_ConditionGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute-condition'), complexType_ProductSpecification_ProductAttributeFilter, scope=complexType_ProductSpecification_ConditionGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 932, 12)))

def _BuildAutomaton_35 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_35
    del _BuildAutomaton_35
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 928, 8))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ConditionGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'brand-condition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 929, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ConditionGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-id-condition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 930, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ConditionGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-condition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 931, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ConditionGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute-condition')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 932, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ProductSpecification_ConditionGroup._Automaton = _BuildAutomaton_35()




complexType_VariationAttribute_Values._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-value'), complexType_VariationAttribute_Value, scope=complexType_VariationAttribute_Values, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 168, 12)))

def _BuildAutomaton_36 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_36
    del _BuildAutomaton_36
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 168, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_VariationAttribute_Values._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 168, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_VariationAttribute_Values._Automaton = _BuildAutomaton_36()




complexType_Product_Images._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image-group'), complexType_Product_ImageGroup, scope=complexType_Product_Images, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 308, 12)))

def _BuildAutomaton_37 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_37
    del _BuildAutomaton_37
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 308, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Images._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image-group')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 308, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Images._Automaton = _BuildAutomaton_37()




complexType_Product_Variations_Variants._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variant'), complexType_Product_Variations_Variant, scope=complexType_Product_Variations_Variants, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 547, 12)))

def _BuildAutomaton_38 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_38
    del _BuildAutomaton_38
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 547, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Variants._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variant')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 547, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations_Variants._Automaton = _BuildAutomaton_38()




complexType_Product_Variations_VariationGroups._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-group'), complexType_Product_Variations_VariationGroup, scope=complexType_Product_Variations_VariationGroups, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 569, 12)))

def _BuildAutomaton_39 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_39
    del _BuildAutomaton_39
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 569, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_VariationGroups._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-group')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 569, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations_VariationGroups._Automaton = _BuildAutomaton_39()




complexType_PriceRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_PriceRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 724, 12)))

complexType_PriceRefinementBucket._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'threshold'), pyxb.binding.datatypes.int, scope=complexType_PriceRefinementBucket, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 725, 12)))

def _BuildAutomaton_40 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_40
    del _BuildAutomaton_40
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 724, 12))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_PriceRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 724, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_PriceRefinementBucket._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'threshold')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 725, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_PriceRefinementBucket._Automaton = _BuildAutomaton_40()




complexType_ProductSpecification_ProductBrandFilter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'brand'), simpleType_Generic_NonEmptyString_256, scope=complexType_ProductSpecification_ProductBrandFilter, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 938, 12)))

def _BuildAutomaton_41 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_41
    del _BuildAutomaton_41
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 938, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ProductBrandFilter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'brand')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 938, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ProductSpecification_ProductBrandFilter._Automaton = _BuildAutomaton_41()




complexType_ProductSpecification_ProductIDFilter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-id'), simpleType_Generic_NonEmptyString_100, scope=complexType_ProductSpecification_ProductIDFilter, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 945, 12)))

def _BuildAutomaton_42 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_42
    del _BuildAutomaton_42
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ProductIDFilter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 945, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_ProductSpecification_ProductIDFilter._Automaton = _BuildAutomaton_42()




CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'header'), complexType_Header, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 16, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-attribute-definitions'), complexType_AttributeDefinitions, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 17, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category'), complexType_Category, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 18, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product'), complexType_Product, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 19, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-option'), complexType_Option, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 20, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute'), complexType_VariationAttribute, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 21, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-assignment'), complexType_CategoryAssignment, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 22, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'recommendation'), complexType_Recommendation, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 23, 16)))

def _BuildAutomaton_43 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_43
    del _BuildAutomaton_43
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 16, 16))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 17, 16))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 18, 16))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 19, 16))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 20, 16))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 21, 16))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 22, 16))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 23, 16))
    counters.add(cc_7)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'header')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 16, 16))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-attribute-definitions')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 17, 16))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 18, 16))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 19, 16))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-option')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 20, 16))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 21, 16))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-assignment')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 22, 16))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'recommendation')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 23, 16))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton_43()




complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 85, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 86, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'type'), simpleType_AttributeType, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 87, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'localizable-flag'), pyxb.binding.datatypes.boolean, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 88, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'site-specific-flag'), pyxb.binding.datatypes.boolean, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 89, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mandatory-flag'), pyxb.binding.datatypes.boolean, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 90, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'visible-flag'), pyxb.binding.datatypes.boolean, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 91, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'externally-managed-flag'), pyxb.binding.datatypes.boolean, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 92, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'min-length'), pyxb.binding.datatypes.int, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 93, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'field-length'), pyxb.binding.datatypes.int, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 94, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'field-height'), pyxb.binding.datatypes.int, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 95, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'unit'), sharedType_LocalizedString, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 96, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'scale'), pyxb.binding.datatypes.int, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 97, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'min-value'), pyxb.binding.datatypes.double, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 98, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'max-value'), pyxb.binding.datatypes.double, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 99, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'regex'), simpleType_Generic_String_256, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 100, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'select-multiple-flag'), pyxb.binding.datatypes.boolean, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 101, 12)))

complexType_AttributeDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value-definitions'), complexType_ValueDefinitions, scope=complexType_AttributeDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 102, 12)))

def _BuildAutomaton_44 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_44
    del _BuildAutomaton_44
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 85, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 86, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 88, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 89, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 90, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 91, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 92, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 93, 12))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 94, 12))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 95, 12))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 96, 12))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 97, 12))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 98, 12))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 99, 12))
    counters.add(cc_13)
    cc_14 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 100, 12))
    counters.add(cc_14)
    cc_15 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 101, 12))
    counters.add(cc_15)
    cc_16 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 102, 12))
    counters.add(cc_16)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 85, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 86, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'type')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 87, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localizable-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 88, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'site-specific-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 89, 12))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mandatory-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 90, 12))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'visible-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 91, 12))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externally-managed-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 92, 12))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'min-length')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 93, 12))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'field-length')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 94, 12))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'field-height')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 95, 12))
    st_10 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'unit')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 96, 12))
    st_11 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'scale')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 97, 12))
    st_12 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'min-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 98, 12))
    st_13 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'max-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 99, 12))
    st_14 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_14, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'regex')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 100, 12))
    st_15 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_15, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'select-multiple-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 101, 12))
    st_16 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_16)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_16, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value-definitions')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 102, 12))
    st_17 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_17)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    transitions.append(fac.Transition(st_9, [
         ]))
    transitions.append(fac.Transition(st_10, [
         ]))
    transitions.append(fac.Transition(st_11, [
         ]))
    transitions.append(fac.Transition(st_12, [
         ]))
    transitions.append(fac.Transition(st_13, [
         ]))
    transitions.append(fac.Transition(st_14, [
         ]))
    transitions.append(fac.Transition(st_15, [
         ]))
    transitions.append(fac.Transition(st_16, [
         ]))
    transitions.append(fac.Transition(st_17, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_13, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_13, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_14, True) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_14, False) ]))
    st_15._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_15, True) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_15, False) ]))
    st_16._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_16, True) ]))
    st_17._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_AttributeDefinition._Automaton = _BuildAutomaton_44()




complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 110, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 111, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 112, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-from'), pyxb.binding.datatypes.dateTime, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 113, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-to'), pyxb.binding.datatypes.dateTime, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 114, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-groups-display-mode'), simpleType_VariationGroupsDisplayMode, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 115, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'parent'), simpleType_Generic_NonEmptyString_256, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 116, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'position'), pyxb.binding.datatypes.double, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 117, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'thumbnail'), simpleType_Generic_String_256, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 118, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), simpleType_Generic_String_256, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 119, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'template'), simpleType_Generic_String_256, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 120, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'search-placement'), pyxb.binding.datatypes.int, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 121, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'search-rank'), pyxb.binding.datatypes.int, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 122, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sitemap-included-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 123, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sitemap-changefrequency'), simpleType_SiteMapChangeFrequency, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 124, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sitemap-priority'), simpleType_SiteMapPriority, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 125, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-attributes'), complexType_PageAttributes, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 126, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 127, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-links'), complexType_CategoryLinks, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 128, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute-groups'), complexType_AttributeGroups, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 129, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'refinement-definitions'), complexType_RefinementDefinitions, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 130, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-detail-page-meta-tag-rules'), complexType_PageMetaTagRules, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 131, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-listing-page-meta-tag-rules'), complexType_PageMetaTagRules, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 132, 12)))

complexType_Category._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-specification-rule'), complexType_ProductSpecificationRule, scope=complexType_Category, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 133, 12)))

def _BuildAutomaton_45 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_45
    del _BuildAutomaton_45
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 110, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 111, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 112, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 113, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 114, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 115, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 116, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 117, 12))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 118, 12))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 119, 12))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 120, 12))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 121, 12))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 122, 12))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 123, 12))
    counters.add(cc_13)
    cc_14 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 124, 12))
    counters.add(cc_14)
    cc_15 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 125, 12))
    counters.add(cc_15)
    cc_16 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 126, 12))
    counters.add(cc_16)
    cc_17 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 127, 12))
    counters.add(cc_17)
    cc_18 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 128, 12))
    counters.add(cc_18)
    cc_19 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 129, 12))
    counters.add(cc_19)
    cc_20 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 130, 12))
    counters.add(cc_20)
    cc_21 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 131, 12))
    counters.add(cc_21)
    cc_22 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 132, 12))
    counters.add(cc_22)
    cc_23 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 133, 12))
    counters.add(cc_23)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 110, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 111, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 112, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-from')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 113, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-to')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 114, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-groups-display-mode')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 115, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'parent')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 116, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'position')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 117, 12))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'thumbnail')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 118, 12))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 119, 12))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'template')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 120, 12))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'search-placement')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 121, 12))
    st_11 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'search-rank')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 122, 12))
    st_12 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sitemap-included-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 123, 12))
    st_13 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_14, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sitemap-changefrequency')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 124, 12))
    st_14 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_15, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sitemap-priority')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 125, 12))
    st_15 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_16, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 126, 12))
    st_16 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_16)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_17, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 127, 12))
    st_17 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_17)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_18, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-links')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 128, 12))
    st_18 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_18)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_19, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute-groups')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 129, 12))
    st_19 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_19)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_20, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'refinement-definitions')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 130, 12))
    st_20 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_20)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_21, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-detail-page-meta-tag-rules')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 131, 12))
    st_21 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_21)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_22, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-listing-page-meta-tag-rules')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 132, 12))
    st_22 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_22)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_23, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Category._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-specification-rule')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 133, 12))
    st_23 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_23)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_13, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_13, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_14, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_14, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_15, True) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_15, False) ]))
    st_15._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_16, True) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_16, False) ]))
    st_16._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_17, True) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_17, False) ]))
    st_17._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_18, True) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_18, False) ]))
    st_18._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_19, True) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_19, False) ]))
    st_19._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_20, True) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_20, False) ]))
    st_20._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_21, True) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_21, False) ]))
    st_21._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_22, True) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_22, False) ]))
    st_22._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_23, True) ]))
    st_23._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Category._Automaton = _BuildAutomaton_45()




complexType_Option._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_Option, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 143, 12)))

complexType_Option._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_Option, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 144, 12)))

complexType_Option._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), simpleType_Generic_String_256, scope=complexType_Option, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 145, 12)))

complexType_Option._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sort-mode'), simpleType_Option_SortMode, scope=complexType_Option, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 146, 12)))

complexType_Option._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'option-values'), complexType_Option_Values, scope=complexType_Option, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 147, 12)))

def _BuildAutomaton_46 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_46
    del _BuildAutomaton_46
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 143, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 144, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 145, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 146, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 147, 12))
    counters.add(cc_4)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 143, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 144, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 145, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sort-mode')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 146, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'option-values')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 147, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Option._Automaton = _BuildAutomaton_46()




complexType_VariationAttribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_VariationAttribute, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 156, 12)))

complexType_VariationAttribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-values'), complexType_VariationAttribute_Values, scope=complexType_VariationAttribute, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 157, 12)))

def _BuildAutomaton_47 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_47
    del _BuildAutomaton_47
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 156, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=2, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 157, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_VariationAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 156, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_VariationAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-attribute-values')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 157, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_VariationAttribute._Automaton = _BuildAutomaton_47()




complexType_VariationAttribute_Value._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-value'), sharedType_LocalizedString, scope=complexType_VariationAttribute_Value, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 186, 12)))

complexType_VariationAttribute_Value._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_VariationAttribute_Value, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 187, 12)))

def _BuildAutomaton_48 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_48
    del _BuildAutomaton_48
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 186, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 187, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_VariationAttribute_Value._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 186, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_VariationAttribute_Value._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 187, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_VariationAttribute_Value._Automaton = _BuildAutomaton_48()




complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ean'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 197, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'upc'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 198, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'unit'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 199, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'min-order-quantity'), pyxb.binding.datatypes.decimal, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 200, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'step-quantity'), pyxb.binding.datatypes.decimal, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 201, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 202, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'short-description'), sharedType_LocalizedText, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 203, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'long-description'), sharedType_LocalizedText, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 204, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-receipt-name'), sharedType_LocalizedText, scope=complexType_Product, documentation='Deprecated. Please use element receipt-name which held by element store-attributes (complexType.Product.StoreAttributes).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 205, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-tax-class'), simpleType_Generic_String_60, scope=complexType_Product, documentation='Deprecated. Please use element tax-class which held by element store-attributes (complexType.Product.StoreAttributes).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 210, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-force-price-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product, documentation='Deprecated. Please use element force-price-flag which held by element store-attributes (complexType.Product.StoreAttributes).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 215, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-non-inventory-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product, documentation='Deprecated. Please use element non-inventory-flag which held by element store-attributes (complexType.Product.StoreAttributes).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 220, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-non-revenue-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product, documentation='Deprecated. Please use element non-revenue-flag which held by element store-attributes (complexType.Product.StoreAttributes).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 225, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-non-discountable-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product, documentation='Deprecated. Please use element non-discountable-flag which held by element store-attributes (complexType.Product.StoreAttributes).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 230, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-flag'), sharedType_SiteSpecificBoolean, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 235, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-from'), sharedType_SiteSpecificDateTime, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 236, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-to'), sharedType_SiteSpecificDateTime, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 237, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'available-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product, documentation='Deprecated. Use inventory feature to maintain product availability.', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 238, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'searchable-flag'), sharedType_SiteSpecificBoolean, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 243, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'searchable-if-unavailable-flag'), sharedType_SiteSpecificBoolean, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 244, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'images'), complexType_Product_Images, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 245, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), simpleType_Generic_String_256, scope=complexType_Product, documentation='Deprecated. Please use the element images (complexType.Product.Images).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 246, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'thumbnail'), simpleType_Generic_String_256, scope=complexType_Product, documentation='Deprecated. Please use the element images (complexType.Product.Images).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 251, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'template'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 256, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'tax-class-id'), simpleType_Generic_NonEmptyString_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 257, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'brand'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 258, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'manufacturer-name'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 259, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'manufacturer-sku'), simpleType_Generic_String_256, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 260, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'search-placement'), sharedType_SiteSpecificInt, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 261, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'search-rank'), sharedType_SiteSpecificInt, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 262, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sitemap-included-flag'), sharedType_SiteSpecificBoolean, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 263, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sitemap-changefrequency'), sharedType_SiteSpecificSiteMapChangeFrequency, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 264, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sitemap-priority'), sharedType_SiteSpecificSiteMapPriority, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 265, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'page-attributes'), complexType_PageAttributes, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 266, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_SiteSpecificCustomAttributes, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 267, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'bundled-products'), complexType_Product_BundledProducts, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 268, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'retail-set-products'), complexType_Product_RetailSetProducts, scope=complexType_Product, documentation='Deprecated please use the element product-set-products (complexType.Product.ProductSetProducts).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 269, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-set-products'), complexType_Product_ProductSetProducts, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 274, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'options'), complexType_Product_Options, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 275, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variations'), complexType_Product_Variations, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 276, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-links'), complexType_Product_ProductLinks, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 277, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-links'), complexType_Product_CategoryLinks, scope=complexType_Product, documentation='Deprecated please use the elements category-assignment (complexType.CategoryAssignment) and classification-category (complexType.Product.ClassificationCategory).', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 278, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'classification-category'), complexType_Product_ClassificationCategory, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 283, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'pinterest-enabled-flag'), sharedType_SiteSpecificBoolean, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 284, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'facebook-enabled-flag'), sharedType_SiteSpecificBoolean, nillable=pyxb.binding.datatypes.boolean(1), scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 285, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-detail-page-meta-tag-rules'), complexType_PageMetaTagRules, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 286, 12)))

complexType_Product._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-attributes'), complexType_Product_StoreAttributes, scope=complexType_Product, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 287, 12)))

def _BuildAutomaton_49 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_49
    del _BuildAutomaton_49
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 197, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 198, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 199, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 200, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 201, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 202, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 203, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 204, 12))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 205, 12))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 210, 12))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 215, 12))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 220, 12))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 225, 12))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 230, 12))
    counters.add(cc_13)
    cc_14 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 235, 12))
    counters.add(cc_14)
    cc_15 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 236, 12))
    counters.add(cc_15)
    cc_16 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 237, 12))
    counters.add(cc_16)
    cc_17 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 238, 12))
    counters.add(cc_17)
    cc_18 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 243, 12))
    counters.add(cc_18)
    cc_19 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 244, 12))
    counters.add(cc_19)
    cc_20 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 245, 12))
    counters.add(cc_20)
    cc_21 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 246, 12))
    counters.add(cc_21)
    cc_22 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 251, 12))
    counters.add(cc_22)
    cc_23 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 256, 12))
    counters.add(cc_23)
    cc_24 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 257, 12))
    counters.add(cc_24)
    cc_25 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 258, 12))
    counters.add(cc_25)
    cc_26 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 259, 12))
    counters.add(cc_26)
    cc_27 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 260, 12))
    counters.add(cc_27)
    cc_28 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 261, 12))
    counters.add(cc_28)
    cc_29 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 262, 12))
    counters.add(cc_29)
    cc_30 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 263, 12))
    counters.add(cc_30)
    cc_31 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 264, 12))
    counters.add(cc_31)
    cc_32 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 265, 12))
    counters.add(cc_32)
    cc_33 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 266, 12))
    counters.add(cc_33)
    cc_34 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 267, 12))
    counters.add(cc_34)
    cc_35 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 268, 12))
    counters.add(cc_35)
    cc_36 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 269, 12))
    counters.add(cc_36)
    cc_37 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 274, 12))
    counters.add(cc_37)
    cc_38 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 275, 12))
    counters.add(cc_38)
    cc_39 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 276, 12))
    counters.add(cc_39)
    cc_40 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 277, 12))
    counters.add(cc_40)
    cc_41 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 278, 12))
    counters.add(cc_41)
    cc_42 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 283, 12))
    counters.add(cc_42)
    cc_43 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 284, 12))
    counters.add(cc_43)
    cc_44 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 285, 12))
    counters.add(cc_44)
    cc_45 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 286, 12))
    counters.add(cc_45)
    cc_46 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 287, 12))
    counters.add(cc_46)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ean')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 197, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'upc')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 198, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'unit')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 199, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'min-order-quantity')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 200, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'step-quantity')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 201, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 202, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'short-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 203, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'long-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 204, 12))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-receipt-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 205, 12))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-tax-class')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 210, 12))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-force-price-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 215, 12))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-non-inventory-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 220, 12))
    st_11 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-non-revenue-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 225, 12))
    st_12 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-non-discountable-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 230, 12))
    st_13 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_14, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 235, 12))
    st_14 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_15, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-from')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 236, 12))
    st_15 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_16, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-to')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 237, 12))
    st_16 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_16)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_17, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'available-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 238, 12))
    st_17 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_17)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_18, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'searchable-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 243, 12))
    st_18 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_18)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_19, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'searchable-if-unavailable-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 244, 12))
    st_19 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_19)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_20, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'images')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 245, 12))
    st_20 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_20)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_21, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 246, 12))
    st_21 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_21)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_22, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'thumbnail')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 251, 12))
    st_22 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_22)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_23, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'template')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 256, 12))
    st_23 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_23)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_24, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'tax-class-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 257, 12))
    st_24 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_24)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_25, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'brand')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 258, 12))
    st_25 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_25)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_26, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'manufacturer-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 259, 12))
    st_26 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_26)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_27, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'manufacturer-sku')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 260, 12))
    st_27 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_27)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_28, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'search-placement')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 261, 12))
    st_28 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_28)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_29, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'search-rank')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 262, 12))
    st_29 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_29)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_30, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sitemap-included-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 263, 12))
    st_30 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_30)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_31, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sitemap-changefrequency')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 264, 12))
    st_31 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_31)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_32, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sitemap-priority')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 265, 12))
    st_32 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_32)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_33, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'page-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 266, 12))
    st_33 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_33)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_34, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 267, 12))
    st_34 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_34)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_35, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'bundled-products')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 268, 12))
    st_35 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_35)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_36, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'retail-set-products')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 269, 12))
    st_36 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_36)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_37, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-set-products')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 274, 12))
    st_37 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_37)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_38, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'options')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 275, 12))
    st_38 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_38)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_39, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variations')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 276, 12))
    st_39 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_39)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_40, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-links')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 277, 12))
    st_40 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_40)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_41, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-links')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 278, 12))
    st_41 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_41)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_42, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'classification-category')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 283, 12))
    st_42 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_42)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_43, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'pinterest-enabled-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 284, 12))
    st_43 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_43)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_44, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'facebook-enabled-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 285, 12))
    st_44 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_44)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_45, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-detail-page-meta-tag-rules')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 286, 12))
    st_45 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_45)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_46, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 287, 12))
    st_46 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_46)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_13, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_13, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_14, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_14, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_15, True) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_15, False) ]))
    st_15._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_16, True) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_16, False) ]))
    st_16._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_17, True) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_17, False) ]))
    st_17._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_18, True) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_18, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_18, False) ]))
    st_18._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_19, True) ]))
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_19, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_19, False) ]))
    st_19._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_20, [
        fac.UpdateInstruction(cc_20, True) ]))
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_20, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_20, False) ]))
    st_20._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_21, [
        fac.UpdateInstruction(cc_21, True) ]))
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_21, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_21, False) ]))
    st_21._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_22, [
        fac.UpdateInstruction(cc_22, True) ]))
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_22, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_22, False) ]))
    st_22._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_23, [
        fac.UpdateInstruction(cc_23, True) ]))
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_23, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_23, False) ]))
    st_23._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_24, [
        fac.UpdateInstruction(cc_24, True) ]))
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_24, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_24, False) ]))
    st_24._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_25, [
        fac.UpdateInstruction(cc_25, True) ]))
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_25, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_25, False) ]))
    st_25._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_26, [
        fac.UpdateInstruction(cc_26, True) ]))
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_26, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_26, False) ]))
    st_26._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_27, [
        fac.UpdateInstruction(cc_27, True) ]))
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_27, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_27, False) ]))
    st_27._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_28, [
        fac.UpdateInstruction(cc_28, True) ]))
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_28, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_28, False) ]))
    st_28._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_29, [
        fac.UpdateInstruction(cc_29, True) ]))
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_29, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_29, False) ]))
    st_29._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_30, [
        fac.UpdateInstruction(cc_30, True) ]))
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_30, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_30, False) ]))
    st_30._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_31, [
        fac.UpdateInstruction(cc_31, True) ]))
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_31, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_31, False) ]))
    st_31._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_32, [
        fac.UpdateInstruction(cc_32, True) ]))
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_32, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_32, False) ]))
    st_32._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_33, [
        fac.UpdateInstruction(cc_33, True) ]))
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_33, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_33, False) ]))
    st_33._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_34, [
        fac.UpdateInstruction(cc_34, True) ]))
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_34, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_34, False) ]))
    st_34._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_35, [
        fac.UpdateInstruction(cc_35, True) ]))
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_35, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_35, False) ]))
    st_35._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_36, [
        fac.UpdateInstruction(cc_36, True) ]))
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_36, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_36, False) ]))
    st_36._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_37, [
        fac.UpdateInstruction(cc_37, True) ]))
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_37, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_37, False) ]))
    st_37._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_38, [
        fac.UpdateInstruction(cc_38, True) ]))
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_38, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_38, False) ]))
    st_38._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_39, [
        fac.UpdateInstruction(cc_39, True) ]))
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_39, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_39, False) ]))
    st_39._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_40, [
        fac.UpdateInstruction(cc_40, True) ]))
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_40, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_40, False) ]))
    st_40._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_41, [
        fac.UpdateInstruction(cc_41, True) ]))
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_41, False) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_41, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_41, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_41, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_41, False) ]))
    st_41._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_42, [
        fac.UpdateInstruction(cc_42, True) ]))
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_42, False) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_42, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_42, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_42, False) ]))
    st_42._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_43, [
        fac.UpdateInstruction(cc_43, True) ]))
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_43, False) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_43, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_43, False) ]))
    st_43._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_44, [
        fac.UpdateInstruction(cc_44, True) ]))
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_44, False) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_44, False) ]))
    st_44._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_45, [
        fac.UpdateInstruction(cc_45, True) ]))
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_45, False) ]))
    st_45._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_46, [
        fac.UpdateInstruction(cc_46, True) ]))
    st_46._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product._Automaton = _BuildAutomaton_49()




complexType_Product_ImageGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation'), complexType_Product_ImageGroup_VariationAttributeValue, scope=complexType_Product_ImageGroup, documentation='\n                        This element, only relevant for variation masters, represents an ordered list of variation\n                        attribute values.  It is used to identify a set of variants to which the images in this group apply.\n\n                        - The image groups related to the product master will have zero of these elements.  These\n                          represent the "fallback" images.\n                        - If an image group applies for all the red variants, for example, it will have one of these\n                          elements, namely "color=red".\n                        - If an image group applies for all the red, leather variants, for example, it will have two\n                          of these elements, namely "color=red, fabric=leather".\n\n                        The list is ordered so that, within a master product, the same variation attribute must always\n                        appear at the same index of the list.  In our example, "color" would always be specified first\n                        for each image group, and then "fabric" would always appear second.  This ensures there are\n                        clear fallback rules to identify which images relate to each variant. When retrieving the images\n                        for a variant at runtime, the system chooses the most specific matching image groups possible.\n                    ', location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 325, 12)))

complexType_Product_ImageGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), complexType_Product_Image, scope=complexType_Product_ImageGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 346, 12)))

def _BuildAutomaton_50 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_50
    del _BuildAutomaton_50
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 325, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 346, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_ImageGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 325, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_ImageGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 346, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_ImageGroup._Automaton = _BuildAutomaton_50()




complexType_Product_Image._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'alt'), sharedType_LocalizedText, scope=complexType_Product_Image, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 363, 12)))

complexType_Product_Image._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'title'), sharedType_LocalizedString, scope=complexType_Product_Image, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 364, 12)))

def _BuildAutomaton_51 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_51
    del _BuildAutomaton_51
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 363, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 364, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Image._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'alt')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 363, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Image._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'title')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 364, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Image._Automaton = _BuildAutomaton_51()




complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 372, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'short-description'), sharedType_LocalizedText, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 373, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'long-description'), sharedType_LocalizedText, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 374, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'callout-message'), sharedType_LocalizedText, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 375, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), simpleType_Generic_String_256, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 376, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'position'), simpleType_CategoryAssignment_Position, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 377, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'primary-flag'), pyxb.binding.datatypes.boolean, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 378, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 379, 12)))

complexType_CategoryAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-assignments'), complexType_CategoryAssignment_VariationAssignments, scope=complexType_CategoryAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 380, 12)))

def _BuildAutomaton_52 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_52
    del _BuildAutomaton_52
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 372, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 373, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 374, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 375, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 376, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 377, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 378, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 379, 12))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 380, 12))
    counters.add(cc_8)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 372, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'short-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 373, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'long-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 374, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'callout-message')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 375, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 376, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'position')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 377, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'primary-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 378, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 379, 12))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-assignments')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 380, 12))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    st_8._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_CategoryAssignment._Automaton = _BuildAutomaton_52()




complexType_Recommendation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 390, 12)))

complexType_Recommendation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'short-description'), sharedType_LocalizedText, scope=complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 391, 12)))

complexType_Recommendation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'long-description'), sharedType_LocalizedText, scope=complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 392, 12)))

complexType_Recommendation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'callout-message'), sharedType_LocalizedText, scope=complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 393, 12)))

complexType_Recommendation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), simpleType_Generic_String_256, scope=complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 394, 12)))

complexType_Recommendation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_Recommendation, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 395, 12)))

def _BuildAutomaton_53 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_53
    del _BuildAutomaton_53
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 390, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 391, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 392, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 393, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 394, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 395, 12))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Recommendation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 390, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Recommendation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'short-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 391, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Recommendation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'long-description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 392, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Recommendation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'callout-message')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 393, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Recommendation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 394, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Recommendation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 395, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Recommendation._Automaton = _BuildAutomaton_53()




complexType_Product_BundledProduct._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'quantity'), pyxb.binding.datatypes.decimal, scope=complexType_Product_BundledProduct, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 427, 12)))

def _BuildAutomaton_54 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_54
    del _BuildAutomaton_54
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_Product_BundledProduct._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'quantity')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 427, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_Product_BundledProduct._Automaton = _BuildAutomaton_54()




complexType_Product_RetailSetProduct._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'quantity'), pyxb.binding.datatypes.decimal, scope=complexType_Product_RetailSetProduct, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 447, 12)))

def _BuildAutomaton_55 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_55
    del _BuildAutomaton_55
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_Product_RetailSetProduct._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'quantity')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 447, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_Product_RetailSetProduct._Automaton = _BuildAutomaton_55()




complexType_Product_CategoryLink._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'position'), pyxb.binding.datatypes.double, scope=complexType_Product_CategoryLink, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 479, 12)))

complexType_Product_CategoryLink._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'primary-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Product_CategoryLink, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 480, 12)))

def _BuildAutomaton_57 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_57
    del _BuildAutomaton_57
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 479, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_CategoryLink._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'position')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 479, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_58 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_58
    del _BuildAutomaton_58
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 480, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_CategoryLink._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'primary-flag')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 480, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=st_0)

def _BuildAutomaton_56 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_56
    del _BuildAutomaton_56
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 479, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 480, 12))
    counters.add(cc_1)
    states = []
    sub_automata = []
    sub_automata.append(_BuildAutomaton_57())
    sub_automata.append(_BuildAutomaton_58())
    final_update = set()
    symbol = pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 478, 8)
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=True)
    st_0._set_subAutomata(*sub_automata)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_CategoryLink._Automaton = _BuildAutomaton_56()




complexType_Product_VariationAttribute_Reference._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_Product_VariationAttribute_Reference, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 539, 12)))

def _BuildAutomaton_59 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_59
    del _BuildAutomaton_59
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 539, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_VariationAttribute_Reference._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 539, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_VariationAttribute_Reference._Automaton = _BuildAutomaton_59()




complexType_CategoryAssignment_VariationAssignment._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment-values'), complexType_CategoryAssignment_VariationAssignment_Values, scope=complexType_CategoryAssignment_VariationAssignment, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 600, 12)))

def _BuildAutomaton_60 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_60
    del _BuildAutomaton_60
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 600, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_CategoryAssignment_VariationAssignment._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'variation-assignment-values')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 600, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_CategoryAssignment_VariationAssignment._Automaton = _BuildAutomaton_60()




complexType_AttributeGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_AttributeGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 622, 12)))

complexType_AttributeGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), sharedType_LocalizedString, scope=complexType_AttributeGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 623, 12)))

complexType_AttributeGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute'), complexType_AttributeReference, scope=complexType_AttributeGroup, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 624, 12)))

def _BuildAutomaton_61 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_61
    del _BuildAutomaton_61
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 622, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 623, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 624, 12))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 622, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 623, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_AttributeGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 624, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_AttributeGroup._Automaton = _BuildAutomaton_61()




complexType_Product_Variations_Attributes_Attribute_Value._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-value'), sharedType_LocalizedString, scope=complexType_Product_Variations_Attributes_Attribute_Value, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 661, 12)))

def _BuildAutomaton_62 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_62
    del _BuildAutomaton_62
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 661, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Attributes_Attribute_Value._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 661, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations_Attributes_Attribute_Value._Automaton = _BuildAutomaton_62()




complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), sharedType_LocalizedString, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 677, 12)))

complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value-set'), simpleType_Refinement_ValueSet, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 678, 12)))

complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sort-mode'), simpleType_Sorting_Mode, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 679, 12)))

complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sort-direction'), simpleType_Sorting_Direction, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 680, 12)))

complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'unbucketed-values-mode'), simpleType_UnbucketedValues_Mode, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 681, 12)))

complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'cutoff-threshold'), pyxb.binding.datatypes.int, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 682, 12)))

complexType_RefinementDefinition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'bucket-definitions'), complexType_RefinementBuckets, scope=complexType_RefinementDefinition, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 683, 12)))

def _BuildAutomaton_63 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_63
    del _BuildAutomaton_63
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 677, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 678, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 679, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 680, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 681, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 682, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 683, 12))
    counters.add(cc_6)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 677, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value-set')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 678, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sort-mode')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 679, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sort-direction')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 680, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'unbucketed-values-mode')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 681, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'cutoff-threshold')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 682, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_RefinementDefinition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'bucket-definitions')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 683, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_RefinementDefinition._Automaton = _BuildAutomaton_63()




complexType_Option_Value._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-value'), sharedType_LocalizedString, scope=complexType_Option_Value, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 769, 12)))

complexType_Option_Value._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'product-id-modifier'), simpleType_Generic_String_256, scope=complexType_Option_Value, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 770, 12)))

complexType_Option_Value._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'option-value-prices'), complexType_Option_Value_Prices, scope=complexType_Option_Value, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 771, 12)))

def _BuildAutomaton_64 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_64
    del _BuildAutomaton_64
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 769, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 770, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 771, 12))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option_Value._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 769, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option_Value._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'product-id-modifier')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 770, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Option_Value._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'option-value-prices')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 771, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Option_Value._Automaton = _BuildAutomaton_64()




sharedType_CustomAttribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), simpleType_Generic_String, scope=sharedType_CustomAttribute, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 833, 12)))

def _BuildAutomaton_65 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_65
    del _BuildAutomaton_65
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 833, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 833, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttribute._Automaton = _BuildAutomaton_65()




complexType_ProductSpecification_ProductCategoryFilter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'category-id'), simpleType_Generic_String_256, scope=complexType_ProductSpecification_ProductCategoryFilter, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 952, 12)))

def _BuildAutomaton_66 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_66
    del _BuildAutomaton_66
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ProductCategoryFilter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'category-id')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 952, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_ProductSpecification_ProductCategoryFilter._Automaton = _BuildAutomaton_66()




complexType_ProductSpecification_ProductAttributeFilter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute-value'), simpleType_Generic_NonEmptyString_256, scope=complexType_ProductSpecification_ProductAttributeFilter, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 960, 12)))

def _BuildAutomaton_67 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_67
    del _BuildAutomaton_67
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 960, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_ProductSpecification_ProductAttributeFilter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute-value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 960, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_ProductSpecification_ProductAttributeFilter._Automaton = _BuildAutomaton_67()




complexType_Product_Variations_Attributes_Attribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'values'), complexType_Product_Variations_Attributes_Attribute_Values, scope=complexType_Product_Variations_Attributes_Attribute, location=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 641, 20)))

def _BuildAutomaton_68 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_68
    del _BuildAutomaton_68
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 641, 20))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Product_Variations_Attributes_Attribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'values')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 641, 20))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Product_Variations_Attributes_Attribute._Automaton = _BuildAutomaton_68()




def _BuildAutomaton_69 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_69
    del _BuildAutomaton_69
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 833, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_SiteSpecificCustomAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/ira/workspace/pyxb-test/catalog.xsd', 833, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_SiteSpecificCustomAttribute._Automaton = _BuildAutomaton_69()

