# ./binding.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:3b56f886b619e2b084e2aee20540dd04cf3e41d4
# Generated 2018-04-13 14:15:36.962301 by PyXB version 1.2.6 using Python 3.6.5.final.0
# Namespace http://www.demandware.com/xml/impex/inventory/2007-05-31

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:cf604ac0-3f5f-11e8-9a33-8c85900cdc76')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes
import pyxb.binding.xml_

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://www.demandware.com/xml/impex/inventory/2007-05-31', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.InventoryAmount
class simpleType_InventoryAmount (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.InventoryAmount')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 90, 4)
    _Documentation = None
simpleType_InventoryAmount._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=simpleType_InventoryAmount, value=pyxb.binding.datatypes.decimal('0.0'))
simpleType_InventoryAmount._InitializeFacetMap(simpleType_InventoryAmount._CF_minInclusive)
Namespace.addCategoryObject('typeBinding', 'simpleType.InventoryAmount', simpleType_InventoryAmount)
_module_typeBindings.simpleType_InventoryAmount = simpleType_InventoryAmount

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.UnboundedInventoryAmount
class simpleType_UnboundedInventoryAmount (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.UnboundedInventoryAmount')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 96, 4)
    _Documentation = None
simpleType_UnboundedInventoryAmount._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'simpleType.UnboundedInventoryAmount', simpleType_UnboundedInventoryAmount)
_module_typeBindings.simpleType_UnboundedInventoryAmount = simpleType_UnboundedInventoryAmount

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.ImportMode
class simpleType_ImportMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.ImportMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 101, 4)
    _Documentation = None
simpleType_ImportMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_ImportMode, enum_prefix=None)
simpleType_ImportMode.delete = simpleType_ImportMode._CF_enumeration.addEnumeration(unicode_value='delete', tag='delete')
simpleType_ImportMode._InitializeFacetMap(simpleType_ImportMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.ImportMode', simpleType_ImportMode)
_module_typeBindings.simpleType_ImportMode = simpleType_ImportMode

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.InventoryRecord.PreorderBackorderHandling
class simpleType_InventoryRecord_PreorderBackorderHandling (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.InventoryRecord.PreorderBackorderHandling')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 107, 4)
    _Documentation = None
simpleType_InventoryRecord_PreorderBackorderHandling._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_InventoryRecord_PreorderBackorderHandling, enum_prefix=None)
simpleType_InventoryRecord_PreorderBackorderHandling.none = simpleType_InventoryRecord_PreorderBackorderHandling._CF_enumeration.addEnumeration(unicode_value='none', tag='none')
simpleType_InventoryRecord_PreorderBackorderHandling.preorder = simpleType_InventoryRecord_PreorderBackorderHandling._CF_enumeration.addEnumeration(unicode_value='preorder', tag='preorder')
simpleType_InventoryRecord_PreorderBackorderHandling.backorder = simpleType_InventoryRecord_PreorderBackorderHandling._CF_enumeration.addEnumeration(unicode_value='backorder', tag='backorder')
simpleType_InventoryRecord_PreorderBackorderHandling._InitializeFacetMap(simpleType_InventoryRecord_PreorderBackorderHandling._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.InventoryRecord.PreorderBackorderHandling', simpleType_InventoryRecord_PreorderBackorderHandling)
_module_typeBindings.simpleType_InventoryRecord_PreorderBackorderHandling = simpleType_InventoryRecord_PreorderBackorderHandling

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.Generic.String
class simpleType_Generic_String (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 115, 4)
    _Documentation = None
simpleType_Generic_String._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String', simpleType_Generic_String)
_module_typeBindings.simpleType_Generic_String = simpleType_Generic_String

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.Generic.String.256
class simpleType_Generic_String_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 119, 4)
    _Documentation = None
simpleType_Generic_String_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_String_256._InitializeFacetMap(simpleType_Generic_String_256._CF_minLength,
   simpleType_Generic_String_256._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.256', simpleType_Generic_String_256)
_module_typeBindings.simpleType_Generic_String_256 = simpleType_Generic_String_256

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.Generic.String.4000
class simpleType_Generic_String_4000 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.4000')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 126, 4)
    _Documentation = None
simpleType_Generic_String_4000._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_4000._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(4000))
simpleType_Generic_String_4000._InitializeFacetMap(simpleType_Generic_String_4000._CF_minLength,
   simpleType_Generic_String_4000._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.4000', simpleType_Generic_String_4000)
_module_typeBindings.simpleType_Generic_String_4000 = simpleType_Generic_String_4000

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.Generic.NonEmptyString.100
class simpleType_Generic_NonEmptyString_100 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.100')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 134, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_100._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_100._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(100))
simpleType_Generic_NonEmptyString_100._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_100._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_100._InitializeFacetMap(simpleType_Generic_NonEmptyString_100._CF_minLength,
   simpleType_Generic_NonEmptyString_100._CF_maxLength,
   simpleType_Generic_NonEmptyString_100._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.100', simpleType_Generic_NonEmptyString_100)
_module_typeBindings.simpleType_Generic_NonEmptyString_100 = simpleType_Generic_NonEmptyString_100

# Atomic simple type: {http://www.demandware.com/xml/impex/inventory/2007-05-31}simpleType.Generic.NonEmptyString.256
class simpleType_Generic_NonEmptyString_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 142, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_NonEmptyString_256._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_256._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_256._InitializeFacetMap(simpleType_Generic_NonEmptyString_256._CF_minLength,
   simpleType_Generic_NonEmptyString_256._CF_maxLength,
   simpleType_Generic_NonEmptyString_256._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.256', simpleType_Generic_NonEmptyString_256)
_module_typeBindings.simpleType_Generic_NonEmptyString_256 = simpleType_Generic_NonEmptyString_256

# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 12, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}inventory-list uses Python identifier inventory_list
    __inventory_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'inventory-list'), 'inventory_list', '__httpwww_demandware_comxmlimpexinventory2007_05_31_CTD_ANON_httpwww_demandware_comxmlimpexinventory2007_05_31inventory_list', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 14, 16), )

    
    inventory_list = property(__inventory_list.value, __inventory_list.set, None, None)

    _ElementMap.update({
        __inventory_list.name() : __inventory_list
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON = CTD_ANON


# Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.InventoryList with content type ELEMENT_ONLY
class complexType_InventoryList (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.InventoryList with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.InventoryList')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 23, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}header uses Python identifier header
    __header = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'header'), 'header', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryList_httpwww_demandware_comxmlimpexinventory2007_05_31header', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 25, 12), )

    
    header = property(__header.value, __header.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}records uses Python identifier records
    __records = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'records'), 'records', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryList_httpwww_demandware_comxmlimpexinventory2007_05_31records', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 26, 12), )

    
    records = property(__records.value, __records.set, None, None)

    _ElementMap.update({
        __header.name() : __header,
        __records.name() : __records
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_InventoryList = complexType_InventoryList
Namespace.addCategoryObject('typeBinding', 'complexType.InventoryList', complexType_InventoryList)


# Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.InventoryRecords with content type ELEMENT_ONLY
class complexType_InventoryRecords (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.InventoryRecords with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.InventoryRecords')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 42, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}record uses Python identifier record
    __record = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'record'), 'record', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecords_httpwww_demandware_comxmlimpexinventory2007_05_31record', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 44, 12), )

    
    record = property(__record.value, __record.set, None, None)

    _ElementMap.update({
        __record.name() : __record
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_InventoryRecords = complexType_InventoryRecords
Namespace.addCategoryObject('typeBinding', 'complexType.InventoryRecords', complexType_InventoryRecords)


# Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}sharedType.CustomAttributes with content type ELEMENT_ONLY
class sharedType_CustomAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}sharedType.CustomAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 74, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}custom-attribute uses Python identifier custom_attribute
    __custom_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), 'custom_attribute', '__httpwww_demandware_comxmlimpexinventory2007_05_31_sharedType_CustomAttributes_httpwww_demandware_comxmlimpexinventory2007_05_31custom_attribute', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 76, 12), )

    
    custom_attribute = property(__custom_attribute.value, __custom_attribute.set, None, None)

    _ElementMap.update({
        __custom_attribute.name() : __custom_attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.sharedType_CustomAttributes = sharedType_CustomAttributes
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttributes', sharedType_CustomAttributes)


# Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.Header with content type ELEMENT_ONLY
class complexType_Header (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.Header with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Header')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 30, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}default-instock uses Python identifier default_instock
    __default_instock = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'default-instock'), 'default_instock', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_httpwww_demandware_comxmlimpexinventory2007_05_31default_instock', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 32, 12), )

    
    default_instock = property(__default_instock.value, __default_instock.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_httpwww_demandware_comxmlimpexinventory2007_05_31description', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 33, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}use-bundle-inventory-only uses Python identifier use_bundle_inventory_only
    __use_bundle_inventory_only = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'use-bundle-inventory-only'), 'use_bundle_inventory_only', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_httpwww_demandware_comxmlimpexinventory2007_05_31use_bundle_inventory_only', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 34, 12), )

    
    use_bundle_inventory_only = property(__use_bundle_inventory_only.value, __use_bundle_inventory_only.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}on-order uses Python identifier on_order
    __on_order = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'on-order'), 'on_order', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_httpwww_demandware_comxmlimpexinventory2007_05_31on_order', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 35, 12), )

    
    on_order = property(__on_order.value, __on_order.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_httpwww_demandware_comxmlimpexinventory2007_05_31custom_attributes', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 36, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Attribute list-id uses Python identifier list_id
    __list_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'list-id'), 'list_id', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_list_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __list_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 38, 8)
    __list_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 38, 8)
    
    list_id = property(__list_id.value, __list_id.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_Header_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 39, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 39, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __default_instock.name() : __default_instock,
        __description.name() : __description,
        __use_bundle_inventory_only.name() : __use_bundle_inventory_only,
        __on_order.name() : __on_order,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        __list_id.name() : __list_id,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_Header = complexType_Header
Namespace.addCategoryObject('typeBinding', 'complexType.Header', complexType_Header)


# Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.InventoryRecord with content type ELEMENT_ONLY
class complexType_InventoryRecord (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}complexType.InventoryRecord with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.InventoryRecord')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 48, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}allocation uses Python identifier allocation
    __allocation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'allocation'), 'allocation', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31allocation', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 50, 12), )

    
    allocation = property(__allocation.value, __allocation.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}allocation-timestamp uses Python identifier allocation_timestamp
    __allocation_timestamp = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'allocation-timestamp'), 'allocation_timestamp', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31allocation_timestamp', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 51, 12), )

    
    allocation_timestamp = property(__allocation_timestamp.value, __allocation_timestamp.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}perpetual uses Python identifier perpetual
    __perpetual = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'perpetual'), 'perpetual', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31perpetual', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 52, 12), )

    
    perpetual = property(__perpetual.value, __perpetual.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}preorder-backorder-handling uses Python identifier preorder_backorder_handling
    __preorder_backorder_handling = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'preorder-backorder-handling'), 'preorder_backorder_handling', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31preorder_backorder_handling', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 53, 12), )

    
    preorder_backorder_handling = property(__preorder_backorder_handling.value, __preorder_backorder_handling.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}preorder-backorder-allocation uses Python identifier preorder_backorder_allocation
    __preorder_backorder_allocation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'preorder-backorder-allocation'), 'preorder_backorder_allocation', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31preorder_backorder_allocation', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 54, 12), )

    
    preorder_backorder_allocation = property(__preorder_backorder_allocation.value, __preorder_backorder_allocation.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}in-stock-date uses Python identifier in_stock_date
    __in_stock_date = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'in-stock-date'), 'in_stock_date', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31in_stock_date', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 55, 12), )

    
    in_stock_date = property(__in_stock_date.value, __in_stock_date.set, None, '\n                        Deprecated in favor of new "in-stock-datetime" element.\n                    ')

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}in-stock-datetime uses Python identifier in_stock_datetime
    __in_stock_datetime = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'in-stock-datetime'), 'in_stock_datetime', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31in_stock_datetime', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 62, 12), )

    
    in_stock_datetime = property(__in_stock_datetime.value, __in_stock_datetime.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}ats uses Python identifier ats
    __ats = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ats'), 'ats', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31ats', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 63, 12), )

    
    ats = property(__ats.value, __ats.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}on-order uses Python identifier on_order
    __on_order = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'on-order'), 'on_order', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31on_order', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 64, 12), )

    
    on_order = property(__on_order.value, __on_order.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}turnover uses Python identifier turnover
    __turnover = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'turnover'), 'turnover', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31turnover', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 65, 12), )

    
    turnover = property(__turnover.value, __turnover.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_httpwww_demandware_comxmlimpexinventory2007_05_31custom_attributes', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 66, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 68, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 68, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexinventory2007_05_31_complexType_InventoryRecord_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 69, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 69, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __allocation.name() : __allocation,
        __allocation_timestamp.name() : __allocation_timestamp,
        __perpetual.name() : __perpetual,
        __preorder_backorder_handling.name() : __preorder_backorder_handling,
        __preorder_backorder_allocation.name() : __preorder_backorder_allocation,
        __in_stock_date.name() : __in_stock_date,
        __in_stock_datetime.name() : __in_stock_datetime,
        __ats.name() : __ats,
        __on_order.name() : __on_order,
        __turnover.name() : __turnover,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        __product_id.name() : __product_id,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_InventoryRecord = complexType_InventoryRecord
Namespace.addCategoryObject('typeBinding', 'complexType.InventoryRecord', complexType_InventoryRecord)


# Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}sharedType.CustomAttribute with content type MIXED
class sharedType_CustomAttribute (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/inventory/2007-05-31}sharedType.CustomAttribute with content type MIXED"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_MIXED
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 80, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/inventory/2007-05-31}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_demandware_comxmlimpexinventory2007_05_31_sharedType_CustomAttribute_httpwww_demandware_comxmlimpexinventory2007_05_31value', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 82, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexinventory2007_05_31_sharedType_CustomAttribute_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 85, 8)
    
    lang = property(__lang.value, __lang.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexinventory2007_05_31_sharedType_CustomAttribute_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 84, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 84, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        __lang.name() : __lang,
        __attribute_id.name() : __attribute_id
    })
_module_typeBindings.sharedType_CustomAttribute = sharedType_CustomAttribute
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttribute', sharedType_CustomAttribute)


inventory = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'inventory'), CTD_ANON, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 11, 4))
Namespace.addCategoryObject('elementBinding', inventory.name().localName(), inventory)

header = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'header'), complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 20, 4))
Namespace.addCategoryObject('elementBinding', header.name().localName(), header)

record = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'record'), complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 21, 4))
Namespace.addCategoryObject('elementBinding', record.name().localName(), record)



CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'inventory-list'), complexType_InventoryList, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 14, 16)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 14, 16))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'inventory-list')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 14, 16))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton()




complexType_InventoryList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'header'), complexType_Header, scope=complexType_InventoryList, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 25, 12)))

complexType_InventoryList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'records'), complexType_InventoryRecords, scope=complexType_InventoryList, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 26, 12)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 26, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'header')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 25, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'records')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 26, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_InventoryList._Automaton = _BuildAutomaton_()




complexType_InventoryRecords._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'record'), complexType_InventoryRecord, scope=complexType_InventoryRecords, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 44, 12)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 44, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecords._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'record')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 44, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_InventoryRecords._Automaton = _BuildAutomaton_2()




sharedType_CustomAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), sharedType_CustomAttribute, scope=sharedType_CustomAttributes, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 76, 12)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 76, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 76, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttributes._Automaton = _BuildAutomaton_3()




complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'default-instock'), pyxb.binding.datatypes.boolean, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 32, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), simpleType_Generic_String_4000, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 33, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'use-bundle-inventory-only'), pyxb.binding.datatypes.boolean, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 34, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'on-order'), pyxb.binding.datatypes.boolean, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 35, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 36, 12)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 33, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 34, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 35, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 36, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'default-instock')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 32, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 33, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'use-bundle-inventory-only')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 34, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'on-order')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 35, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 36, 12))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_Header._Automaton = _BuildAutomaton_4()




complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'allocation'), simpleType_InventoryAmount, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 50, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'allocation-timestamp'), pyxb.binding.datatypes.dateTime, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 51, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'perpetual'), pyxb.binding.datatypes.boolean, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 52, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'preorder-backorder-handling'), simpleType_InventoryRecord_PreorderBackorderHandling, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 53, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'preorder-backorder-allocation'), simpleType_InventoryAmount, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 54, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'in-stock-date'), pyxb.binding.datatypes.date, scope=complexType_InventoryRecord, documentation='\n                        Deprecated in favor of new "in-stock-datetime" element.\n                    ', location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 55, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'in-stock-datetime'), pyxb.binding.datatypes.dateTime, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 62, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ats'), simpleType_InventoryAmount, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 63, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'on-order'), pyxb.binding.datatypes.decimal, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 64, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'turnover'), simpleType_UnboundedInventoryAmount, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 65, 12)))

complexType_InventoryRecord._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_InventoryRecord, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 66, 12)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 50, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 51, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 52, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 53, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 54, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 55, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 62, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 63, 12))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 64, 12))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 65, 12))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 66, 12))
    counters.add(cc_10)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'allocation')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 50, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'allocation-timestamp')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 51, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'perpetual')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 52, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'preorder-backorder-handling')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 53, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'preorder-backorder-allocation')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 54, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'in-stock-date')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 55, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'in-stock-datetime')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 62, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ats')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 63, 12))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'on-order')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 64, 12))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'turnover')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 65, 12))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(complexType_InventoryRecord._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 66, 12))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    st_10._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_InventoryRecord._Automaton = _BuildAutomaton_5()




sharedType_CustomAttribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), simpleType_Generic_String, scope=sharedType_CustomAttribute, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 82, 12)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 82, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/inventory.xsd', 82, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttribute._Automaton = _BuildAutomaton_6()

