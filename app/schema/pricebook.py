# ./binding.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:30645421ed0edd7e9294618554272f884cb9f6b5
# Generated 2018-04-17 12:53:47.225048 by PyXB version 1.2.6 using Python 3.6.5.final.0
# Namespace http://www.demandware.com/xml/impex/pricebook/2006-10-31

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:0a98560c-4279-11e8-9b4c-8c85900cdc76')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes
import pyxb.binding.xml_

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://www.demandware.com/xml/impex/pricebook/2006-10-31', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.ImportMode
class simpleType_ImportMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.ImportMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 89, 4)
    _Documentation = None
simpleType_ImportMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_ImportMode, enum_prefix=None)
simpleType_ImportMode.delete = simpleType_ImportMode._CF_enumeration.addEnumeration(unicode_value='delete', tag='delete')
simpleType_ImportMode._InitializeFacetMap(simpleType_ImportMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.ImportMode', simpleType_ImportMode)
_module_typeBindings.simpleType_ImportMode = simpleType_ImportMode

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.PriceTableImportMode
class simpleType_PriceTableImportMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.PriceTableImportMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 95, 4)
    _Documentation = None
simpleType_PriceTableImportMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_PriceTableImportMode, enum_prefix=None)
simpleType_PriceTableImportMode.delete = simpleType_PriceTableImportMode._CF_enumeration.addEnumeration(unicode_value='delete', tag='delete')
simpleType_PriceTableImportMode.delete_all = simpleType_PriceTableImportMode._CF_enumeration.addEnumeration(unicode_value='delete-all', tag='delete_all')
simpleType_PriceTableImportMode._InitializeFacetMap(simpleType_PriceTableImportMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.PriceTableImportMode', simpleType_PriceTableImportMode)
_module_typeBindings.simpleType_PriceTableImportMode = simpleType_PriceTableImportMode

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.Currency
class simpleType_Currency (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Currency')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 103, 4)
    _Documentation = None
simpleType_Currency._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Currency._CF_pattern.addPattern(pattern='[A-Z]{3}')
simpleType_Currency._InitializeFacetMap(simpleType_Currency._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Currency', simpleType_Currency)
_module_typeBindings.simpleType_Currency = simpleType_Currency

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.Generic.String
class simpleType_Generic_String (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 109, 4)
    _Documentation = None
simpleType_Generic_String._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String', simpleType_Generic_String)
_module_typeBindings.simpleType_Generic_String = simpleType_Generic_String

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.Generic.String.256
class simpleType_Generic_String_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 113, 4)
    _Documentation = None
simpleType_Generic_String_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_String_256._InitializeFacetMap(simpleType_Generic_String_256._CF_minLength,
   simpleType_Generic_String_256._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.256', simpleType_Generic_String_256)
_module_typeBindings.simpleType_Generic_String_256 = simpleType_Generic_String_256

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.Generic.String.4000
class simpleType_Generic_String_4000 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.4000')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 120, 4)
    _Documentation = None
simpleType_Generic_String_4000._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_4000._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(4000))
simpleType_Generic_String_4000._InitializeFacetMap(simpleType_Generic_String_4000._CF_minLength,
   simpleType_Generic_String_4000._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.4000', simpleType_Generic_String_4000)
_module_typeBindings.simpleType_Generic_String_4000 = simpleType_Generic_String_4000

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.Generic.NonEmptyString.100
class simpleType_Generic_NonEmptyString_100 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.100')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 129, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_100._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_100._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(100))
simpleType_Generic_NonEmptyString_100._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_100._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_100._InitializeFacetMap(simpleType_Generic_NonEmptyString_100._CF_minLength,
   simpleType_Generic_NonEmptyString_100._CF_maxLength,
   simpleType_Generic_NonEmptyString_100._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.100', simpleType_Generic_NonEmptyString_100)
_module_typeBindings.simpleType_Generic_NonEmptyString_100 = simpleType_Generic_NonEmptyString_100

# Atomic simple type: {http://www.demandware.com/xml/impex/pricebook/2006-10-31}simpleType.Generic.NonEmptyString.256
class simpleType_Generic_NonEmptyString_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 137, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_NonEmptyString_256._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_256._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_256._InitializeFacetMap(simpleType_Generic_NonEmptyString_256._CF_minLength,
   simpleType_Generic_NonEmptyString_256._CF_maxLength,
   simpleType_Generic_NonEmptyString_256._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.256', simpleType_Generic_NonEmptyString_256)
_module_typeBindings.simpleType_Generic_NonEmptyString_256 = simpleType_Generic_NonEmptyString_256

# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 12, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}pricebook uses Python identifier pricebook
    __pricebook = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'pricebook'), 'pricebook', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_CTD_ANON_httpwww_demandware_comxmlimpexpricebook2006_10_31pricebook', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 14, 16), )

    
    pricebook = property(__pricebook.value, __pricebook.set, None, None)

    _ElementMap.update({
        __pricebook.name() : __pricebook
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON = CTD_ANON


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceBook with content type ELEMENT_ONLY
class complexType_PriceBook (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceBook with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PriceBook')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 23, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}header uses Python identifier header
    __header = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'header'), 'header', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceBook_httpwww_demandware_comxmlimpexpricebook2006_10_31header', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 25, 12), )

    
    header = property(__header.value, __header.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}price-tables uses Python identifier price_tables
    __price_tables = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'price-tables'), 'price_tables', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceBook_httpwww_demandware_comxmlimpexpricebook2006_10_31price_tables', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 26, 12), )

    
    price_tables = property(__price_tables.value, __price_tables.set, None, None)

    _ElementMap.update({
        __header.name() : __header,
        __price_tables.name() : __price_tables
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_PriceBook = complexType_PriceBook
Namespace.addCategoryObject('typeBinding', 'complexType.PriceBook', complexType_PriceBook)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceTables with content type ELEMENT_ONLY
class complexType_PriceTables (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceTables with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PriceTables')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 45, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}price-table uses Python identifier price_table
    __price_table = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'price-table'), 'price_table', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTables_httpwww_demandware_comxmlimpexpricebook2006_10_31price_table', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 47, 12), )

    
    price_table = property(__price_table.value, __price_table.set, None, None)

    _ElementMap.update({
        __price_table.name() : __price_table
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_PriceTables = complexType_PriceTables
Namespace.addCategoryObject('typeBinding', 'complexType.PriceTables', complexType_PriceTables)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.AmountEntry with content type SIMPLE
class complexType_AmountEntry (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.AmountEntry with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.decimal
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.AmountEntry')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 65, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.decimal
    
    # Attribute quantity uses Python identifier quantity
    __quantity = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'quantity'), 'quantity', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_AmountEntry_quantity', pyxb.binding.datatypes.decimal)
    __quantity._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 68, 16)
    __quantity._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 68, 16)
    
    quantity = property(__quantity.value, __quantity.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __quantity.name() : __quantity
    })
_module_typeBindings.complexType_AmountEntry = complexType_AmountEntry
Namespace.addCategoryObject('typeBinding', 'complexType.AmountEntry', complexType_AmountEntry)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PercentageEntry with content type SIMPLE
class complexType_PercentageEntry (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PercentageEntry with content type SIMPLE"""
    _TypeDefinition = pyxb.binding.datatypes.decimal
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PercentageEntry')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 73, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.decimal
    
    # Attribute quantity uses Python identifier quantity
    __quantity = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'quantity'), 'quantity', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PercentageEntry_quantity', pyxb.binding.datatypes.decimal)
    __quantity._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 76, 16)
    __quantity._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 76, 16)
    
    quantity = property(__quantity.value, __quantity.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __quantity.name() : __quantity
    })
_module_typeBindings.complexType_PercentageEntry = complexType_PercentageEntry
Namespace.addCategoryObject('typeBinding', 'complexType.PercentageEntry', complexType_PercentageEntry)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}sharedType.CustomAttributes with content type ELEMENT_ONLY
class sharedType_CustomAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}sharedType.CustomAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 146, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}custom-attribute uses Python identifier custom_attribute
    __custom_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), 'custom_attribute', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_sharedType_CustomAttributes_httpwww_demandware_comxmlimpexpricebook2006_10_31custom_attribute', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 148, 12), )

    
    custom_attribute = property(__custom_attribute.value, __custom_attribute.set, None, None)

    _ElementMap.update({
        __custom_attribute.name() : __custom_attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.sharedType_CustomAttributes = sharedType_CustomAttributes
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttributes', sharedType_CustomAttributes)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.Header with content type ELEMENT_ONLY
class complexType_Header (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.Header with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Header')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 30, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}currency uses Python identifier currency
    __currency = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'currency'), 'currency', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31currency', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 32, 12), )

    
    currency = property(__currency.value, __currency.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}display-name uses Python identifier display_name
    __display_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'display-name'), 'display_name', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31display_name', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 33, 12), )

    
    display_name = property(__display_name.value, __display_name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31description', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 34, 12), )

    
    description = property(__description.value, __description.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}online-flag uses Python identifier online_flag
    __online_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-flag'), 'online_flag', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31online_flag', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 35, 12), )

    
    online_flag = property(__online_flag.value, __online_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}online-from uses Python identifier online_from
    __online_from = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-from'), 'online_from', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31online_from', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 36, 12), )

    
    online_from = property(__online_from.value, __online_from.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}online-to uses Python identifier online_to
    __online_to = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-to'), 'online_to', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31online_to', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 37, 12), )

    
    online_to = property(__online_to.value, __online_to.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}parent uses Python identifier parent
    __parent = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'parent'), 'parent', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31parent', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 38, 12), )

    
    parent = property(__parent.value, __parent.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_httpwww_demandware_comxmlimpexpricebook2006_10_31custom_attributes', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 39, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Attribute pricebook-id uses Python identifier pricebook_id
    __pricebook_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'pricebook-id'), 'pricebook_id', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_pricebook_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __pricebook_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 41, 8)
    __pricebook_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 41, 8)
    
    pricebook_id = property(__pricebook_id.value, __pricebook_id.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_Header_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 42, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 42, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __currency.name() : __currency,
        __display_name.name() : __display_name,
        __description.name() : __description,
        __online_flag.name() : __online_flag,
        __online_from.name() : __online_from,
        __online_to.name() : __online_to,
        __parent.name() : __parent,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        __pricebook_id.name() : __pricebook_id,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_Header = complexType_Header
Namespace.addCategoryObject('typeBinding', 'complexType.Header', complexType_Header)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceTable with content type ELEMENT_ONLY
class complexType_PriceTable (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceTable with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PriceTable')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 51, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}online-from uses Python identifier online_from
    __online_from = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-from'), 'online_from', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_httpwww_demandware_comxmlimpexpricebook2006_10_31online_from', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 53, 12), )

    
    online_from = property(__online_from.value, __online_from.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}online-to uses Python identifier online_to
    __online_to = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'online-to'), 'online_to', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_httpwww_demandware_comxmlimpexpricebook2006_10_31online_to', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 54, 12), )

    
    online_to = property(__online_to.value, __online_to.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}amount uses Python identifier amount
    __amount = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'amount'), 'amount', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_httpwww_demandware_comxmlimpexpricebook2006_10_31amount', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 56, 16), )

    
    amount = property(__amount.value, __amount.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}percentage uses Python identifier percentage
    __percentage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'percentage'), 'percentage', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_httpwww_demandware_comxmlimpexpricebook2006_10_31percentage', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 57, 16), )

    
    percentage = property(__percentage.value, __percentage.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}price-info uses Python identifier price_info
    __price_info = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'price-info'), 'price_info', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_httpwww_demandware_comxmlimpexpricebook2006_10_31price_info', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 59, 12), )

    
    price_info = property(__price_info.value, __price_info.set, None, None)

    
    # Attribute product-id uses Python identifier product_id
    __product_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'product-id'), 'product_id', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_product_id', _module_typeBindings.simpleType_Generic_NonEmptyString_100, required=True)
    __product_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 61, 8)
    __product_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 61, 8)
    
    product_id = property(__product_id.value, __product_id.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_PriceTable_mode', _module_typeBindings.simpleType_PriceTableImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 62, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 62, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __online_from.name() : __online_from,
        __online_to.name() : __online_to,
        __amount.name() : __amount,
        __percentage.name() : __percentage,
        __price_info.name() : __price_info
    })
    _AttributeMap.update({
        __product_id.name() : __product_id,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_PriceTable = complexType_PriceTable
Namespace.addCategoryObject('typeBinding', 'complexType.PriceTable', complexType_PriceTable)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.LocalizedString with content type SIMPLE
class complexType_LocalizedString (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.LocalizedString with content type SIMPLE"""
    _TypeDefinition = simpleType_Generic_String_4000
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.LocalizedString')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 81, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_Generic_String_4000
    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_complexType_LocalizedString_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 84, 16)
    
    lang = property(__lang.value, __lang.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __lang.name() : __lang
    })
_module_typeBindings.complexType_LocalizedString = complexType_LocalizedString
Namespace.addCategoryObject('typeBinding', 'complexType.LocalizedString', complexType_LocalizedString)


# Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}sharedType.CustomAttribute with content type MIXED
class sharedType_CustomAttribute (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/pricebook/2006-10-31}sharedType.CustomAttribute with content type MIXED"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_MIXED
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 152, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/pricebook/2006-10-31}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_sharedType_CustomAttribute_httpwww_demandware_comxmlimpexpricebook2006_10_31value', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 154, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_sharedType_CustomAttribute_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 157, 8)
    
    lang = property(__lang.value, __lang.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexpricebook2006_10_31_sharedType_CustomAttribute_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 156, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 156, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        __lang.name() : __lang,
        __attribute_id.name() : __attribute_id
    })
_module_typeBindings.sharedType_CustomAttribute = sharedType_CustomAttribute
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttribute', sharedType_CustomAttribute)


pricebooks = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'pricebooks'), CTD_ANON, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 11, 4))
Namespace.addCategoryObject('elementBinding', pricebooks.name().localName(), pricebooks)

header = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'header'), complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 20, 4))
Namespace.addCategoryObject('elementBinding', header.name().localName(), header)

price_table = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-table'), complexType_PriceTable, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 21, 4))
Namespace.addCategoryObject('elementBinding', price_table.name().localName(), price_table)



CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'pricebook'), complexType_PriceBook, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 14, 16)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 14, 16))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'pricebook')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 14, 16))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton()




complexType_PriceBook._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'header'), complexType_Header, scope=complexType_PriceBook, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 25, 12)))

complexType_PriceBook._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-tables'), complexType_PriceTables, scope=complexType_PriceBook, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 26, 12)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 26, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_PriceBook._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'header')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 25, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceBook._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'price-tables')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 26, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_PriceBook._Automaton = _BuildAutomaton_()




complexType_PriceTables._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-table'), complexType_PriceTable, scope=complexType_PriceTables, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 47, 12)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 47, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceTables._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'price-table')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 47, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_PriceTables._Automaton = _BuildAutomaton_2()




sharedType_CustomAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), sharedType_CustomAttribute, scope=sharedType_CustomAttributes, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 148, 12)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 148, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 148, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttributes._Automaton = _BuildAutomaton_3()




complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'currency'), simpleType_Currency, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 32, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'display-name'), complexType_LocalizedString, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 33, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), complexType_LocalizedString, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 34, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 35, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-from'), pyxb.binding.datatypes.dateTime, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 36, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-to'), pyxb.binding.datatypes.dateTime, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 37, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'parent'), simpleType_Generic_NonEmptyString_256, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 38, 12)))

complexType_Header._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_Header, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 39, 12)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 33, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 34, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 35, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 36, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 37, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 38, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 39, 12))
    counters.add(cc_6)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'currency')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 32, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'display-name')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 33, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 34, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-flag')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 35, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-from')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 36, 12))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-to')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 37, 12))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'parent')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 38, 12))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Header._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 39, 12))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
complexType_Header._Automaton = _BuildAutomaton_4()




complexType_PriceTable._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-from'), pyxb.binding.datatypes.dateTime, scope=complexType_PriceTable, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 53, 12)))

complexType_PriceTable._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'online-to'), pyxb.binding.datatypes.dateTime, scope=complexType_PriceTable, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 54, 12)))

complexType_PriceTable._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'amount'), complexType_AmountEntry, scope=complexType_PriceTable, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 56, 16)))

complexType_PriceTable._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'percentage'), complexType_PercentageEntry, scope=complexType_PriceTable, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 57, 16)))

complexType_PriceTable._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-info'), simpleType_Generic_String_256, scope=complexType_PriceTable, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 59, 12)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 53, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 54, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 55, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 59, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceTable._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-from')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 53, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceTable._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'online-to')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 54, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceTable._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'amount')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 56, 16))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceTable._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'percentage')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 57, 16))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceTable._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'price-info')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 59, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_PriceTable._Automaton = _BuildAutomaton_5()




sharedType_CustomAttribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), simpleType_Generic_String, scope=sharedType_CustomAttribute, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 154, 12)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 154, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/pricebook.xsd', 154, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttribute._Automaton = _BuildAutomaton_6()

