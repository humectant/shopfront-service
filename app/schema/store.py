# ./binding.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:24ab06c6bc656caea0751f763e5b9285e88a57cd
# Generated 2018-04-19 09:10:45.021829 by PyXB version 1.2.6 using Python 3.6.5.final.0
# Namespace http://www.demandware.com/xml/impex/store/2007-04-30

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:37048d4a-43ec-11e8-84ba-8c85900cdc76')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes
import pyxb.binding.xml_

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://www.demandware.com/xml/impex/store/2007-04-30', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Generic.String
class simpleType_Generic_String (pyxb.binding.datatypes.string):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 120, 4)
    _Documentation = None
simpleType_Generic_String._InitializeFacetMap()
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String', simpleType_Generic_String)
_module_typeBindings.simpleType_Generic_String = simpleType_Generic_String

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Latitude
class simpleType_Latitude (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Latitude')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 175, 4)
    _Documentation = None
simpleType_Latitude._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=simpleType_Latitude, value=pyxb.binding.datatypes.decimal('-90.0'))
simpleType_Latitude._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=simpleType_Latitude, value=pyxb.binding.datatypes.decimal('90.0'))
simpleType_Latitude._InitializeFacetMap(simpleType_Latitude._CF_minInclusive,
   simpleType_Latitude._CF_maxInclusive)
Namespace.addCategoryObject('typeBinding', 'simpleType.Latitude', simpleType_Latitude)
_module_typeBindings.simpleType_Latitude = simpleType_Latitude

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Longitude
class simpleType_Longitude (pyxb.binding.datatypes.decimal):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Longitude')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 182, 4)
    _Documentation = None
simpleType_Longitude._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=simpleType_Longitude, value=pyxb.binding.datatypes.decimal('-180.0'))
simpleType_Longitude._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=simpleType_Longitude, value=pyxb.binding.datatypes.decimal('180.0'))
simpleType_Longitude._InitializeFacetMap(simpleType_Longitude._CF_minInclusive,
   simpleType_Longitude._CF_maxInclusive)
Namespace.addCategoryObject('typeBinding', 'simpleType.Longitude', simpleType_Longitude)
_module_typeBindings.simpleType_Longitude = simpleType_Longitude

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.ImportMode
class simpleType_ImportMode (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.ImportMode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 189, 4)
    _Documentation = None
simpleType_ImportMode._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=simpleType_ImportMode, enum_prefix=None)
simpleType_ImportMode.delete = simpleType_ImportMode._CF_enumeration.addEnumeration(unicode_value='delete', tag='delete')
simpleType_ImportMode._InitializeFacetMap(simpleType_ImportMode._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'simpleType.ImportMode', simpleType_ImportMode)
_module_typeBindings.simpleType_ImportMode = simpleType_ImportMode

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Generic.String.10
class simpleType_Generic_String_10 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.10')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 124, 4)
    _Documentation = None
simpleType_Generic_String_10._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_10._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(10))
simpleType_Generic_String_10._InitializeFacetMap(simpleType_Generic_String_10._CF_minLength,
   simpleType_Generic_String_10._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.10', simpleType_Generic_String_10)
_module_typeBindings.simpleType_Generic_String_10 = simpleType_Generic_String_10

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Generic.String.256
class simpleType_Generic_String_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 131, 4)
    _Documentation = None
simpleType_Generic_String_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_String_256._InitializeFacetMap(simpleType_Generic_String_256._CF_minLength,
   simpleType_Generic_String_256._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.256', simpleType_Generic_String_256)
_module_typeBindings.simpleType_Generic_String_256 = simpleType_Generic_String_256

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Generic.String.4000
class simpleType_Generic_String_4000 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.String.4000')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 138, 4)
    _Documentation = None
simpleType_Generic_String_4000._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Generic_String_4000._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(4000))
simpleType_Generic_String_4000._InitializeFacetMap(simpleType_Generic_String_4000._CF_minLength,
   simpleType_Generic_String_4000._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.String.4000', simpleType_Generic_String_4000)
_module_typeBindings.simpleType_Generic_String_4000 = simpleType_Generic_String_4000

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Generic.NonEmptyString.256
class simpleType_Generic_NonEmptyString_256 (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Generic.NonEmptyString.256')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 146, 4)
    _Documentation = None
simpleType_Generic_NonEmptyString_256._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
simpleType_Generic_NonEmptyString_256._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Generic_NonEmptyString_256._CF_pattern = pyxb.binding.facets.CF_pattern()
simpleType_Generic_NonEmptyString_256._CF_pattern.addPattern(pattern='\\S|(\\S(.*)\\S)')
simpleType_Generic_NonEmptyString_256._InitializeFacetMap(simpleType_Generic_NonEmptyString_256._CF_minLength,
   simpleType_Generic_NonEmptyString_256._CF_maxLength,
   simpleType_Generic_NonEmptyString_256._CF_pattern)
Namespace.addCategoryObject('typeBinding', 'simpleType.Generic.NonEmptyString.256', simpleType_Generic_NonEmptyString_256)
_module_typeBindings.simpleType_Generic_NonEmptyString_256 = simpleType_Generic_NonEmptyString_256

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.CountryCode
class simpleType_CountryCode (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.CountryCode')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 154, 4)
    _Documentation = None
simpleType_CountryCode._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
simpleType_CountryCode._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
simpleType_CountryCode._InitializeFacetMap(simpleType_CountryCode._CF_minLength,
   simpleType_CountryCode._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.CountryCode', simpleType_CountryCode)
_module_typeBindings.simpleType_CountryCode = simpleType_CountryCode

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.Email
class simpleType_Email (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.Email')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 161, 4)
    _Documentation = None
simpleType_Email._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_Email._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_Email._InitializeFacetMap(simpleType_Email._CF_minLength,
   simpleType_Email._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.Email', simpleType_Email)
_module_typeBindings.simpleType_Email = simpleType_Email

# Atomic simple type: {http://www.demandware.com/xml/impex/store/2007-04-30}simpleType.PhoneNumber
class simpleType_PhoneNumber (simpleType_Generic_String):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'simpleType.PhoneNumber')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 168, 4)
    _Documentation = None
simpleType_PhoneNumber._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(0))
simpleType_PhoneNumber._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
simpleType_PhoneNumber._InitializeFacetMap(simpleType_PhoneNumber._CF_minLength,
   simpleType_PhoneNumber._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'simpleType.PhoneNumber', simpleType_PhoneNumber)
_module_typeBindings.simpleType_PhoneNumber = simpleType_PhoneNumber

# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 11, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}store uses Python identifier store
    __store = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store'), 'store', '__httpwww_demandware_comxmlimpexstore2007_04_30_CTD_ANON_httpwww_demandware_comxmlimpexstore2007_04_30store', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 13, 16), )

    
    store = property(__store.value, __store.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}store-group uses Python identifier store_group
    __store_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-group'), 'store_group', '__httpwww_demandware_comxmlimpexstore2007_04_30_CTD_ANON_httpwww_demandware_comxmlimpexstore2007_04_30store_group', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 14, 16), )

    
    store_group = property(__store_group.value, __store_group.set, None, '\n                            store-group are part of "DemandwareStore" beta feature. This element will be ignored on import if the feature is not enabled.\n                        ')

    _ElementMap.update({
        __store.name() : __store,
        __store_group.name() : __store_group
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON = CTD_ANON


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.StoreList with content type ELEMENT_ONLY
class complexType_StoreList (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.StoreList with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.StoreList')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 74, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}store uses Python identifier store
    __store = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store'), 'store', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreList_httpwww_demandware_comxmlimpexstore2007_04_30store', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 76, 12), )

    
    store = property(__store.value, __store.set, None, None)

    _ElementMap.update({
        __store.name() : __store
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_StoreList = complexType_StoreList
Namespace.addCategoryObject('typeBinding', 'complexType.StoreList', complexType_StoreList)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.PriceBookList with content type ELEMENT_ONLY
class complexType_PriceBookList (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.PriceBookList with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PriceBookList')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 80, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}price-book uses Python identifier price_book
    __price_book = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'price-book'), 'price_book', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_PriceBookList_httpwww_demandware_comxmlimpexstore2007_04_30price_book', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 82, 12), )

    
    price_book = property(__price_book.value, __price_book.set, None, None)

    _ElementMap.update({
        __price_book.name() : __price_book
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.complexType_PriceBookList = complexType_PriceBookList
Namespace.addCategoryObject('typeBinding', 'complexType.PriceBookList', complexType_PriceBookList)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}sharedType.CustomAttributes with content type ELEMENT_ONLY
class sharedType_CustomAttributes (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}sharedType.CustomAttributes with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttributes')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 104, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}custom-attribute uses Python identifier custom_attribute
    __custom_attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), 'custom_attribute', '__httpwww_demandware_comxmlimpexstore2007_04_30_sharedType_CustomAttributes_httpwww_demandware_comxmlimpexstore2007_04_30custom_attribute', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 106, 12), )

    
    custom_attribute = property(__custom_attribute.value, __custom_attribute.set, None, None)

    _ElementMap.update({
        __custom_attribute.name() : __custom_attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.sharedType_CustomAttributes = sharedType_CustomAttributes
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttributes', sharedType_CustomAttributes)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}sharedType.LocalizedText with content type SIMPLE
class sharedType_LocalizedText (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}sharedType.LocalizedText with content type SIMPLE"""
    _TypeDefinition = simpleType_Generic_String
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.LocalizedText')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 96, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is simpleType_Generic_String
    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexstore2007_04_30_sharedType_LocalizedText_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 99, 16)
    
    lang = property(__lang.value, __lang.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __lang.name() : __lang
    })
_module_typeBindings.sharedType_LocalizedText = sharedType_LocalizedText
Namespace.addCategoryObject('typeBinding', 'sharedType.LocalizedText', sharedType_LocalizedText)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.Store with content type ELEMENT_ONLY
class complexType_Store (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.Store with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.Store')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 29, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30name', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 31, 12), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}address1 uses Python identifier address1
    __address1 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'address1'), 'address1', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30address1', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 32, 12), )

    
    address1 = property(__address1.value, __address1.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}address2 uses Python identifier address2
    __address2 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'address2'), 'address2', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30address2', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 33, 12), )

    
    address2 = property(__address2.value, __address2.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}city uses Python identifier city
    __city = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'city'), 'city', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30city', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 34, 12), )

    
    city = property(__city.value, __city.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}postal-code uses Python identifier postal_code
    __postal_code = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'postal-code'), 'postal_code', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30postal_code', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 35, 12), )

    
    postal_code = property(__postal_code.value, __postal_code.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}state-code uses Python identifier state_code
    __state_code = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'state-code'), 'state_code', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30state_code', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 36, 12), )

    
    state_code = property(__state_code.value, __state_code.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}country-code uses Python identifier country_code
    __country_code = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'country-code'), 'country_code', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30country_code', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 37, 12), )

    
    country_code = property(__country_code.value, __country_code.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}email uses Python identifier email
    __email = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'email'), 'email', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30email', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 38, 12), )

    
    email = property(__email.value, __email.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}phone uses Python identifier phone
    __phone = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'phone'), 'phone', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30phone', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 39, 12), )

    
    phone = property(__phone.value, __phone.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}fax uses Python identifier fax
    __fax = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'fax'), 'fax', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30fax', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 40, 12), )

    
    fax = property(__fax.value, __fax.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}store-events uses Python identifier store_events
    __store_events = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-events'), 'store_events', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30store_events', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 41, 12), )

    
    store_events = property(__store_events.value, __store_events.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}store-hours uses Python identifier store_hours
    __store_hours = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-hours'), 'store_hours', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30store_hours', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 42, 12), )

    
    store_hours = property(__store_hours.value, __store_hours.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}image uses Python identifier image
    __image = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'image'), 'image', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30image', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 43, 12), )

    
    image = property(__image.value, __image.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}latitude uses Python identifier latitude
    __latitude = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'latitude'), 'latitude', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30latitude', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 44, 12), )

    
    latitude = property(__latitude.value, __latitude.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}longitude uses Python identifier longitude
    __longitude = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'longitude'), 'longitude', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30longitude', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 45, 12), )

    
    longitude = property(__longitude.value, __longitude.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}inventory-list-id uses Python identifier inventory_list_id
    __inventory_list_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'inventory-list-id'), 'inventory_list_id', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30inventory_list_id', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 46, 12), )

    
    inventory_list_id = property(__inventory_list_id.value, __inventory_list_id.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}store-locator-enabled-flag uses Python identifier store_locator_enabled_flag
    __store_locator_enabled_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'store-locator-enabled-flag'), 'store_locator_enabled_flag', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30store_locator_enabled_flag', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 47, 12), )

    
    store_locator_enabled_flag = property(__store_locator_enabled_flag.value, __store_locator_enabled_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}demandware-pos-enabled-flag uses Python identifier demandware_pos_enabled_flag
    __demandware_pos_enabled_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'demandware-pos-enabled-flag'), 'demandware_pos_enabled_flag', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30demandware_pos_enabled_flag', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 48, 12), )

    
    demandware_pos_enabled_flag = property(__demandware_pos_enabled_flag.value, __demandware_pos_enabled_flag.set, None, '\n                        Deprecated and will be removed in a future release.\n                        Please use pos-enabled-flag instead.\n                    ')

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}pos-enabled-flag uses Python identifier pos_enabled_flag
    __pos_enabled_flag = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'pos-enabled-flag'), 'pos_enabled_flag', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30pos_enabled_flag', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 56, 12), )

    
    pos_enabled_flag = property(__pos_enabled_flag.value, __pos_enabled_flag.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_httpwww_demandware_comxmlimpexstore2007_04_30custom_attributes', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 57, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Attribute store-id uses Python identifier store_id
    __store_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'store-id'), 'store_id', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_store_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __store_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 59, 8)
    __store_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 59, 8)
    
    store_id = property(__store_id.value, __store_id.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_Store_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 60, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 60, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __address1.name() : __address1,
        __address2.name() : __address2,
        __city.name() : __city,
        __postal_code.name() : __postal_code,
        __state_code.name() : __state_code,
        __country_code.name() : __country_code,
        __email.name() : __email,
        __phone.name() : __phone,
        __fax.name() : __fax,
        __store_events.name() : __store_events,
        __store_hours.name() : __store_hours,
        __image.name() : __image,
        __latitude.name() : __latitude,
        __longitude.name() : __longitude,
        __inventory_list_id.name() : __inventory_list_id,
        __store_locator_enabled_flag.name() : __store_locator_enabled_flag,
        __demandware_pos_enabled_flag.name() : __demandware_pos_enabled_flag,
        __pos_enabled_flag.name() : __pos_enabled_flag,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        __store_id.name() : __store_id,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_Store = complexType_Store
Namespace.addCategoryObject('typeBinding', 'complexType.Store', complexType_Store)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.StoreGroup with content type ELEMENT_ONLY
class complexType_StoreGroup (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.StoreGroup with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.StoreGroup')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 63, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreGroup_httpwww_demandware_comxmlimpexstore2007_04_30name', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 65, 12), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}stores uses Python identifier stores
    __stores = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'stores'), 'stores', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreGroup_httpwww_demandware_comxmlimpexstore2007_04_30stores', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 66, 12), )

    
    stores = property(__stores.value, __stores.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}price-books uses Python identifier price_books
    __price_books = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'price-books'), 'price_books', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreGroup_httpwww_demandware_comxmlimpexstore2007_04_30price_books', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 67, 12), )

    
    price_books = property(__price_books.value, __price_books.set, None, None)

    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}custom-attributes uses Python identifier custom_attributes
    __custom_attributes = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), 'custom_attributes', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreGroup_httpwww_demandware_comxmlimpexstore2007_04_30custom_attributes', False, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 68, 12), )

    
    custom_attributes = property(__custom_attributes.value, __custom_attributes.set, None, None)

    
    # Attribute store-group-id uses Python identifier store_group_id
    __store_group_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'store-group-id'), 'store_group_id', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreGroup_store_group_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __store_group_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 70, 8)
    __store_group_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 70, 8)
    
    store_group_id = property(__store_group_id.value, __store_group_id.set, None, None)

    
    # Attribute mode uses Python identifier mode
    __mode = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mode'), 'mode', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreGroup_mode', _module_typeBindings.simpleType_ImportMode)
    __mode._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 71, 8)
    __mode._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 71, 8)
    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __stores.name() : __stores,
        __price_books.name() : __price_books,
        __custom_attributes.name() : __custom_attributes
    })
    _AttributeMap.update({
        __store_group_id.name() : __store_group_id,
        __mode.name() : __mode
    })
_module_typeBindings.complexType_StoreGroup = complexType_StoreGroup
Namespace.addCategoryObject('typeBinding', 'complexType.StoreGroup', complexType_StoreGroup)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.StoreAssignment with content type EMPTY
class complexType_StoreAssignment (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.StoreAssignment with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.StoreAssignment')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 86, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute store-id uses Python identifier store_id
    __store_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'store-id'), 'store_id', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_StoreAssignment_store_id', _module_typeBindings.simpleType_Generic_String_256, required=True)
    __store_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 87, 8)
    __store_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 87, 8)
    
    store_id = property(__store_id.value, __store_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __store_id.name() : __store_id
    })
_module_typeBindings.complexType_StoreAssignment = complexType_StoreAssignment
Namespace.addCategoryObject('typeBinding', 'complexType.StoreAssignment', complexType_StoreAssignment)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.PriceBookAssignment with content type EMPTY
class complexType_PriceBookAssignment (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}complexType.PriceBookAssignment with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'complexType.PriceBookAssignment')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 90, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute price-book-id uses Python identifier price_book_id
    __price_book_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'price-book-id'), 'price_book_id', '__httpwww_demandware_comxmlimpexstore2007_04_30_complexType_PriceBookAssignment_price_book_id', _module_typeBindings.simpleType_Generic_String_256, required=True)
    __price_book_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 91, 8)
    __price_book_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 91, 8)
    
    price_book_id = property(__price_book_id.value, __price_book_id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __price_book_id.name() : __price_book_id
    })
_module_typeBindings.complexType_PriceBookAssignment = complexType_PriceBookAssignment
Namespace.addCategoryObject('typeBinding', 'complexType.PriceBookAssignment', complexType_PriceBookAssignment)


# Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}sharedType.CustomAttribute with content type MIXED
class sharedType_CustomAttribute (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.demandware.com/xml/impex/store/2007-04-30}sharedType.CustomAttribute with content type MIXED"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_MIXED
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'sharedType.CustomAttribute')
    _XSDLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 110, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.demandware.com/xml/impex/store/2007-04-30}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_demandware_comxmlimpexstore2007_04_30_sharedType_CustomAttribute_httpwww_demandware_comxmlimpexstore2007_04_30value', True, pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 112, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    
    # Attribute {http://www.w3.org/XML/1998/namespace}lang uses Python identifier lang
    __lang = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(pyxb.namespace.XML, 'lang'), 'lang', '__httpwww_demandware_comxmlimpexstore2007_04_30_sharedType_CustomAttribute_httpwww_w3_orgXML1998namespacelang', pyxb.binding.xml_.STD_ANON_lang)
    __lang._DeclarationLocation = None
    __lang._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 115, 8)
    
    lang = property(__lang.value, __lang.set, None, None)

    
    # Attribute attribute-id uses Python identifier attribute_id
    __attribute_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'attribute-id'), 'attribute_id', '__httpwww_demandware_comxmlimpexstore2007_04_30_sharedType_CustomAttribute_attribute_id', _module_typeBindings.simpleType_Generic_NonEmptyString_256, required=True)
    __attribute_id._DeclarationLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 114, 8)
    __attribute_id._UseLocation = pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 114, 8)
    
    attribute_id = property(__attribute_id.value, __attribute_id.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        __lang.name() : __lang,
        __attribute_id.name() : __attribute_id
    })
_module_typeBindings.sharedType_CustomAttribute = sharedType_CustomAttribute
Namespace.addCategoryObject('typeBinding', 'sharedType.CustomAttribute', sharedType_CustomAttribute)


stores = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'stores'), CTD_ANON, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 10, 4))
Namespace.addCategoryObject('elementBinding', stores.name().localName(), stores)

store = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store'), complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 26, 4))
Namespace.addCategoryObject('elementBinding', store.name().localName(), store)

store_group = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-group'), complexType_StoreGroup, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 27, 4))
Namespace.addCategoryObject('elementBinding', store_group.name().localName(), store_group)



CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store'), complexType_Store, scope=CTD_ANON, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 13, 16)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-group'), complexType_StoreGroup, scope=CTD_ANON, documentation='\n                            store-group are part of "DemandwareStore" beta feature. This element will be ignored on import if the feature is not enabled.\n                        ', location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 14, 16)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 13, 16))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 14, 16))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 13, 16))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-group')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 14, 16))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton()




complexType_StoreList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store'), complexType_StoreAssignment, scope=complexType_StoreList, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 76, 12)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 76, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_StoreList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 76, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_StoreList._Automaton = _BuildAutomaton_()




complexType_PriceBookList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-book'), complexType_PriceBookAssignment, scope=complexType_PriceBookList, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 82, 12)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 82, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_PriceBookList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'price-book')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 82, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_PriceBookList._Automaton = _BuildAutomaton_2()




sharedType_CustomAttributes._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute'), sharedType_CustomAttribute, scope=sharedType_CustomAttributes, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 106, 12)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 106, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttributes._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attribute')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 106, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttributes._Automaton = _BuildAutomaton_3()




complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 31, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'address1'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 32, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'address2'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 33, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'city'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 34, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'postal-code'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 35, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'state-code'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 36, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'country-code'), simpleType_CountryCode, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 37, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'email'), simpleType_Email, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 38, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'phone'), simpleType_PhoneNumber, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 39, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'fax'), simpleType_PhoneNumber, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 40, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-events'), sharedType_LocalizedText, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 41, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-hours'), sharedType_LocalizedText, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 42, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'image'), simpleType_Generic_String_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 43, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'latitude'), simpleType_Latitude, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 44, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'longitude'), simpleType_Longitude, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 45, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'inventory-list-id'), simpleType_Generic_NonEmptyString_256, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 46, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'store-locator-enabled-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 47, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'demandware-pos-enabled-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Store, documentation='\n                        Deprecated and will be removed in a future release.\n                        Please use pos-enabled-flag instead.\n                    ', location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 48, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'pos-enabled-flag'), pyxb.binding.datatypes.boolean, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 56, 12)))

complexType_Store._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_Store, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 57, 12)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 31, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 32, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 33, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 34, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 35, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 36, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 37, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 38, 12))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 39, 12))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 40, 12))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 41, 12))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 42, 12))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 43, 12))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 44, 12))
    counters.add(cc_13)
    cc_14 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 45, 12))
    counters.add(cc_14)
    cc_15 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 46, 12))
    counters.add(cc_15)
    cc_16 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 47, 12))
    counters.add(cc_16)
    cc_17 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 48, 12))
    counters.add(cc_17)
    cc_18 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 56, 12))
    counters.add(cc_18)
    cc_19 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 57, 12))
    counters.add(cc_19)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 31, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'address1')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 32, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'address2')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 33, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'city')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 34, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'postal-code')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 35, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'state-code')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 36, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'country-code')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 37, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'email')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 38, 12))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'phone')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 39, 12))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fax')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 40, 12))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-events')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 41, 12))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-hours')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 42, 12))
    st_11 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'image')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 43, 12))
    st_12 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'latitude')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 44, 12))
    st_13 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_14, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'longitude')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 45, 12))
    st_14 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_14)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_15, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'inventory-list-id')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 46, 12))
    st_15 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_15)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_16, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'store-locator-enabled-flag')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 47, 12))
    st_16 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_16)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_17, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'demandware-pos-enabled-flag')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 48, 12))
    st_17 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_17)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_18, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'pos-enabled-flag')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 56, 12))
    st_18 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_18)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_19, False))
    symbol = pyxb.binding.content.ElementUse(complexType_Store._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 57, 12))
    st_19 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_19)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_12, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_13, True) ]))
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_13, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_13, False) ]))
    st_13._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_14, [
        fac.UpdateInstruction(cc_14, True) ]))
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_14, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_14, False) ]))
    st_14._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_15, [
        fac.UpdateInstruction(cc_15, True) ]))
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_15, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_15, False) ]))
    st_15._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_16, [
        fac.UpdateInstruction(cc_16, True) ]))
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_16, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_16, False) ]))
    st_16._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_17, [
        fac.UpdateInstruction(cc_17, True) ]))
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_17, False) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_17, False) ]))
    st_17._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_18, [
        fac.UpdateInstruction(cc_18, True) ]))
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_18, False) ]))
    st_18._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_19, [
        fac.UpdateInstruction(cc_19, True) ]))
    st_19._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_Store._Automaton = _BuildAutomaton_4()




complexType_StoreGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), simpleType_Generic_String_256, scope=complexType_StoreGroup, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 65, 12)))

complexType_StoreGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'stores'), complexType_StoreList, scope=complexType_StoreGroup, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 66, 12)))

complexType_StoreGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'price-books'), complexType_PriceBookList, scope=complexType_StoreGroup, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 67, 12)))

complexType_StoreGroup._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes'), sharedType_CustomAttributes, scope=complexType_StoreGroup, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 68, 12)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 65, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 66, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 67, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 68, 12))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(complexType_StoreGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 65, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(complexType_StoreGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'stores')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 66, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(complexType_StoreGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'price-books')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 67, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(complexType_StoreGroup._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'custom-attributes')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 68, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
complexType_StoreGroup._Automaton = _BuildAutomaton_5()




sharedType_CustomAttribute._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), simpleType_Generic_String, scope=sharedType_CustomAttribute, location=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 112, 12)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 112, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(sharedType_CustomAttribute._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/Users/brett/Develop/Sur La Table/Data Feeds/schema/store.xsd', 112, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
sharedType_CustomAttribute._Automaton = _BuildAutomaton_6()

