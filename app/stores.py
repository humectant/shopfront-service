import pyxb

from statsd import StatsClient
from typing import List
from pydantic import BaseModel
from app.util import shopfront
from app.schema import store as pyxb_store
from app.configuration import CONFIG

import logging

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-stores')

logger = logging.getLogger('shopfront.stores')
logger.setLevel(CONFIG.LOG_LEVEL)


class Address(BaseModel):

    address1: str = None
    address2: str = None
    city: str = None
    country: str = None
    county: str = None
    postalCode: str = None
    state: str = None


class Hours(BaseModel):

    title: str
    description: str = None


class Store(BaseModel):
    name: str
    storeId: str
    address: Address = None
    channel: str = None
    culinaryEmail: str = None
    culinaryPhone: str = None
    district: str = None
    closeDate: str = None
    email: str = None
    fax: str = None
    fob: str = None
    hours: List[Hours] = None
    isCulinary: bool = None
    latitude: float = None
    longitude: float = None
    openDate: str = None
    phone: str = None
    title: str = None
    type: str = None


class Stores(BaseModel):
    stores: List[Store] = None

    def to_pyxb(self):

        pyxb_stores = pyxb_store.stores()
        for store in self.stores:

            if not store.address:
                continue

            store_record = pyxb_store.store()
            store_record.store_id = store.storeId
            store_record.name = store.name

            # Address
            store_record.address1 = store.address.address1
            store_record.city = store.address.city
            store_record.state_code = store.address.state
            store_record.country_code = store.address.country
            store_record.postal_code = store.address.postalCode

            # Contact info
            store_record.latitude = store.latitude
            store_record.longitude = store.longitude
            store_record.email = store.email
            store_record.phone = store.phone
            store_record.fax = store.fax

            # Store hours
            store_hours_str = ""
            if store.hours:
                for hours in store.hours:
                    title = hours.title
                    description = hours.description
                    store_hours_str += f'{title}: {description} <br/>'
                store_record.store_hours = [store_hours_str]

            pyxb_stores.append(store_record)

        return pyxb_stores


@statsd.timer('create_store')
def create_store(store: Store, save=True, tracking_id=None):

    logger.info(f'create_store', extra={"tracking_id": tracking_id})
    stores = Stores(stores=[store])
    return create_stores_bulk(stores)


@statsd.timer('create_stores_bulk')
def create_stores_bulk(stores_object: Stores, save=True, tracking_id=None):

    logger.info(f'create_stores_bulk', extra={"tracking_id": tracking_id})
    doc = stores_object.to_pyxb()
    xml = shopfront.to_xml(doc, CONFIG.stores_namespace)
    path = shopfront.url_join(CONFIG.output_directory, CONFIG.stores_directory)
    if save:
        shopfront.save(CONFIG.output_destination,
                       path,
                       shopfront.file_name(CONFIG.stores_filename),
                       xml)
    return xml.decode("utf-8")



