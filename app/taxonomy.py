import pyxb

from statsd import StatsClient
from typing import List
from pydantic import BaseModel
from app.util import shopfront
from app.schema import catalog
from app.configuration import CONFIG

import logging

pyxb.RequireValidWhenGenerating(False)

statsd = StatsClient(prefix='shopfront-service-taxonomy')

logger = logging.getLogger('shopfront.taxonomy')
logger.setLevel(CONFIG.LOG_LEVEL)


class Category(BaseModel):

    id: str
    displayName: str
    pageTitle: str
    parent: str


class Taxonomy(BaseModel):

    categories: List[Category]

    def to_pyxb(self):

        cat = catalog.catalog()
        cat.catalog_id = 'shop-slt-storefront-catalog'

        for category in self.categories:
            logger.info(f'category: {category}')
            category_elem = catalog.category()
            category_elem.category_id = category.id
            category_elem.display_name = [category.displayName]
            category_elem.pageTitle = [category.pageTitle]
            category_elem.parent = category.parent
            cat.append(category_elem)

        return cat


@statsd.timer('create_taxonomy_bulk')
def create_taxonomy_bulk(tax: Taxonomy, save=True, tracking_id=None):

    logger.info(f'create_taxonomy_bulk', extra={"tracking_id": tracking_id})
    doc = tax.to_pyxb()
    xml = shopfront.to_xml(doc, CONFIG.taxonomy_namespace)
    path = shopfront.url_join(
        CONFIG.output_directory,
        CONFIG.taxonomy_directory)
    if save:
        shopfront.save(CONFIG.output_destination,
                       path,
                       shopfront.file_name(CONFIG.taxonomy_filename),
                       xml)
    return xml.decode("utf-8")



