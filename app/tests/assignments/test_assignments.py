import json
import warnings
from app import assignments

warnings.filterwarnings(action='ignore', module='.*paramiko.*')


def test_assignments_bulk():
    with open("app/tests/assignments/assignments_many.json") as f:
        data = f.read()
    items = json.loads(data)
    some_assignments = assignments.Assignments(**items)
    xml = assignments.create_assignments_bulk(some_assignments, save=False)
    print("xml", xml)
    assert "TCA-257775" in xml

