import json
import warnings
import pytest
from app import culinary

warnings.filterwarnings(action='ignore', module='.*paramiko.*')


@pytest.mark.skip(reason="Not ready")
def test_culinary_bulk():
    with open("app/tests/culinary/culinary_with_events.json") as f:
        data = f.read()
    items = json.loads(data)
    some_culinary = culinary.CulinaryProducts(**items)
    xml = culinary.create_culinary_bulk(some_culinary, save=False)
    print("xml", xml)
    assert "CFA-1001809" in xml
