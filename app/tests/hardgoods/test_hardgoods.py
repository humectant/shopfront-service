import json
import warnings
import pytest
from app import hardgoods


def test_hardgoods_bulk():
    with open("app/tests/hardgoods/hardgoods_many.json") as f:
        data = f.read()
    items = json.loads(data)
    some_hard_goods = hardgoods.HardGoodProducts(**items)
    xml = hardgoods.create_hardgoods_bulk(some_hard_goods, save=False)
    print("xml", xml)
    assert "656488" in xml



