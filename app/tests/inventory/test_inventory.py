import json
import warnings
from app import inventory

warnings.filterwarnings(action='ignore', module='.*paramiko.*')


def test_inventory_bulk():
    with open("app/tests/inventory/inventory_many.json") as f:
        data = f.read()
    items = json.loads(data)
    some_inventory = inventory.Inventory(**items)
    xml = inventory.create_inventory_bulk(some_inventory, save=False)
    print("xml", xml)
    assert "112870" in xml

