import json
import warnings
import pytest
from app import pricebooks


def test_pricebooks():

    with open("app/tests/pricebooks/pricebooks_many.json") as f:
        data = f.read()

    items = json.loads(data)
    some_pricebooks = pricebooks.PriceBooks(**items)
    xml = pricebooks.create_pricebooks_bulk(some_pricebooks, save=False)
    print("xml", xml)
    assert "456" in xml

