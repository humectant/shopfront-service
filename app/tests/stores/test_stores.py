import json
import warnings
from app import stores

warnings.filterwarnings(action='ignore', module='.*paramiko.*')


def test_stores():

    with open("app/tests/stores/stores_one.json") as f:
        data = f.read()

    some_stores = stores.Stores(stores=[json.loads(data)])
    xml = stores.create_stores_bulk(some_stores, save=False)
    print("xml", xml)
    assert "90 CENTRAL WAY" in xml


def test_stores_bulk():

    with open("app/tests/stores/stores_many.json") as f:
        data = f.read()

    items = json.loads(data)
    some_stores = stores.Stores(**items)
    xml = stores.create_stores_bulk(some_stores, save=False)
    print("xml", xml)
    assert "84 PINE ST" in xml
    assert "EAGLE POINT BUSINESS PARK" in xml

