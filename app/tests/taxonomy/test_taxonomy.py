import json
import warnings
from app import taxonomy

warnings.filterwarnings(action='ignore', module='.*paramiko.*')


def test_taxonomy_bulk():
    with open("app/tests/taxonomy/taxonomy_many.json") as f:
        data = f.read()
    items = json.loads(data)
    some_taxonomy = taxonomy.Taxonomy(**items)
    xml = taxonomy.create_taxonomy_bulk(some_taxonomy, save=False)
    print("xml", xml)
    assert "Food" in xml

