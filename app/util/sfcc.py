import datetime
import logging
import re
from app.schema import catalog

logger = logging.getLogger(__name__)


def map_attrib(pyxb_object, sfcc_key, value, default=''):
    """
    Map SLT attribute to SFCC attribute
    """

    if not value:
        value = default
    setattr(pyxb_object, sfcc_key, value)


def map_date_attrib(pyxb_object, sfcc_key, date_str):
    """
    Map SLT date attribute to SFCC attribute
    """

    if date_str:
        try:
            setattr(pyxb_object, sfcc_key, sfcc_datetime(date_str))
        except Exception as e:
            logger.warning(f"Failed to parse {date_str}")
            raise e


def append_attrib(pyxb_object, value, default=''):
    """
    Append SLT attribute value to SFCC attribute
    """

    if not value:
        value = default

    pyxb_object.append(value)


def append_date_attrib(pyxb_object, date_str):
    """
    Append SLT date attribute value to SFCC attribute
    """

    if date_str:
        try:
            logger.info(f'date_str {date_str}')
            pyxb_object.append(sfcc_datetime(date_str))
        except Exception:
            logger.warning(f"Failed to parse {date_str}")
            raise


def append_attributes(value_sets):
    """
    Append SLT attribute values to SFCC attributes
    """

    for attrib, value in value_sets:
        append_attrib(attrib, value)


def map_custom_attributes(attributes, value_sets):
    """
    Map SLT attributes to custom SFCC attributes
    """

    for key, value in value_sets:
        map_custom_attrib(attributes, key, value)


def map_custom_attrib(attributes, key, value, default=''):
    """
    Map SLT attribute to custom SFCC attribute
    """

    if not value:
        value = default
    attributes.append(
        catalog.sharedType_SiteSpecificCustomAttribute(
            value, attribute_id=key))


def sfcc_boolean(b):
    """
    Get SFCC boolean
    """

    return str(bool(b)).lower()


def build_attribute(id, name):
    """
    Generate attribute
    """

    attrib = catalog.complexType_VariationAttribute()
    attrib.display_name = [name]
    attrib.variation_attribute_id = id
    attrib.attribute_id = id

    return attrib


def build_variation_attribute_value(attribute_id_value, display_value):
    """
    Value must conform SFCC schema for an attribute ID
    """

    variation_attribute_value = catalog.complexType_VariationAttribute_Value()
    variation_attribute_value.attribute_id = attribute_id_value.strip()
    variation_attribute_value.variation_attribute_id = attribute_id_value.strip()
    variation_attribute_value.display_value = [display_value]
    return variation_attribute_value


def is_color(slt_product_sku):
    """
    True if this SKU has a color
    """

    return slt_product_sku.get('itemColor')


def add_variants(variants, slt_product_skus):
    """
    Generate variants XML
    """

    for slt_product_sku in slt_product_skus:
        variant = catalog.complexType_Product_Variations_Variant()
        variant.product_id = slt_product_sku.skuId
        variants.append(variant)


def product_image(path):
    """
    Generate product image XML
    """

    image = catalog.complexType_Product_Image()
    image.path = path
    return image


def image_group(view_type):
    """
    Generate image group XML
    """

    image_group = catalog.complexType_Product_ImageGroup()
    image_group.view_type = view_type
    return image_group


def image_group_variation(view_type, attribute_id, value):
    """
    Generate image group variations XML
    """

    image_group = catalog.complexType_Product_ImageGroup()
    image_group.view_type = view_type
    image_variation = catalog.complexType_Product_ImageGroup_VariationAttributeValue()
    image_variation.attribute_id = attribute_id
    image_variation.value_ = value.strip()
    image_group.variation.extend([image_variation])

    return image_group


def image_groups(slt_product, slt_product_skus):
    """
    Generate image groups XML
    """

    fake_image_dict = {"images": {"primary-image": "3436474_03Z_1218_p",
                                  "swatch-images": ["3979176_03i_1118_s"],
                                  "large-images": ["3436474_14Z_1218_14", "3897741_09G_0318_01"]}}

    image_groups = []
    image_group_large = image_group('large')
    image = product_image(fake_image_dict['images']['primary-image'])
    image_group_large.append(image)
    for path in fake_image_dict['images']['large-images']:
        image = product_image(path)
        image_group_large.append(image)
    image_groups.append(image_group_large)
    image_group_swatch = image_group('swatch')
    for path in fake_image_dict['images']['swatch-images']:
        image = product_image(path)
        image_group_swatch.append(image)
    image_groups.append(image_group_swatch)

    """
    for slt_product_sku in slt_product_skus:
        size = size_variation(slt_product_sku)
        color = color_variation(slt_product_sku)
        if size:
            image_group_large_sku = image_group_variation(
                'swatch', 'size', size)
            for path in fake_image_dict['images']['large-images']:
                image = product_image(path)
                image_group_large_sku.append(image)
            image_groups.append(image_group_large_sku)

        if color:
            image_group_swatch_sku = image_group_variation(
                'swatch', 'color', color)
            for path in fake_image_dict['images']['large-images']:
                image = product_image(path)
                image_group_swatch_sku.append(image)
            image_groups.append(image_group_swatch_sku)
    """

    return image_groups


def id_from_value(text_value):
    """
    Extract a value suitable for an XML attribute ID
    """

    numbers = re.findall(r'\d+', text_value)
    if numbers:
        clean_value = next(iter(re.findall(r'\d+', text_value)))
    else:
        clean_value = text_value.strip()
    logger.info(f'clean_value {clean_value}')
    return str(clean_value)


def id_from_date_value(date):
    """
    Extract a value suitable for an XML attribute ID
    """
    return str(date)


def file_name(filename):
    """
    Retun SFCC file name
    """
    timestamp = datetime.datetime.utcnow().strftime("%m%d%y%H%M%S")
    return f'{filename}-{timestamp}.xml'


def sfcc_datetime(date_str):
    try:
        return datetime.datetime.strptime(date_str, '%Y/%m/%d %H:%M:%S')
    except ValueError:
        raise ValueError('No valid date format found')


def url_join(*args):
    """
    Joins given arguments into an url. Trailing but not leading slashes are
    stripped for each argument.
    """

    return "/".join(map(lambda x: str(x).rstrip('/'), args))


def json_to_list(data):
    if isinstance(data, dict):
        return [data]
    else:
        return data