import datetime
import gzip
import logging
import os
import warnings
import collections
import paramiko
import pyxb

from app.configuration import CONFIG

warnings.filterwarnings(action='ignore', module='.*paramiko.*')

logger = logging.getLogger(__name__)

Document = collections.namedtuple('Document', 'destination path filename body')


def connect_sfcc_sftp(hostname, username, password=None, key_filename=None):
    """
    We use SSHClient so we can adjust lower level transport properties.
    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
    if key_filename:
        ssh.connect(hostname=hostname,
                    username=username,
                    key_filename=key_filename)
    else:
        ssh.connect(hostname,
                    username=username,
                    password=password)
    transport = ssh.get_transport()
    transport.window_size = 3 * 1024 * 1024
    transport.packetizer.REKEY_BYTES = pow(2, 40)
    transport.packetizer.REKEY_PACKETS = pow(2, 40)
    sftp = ssh.open_sftp()
    return sftp


def save_documents(documents):
    for destination, path, filename, body in documents:
        save(destination, path, filename, body)


def save(destination, path, filename, body):

    if destination == 'local':
        path = path.lstrip('/')
        path = os.path.join('data', path)
        os.makedirs(path, exist_ok=True)
        filepath = os.path.join(path, filename)
        logger.info(f'Save to {filepath}')
        with open(filepath, "w") as text_file:
            text_file.write(body.decode("utf-8"))

    if destination == 'sftp':

        # Let's try do this async or alternatively save to S3 and make SFTP a separate concern
        # Or run this as a background task: https://fastapi.tiangolo.com/tutorial/background-tasks/
        filepath = url_join(path, filename + '.gz')
        logger.info(f'SFTP to {filepath} to {CONFIG.SFCC_SFTP_HOST}')
        sftp = connect_sfcc_sftp(CONFIG.SFCC_SFTP_HOST,
                                 CONFIG.SFCC_SFTP_USERNAME,
                                 key_filename=CONFIG.SFCC_SFTP_KEY_PATH,
                                 password=CONFIG.SFCC_SFTP_PASSWORD)
        with sftp.file(filepath, 'w') as sftpf:
            with gzip.GzipFile(fileobj=sftpf, mode='w') as xmlzip:
                xmlzip.write(body)
                xmlzip.close()
            sftpf.close()


def to_xml(pyxb_object, namespace):
    pyxb.utils.domutils.BindingDOMSupport.SetDefaultNamespace(namespace)
    xml_bytes = pyxb_object.toDOM().toprettyxml(encoding="utf-8")
    return xml_bytes


def file_name(filename):
    timestamp = datetime.datetime.utcnow().strftime("%m%d%y%H%M%S")
    return f'{filename}-{timestamp}.xml'


def url_join(*args):
    return "/".join(map(lambda x: str(x).rstrip('/'), args))


def slt_datetime(date):
    return datetime.strptime(date, '%Y/%m/%d %H:%M:%S')

